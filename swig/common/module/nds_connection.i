/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil -*- */

/*
 * High-level SWIG Interface to NDS client library
 *
 * Copyright (C) 2015  Edward Maros <ed.maros@ligo.org>
 *
 * This work is derived from the C NDS clientlibrary created by Leo Singer
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef SWIG__COMMON__NDS_CONNECTION_I
#define SWIG__COMMON__NDS_CONNECTION_I

%include "std_string.i"

%{
#include "nds_swig.hh"

typedef NDS_swig::connection<NDS::connection, NDS::connection> connection;

typedef NDS_swig::parameters parameters;

using NDS::channel_predicate_object;
using NDS::request_period;
%}

%nds_doc_class_seperator( )


// %template(buffers_vector) std::vector< buffer >;
//%template(connection_parameters_type) std::vector< std::string >;

//typedef std::vector< buffer > buffers_type;
// typedef std::vector< channel > channels_type;

%nds_doc_class_begin(connection)
%nds_doc_brief("A connection to the NDS/NDS2 server")
%nds_doc_details("The connection object is used to interact with the NDS (v1 or v2) servers to retrieve"%nds_doc_nl( )
                 "       data and channel lists."%nds_doc_nl( )
                 " Connnection objects have a few main categories of methods:"%nds_doc_nl( )
                 "       1. Data retreival (fetch and iterate)"%nds_doc_nl( )
                 "       2. Channel listing (find_channels)"%nds_doc_nl( )
                 %nds_doc_nl( )
                 "       In addition there are methods designed to deal with the volume of data and channels in a NDS2 server:"%nds_doc_nl( )
                 "       1. Setting limits/epochs on requests (get_epoch, set_epoch, current_epoch)"%nds_doc_nl( )
                 "       2. Channel availability checking (get_availability)")
%nds_doc_class_end( )

#ifndef CONNECTION_PROTOCOL
%define CONNECTION_PROTOCOL( )
  typedef enum {
    PROTOCOL_INVALID = -1,
    PROTOCOL_ONE = 1,
    PROTOCOL_TWO = 2,
    PROTOCOL_TRY = 3
  } protocol_type;
%enddef
#endif /* CONNECTION_PROTOCOL */

class connection
{
public:
  CONNECTION_PROTOCOL( )

  typedef std::string host_type;
  typedef int port_type;
  typedef size_t count_type;
  typedef channel::channel_names_type channel_names_type;
  typedef std::vector<std::string> parameters_type;

  static const port_type DEFAULT_PORT = 31200;
  static const buffer::gps_second_type FAST_STRIDE = -1;
  static const buffer::gps_second_type AUTO_STRIDE = 0;

  %nds_doc_method_begin(connection,connection,)
  %nds_doc_param(const host_type& host,%nds_type_string,
                 "Server to connect to")
  %nds_doc_param(port_type port,%nds_type_port( ),
                 "Port number to connect to (defaults to DEFAULT_PORT)")
  %nds_doc_param(protocol_type,%nds_type_protocol( ),
                 "Protocol version to use (defaults to PROTOCOL_TRY)")
  %nds_doc_body_begin( )
  %nds_doc_brief("Create a connection")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  connection( const host_type& host,
              port_type port = DEFAULT_PORT,
              protocol_type protocol = PROTOCOL_TRY );

    %nds_doc_method_begin(connection,connection,)
    %nds_doc_param(const parameters& host,%nds_type_string,
    "Parameter/configuration object that specifies which server to connect to as well as default settings")
    %nds_doc_body_begin( )
    %nds_doc_brief("Create a connection with settings")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
  connection( const parameters& params );

    %nds_doc_method_begin(connection,connection,)
    %nds_doc_body_begin( )
    %nds_doc_brief("Create a connection with settings extracted from the environment")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
  connection( );

  %nds_doc_method_begin(connection,~connection,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Destroys connection")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  ~connection( );

  %nds_doc_method_begin(connection,close,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Close the connection")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  void close( );

  %nds_doc_method_begin(connection,count_channels,)
  %nds_doc_param(std::string channel_glob,%nds_type_string,
                 "A string to match channels to.  Bash style globs are allowed (* to mach anything, ? matches 1 character).  '*' matches all channels.")
  %nds_doc_param(channel::channel_type channel_type_mask,%nds_type_string,
                 "Set of channel types to search.  Defaults to all.")
  %nds_doc_param(channel::data_type data_type_mask,%nds_type_string,
                 "A set of data types to search.  Defaults to all.")
  %nds_doc_param(channel::sample_rate_type min_sample_rate,%nds_type_sample_rate( ),
                 "The lowest sample rate to return.")
  %nds_doc_param(channel::sample_rate_type max_sample_rate,%nds_type_sample_rate( ),
                 "The highest sample rate to return.")
  %nds_doc_body_begin( )
  %nds_doc_returns("A count of channels matching the input parameters.")
  %nds_doc_remark("For NDS1 a local cache of the channel list is searched.  For NDS2 this call may result in one or requests to the server, no local cache is used.")
  %nds_doc_remark("For requests against an NDS2 server the count is further constrained by the channels availabe during the currently selected epoch.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  count_type
  count_channels( std::string channel_glob = "*",
                  channel::channel_type channel_type_mask = channel::DEFAULT_CHANNEL_MASK,
                  channel::data_type data_type_mask = channel::DEFAULT_DATA_MASK,
                  channel::sample_rate_type min_sample_rate = channel::MIN_SAMPLE_RATE,
                  channel::sample_rate_type max_sample_rate = channel::MAX_SAMPLE_RATE );
  
  %nds_doc_method_begin(connection,find_channels,)
  %nds_doc_param(std::string channel_glob,%nds_type_string,
                 "A string to match channels to.  Bash style globs are allowed (* to mach anything, ? matches 1 character).  '*' matches all channels.")
  %nds_doc_param(channel::channel_type channel_type_mask,%nds_type_string,
                 "Set of channel types to search.  Defaults to all.")
  %nds_doc_param(channel::data_type data_type_mask,%nds_type_string,
                 "A set of data types to search.  Defaults to all.")
  %nds_doc_param(channel::sample_rate_type min_sample_rate,%nds_type_sample_rate( ),
                 "The lowest sample rate to return.")
  %nds_doc_param(channel::sample_rate_type max_sample_rate,%nds_type_sample_rate( ),
                 "The highest sample rate to return.")
  %nds_doc_body_begin( )
  %nds_doc_returns("A list of available channels matching the input parameters.")
  %nds_doc_remark("For NDS1 a local cache of the channel list is searched.  For NDS2 this call may result in one or"
                  " requests to the server, no local cache is used.")
  %nds_doc_remark("For requests against an NDS2 server the count is further constrained by the channels availabe during the currently selected epoch.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  channels_type
  find_channels( std::string channel_glob = "*",
                 channel::channel_type channel_type_mask = channel::DEFAULT_CHANNEL_MASK,
                 channel::data_type data_type_mask = channel::DEFAULT_DATA_MASK,
                 channel::sample_rate_type min_sample_rate = channel::MIN_SAMPLE_RATE,
                 channel::sample_rate_type max_sample_rate = channel::MAX_SAMPLE_RATE );

  %nds_doc_method_begin(connection,getEpochs,)
  %nds_doc_body_begin( )
  %nds_doc_returns("A list of pre-defined NDS2 epochs [start, stop) time spans.")
  %nds_doc_remark("This is only supported against NDS2 servers")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  epochs_type
  get_epochs();

  %nds_doc_method_begin(connection,setEpoch,)
  %nds_doc_param(std::string epoch,%nds_type_string,"The epoch value as a string")
  %nds_doc_body_begin( )
  %nds_doc_returns("True if the epoch can be set")
  %nds_doc_remark("The epoch may be a named epoch, ie 'O1' or a string with start/stop gps times, ie '1126621184-1137258496')")
  %nds_doc_remark("Sets the epoch to [gps_start,gps_stop)")
  %nds_doc_remark("This command only works for NDS2, NDS1 servers will silently ignore it.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  bool
  set_epoch(std::string epoch);

  %nds_doc_method_begin(connection,setEpoch,)
  %nds_doc_param(buffer::gps_second_type gps_start,%nds_type_gps_seconds( ),"Start GPS Time")
  %nds_doc_param(buffer::gps_second_type gps_stop,%nds_type_gps_seconds( ),"Stop GPS Time")
  %nds_doc_body_none();
  bool
  set_epoch(buffer::gps_second_type gps_start,
            buffer::gps_second_type gps_stop);

  %nds_doc_method_begin(connection,check,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The currently active epoch")
  %nds_doc_remark("This command only works for NDS2, NDS1 servers will return a default epoch")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  epoch
  current_epoch() const;

  %nds_doc_method_begin(connection,check,)
  %nds_doc_param(buffer::gps_second_type gps_start,%nds_type_gps_seconds( ),"Start GPS Time")
  %nds_doc_param(buffer::gps_second_type gps_stop,%nds_type_gps_seconds( ),"Stop GPS Time")
  %nds_doc_param(const channel_names_type channel_names,%nds_type_channel_names( ),"The channel list")
  %nds_doc_body_begin( )
  %nds_doc_brief("Check to see if data is avaiable")
  %nds_doc_details("Given a start/stop time and a channel list check to see if a fetch request would suceed.  This will return failure if there is a gap in the data, or if the data is on tape.")
  %nds_doc_returns("True if the data is available and not on tape.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  bool
  check( buffer::gps_second_type gps_start,
         buffer::gps_second_type gps_stop,
         const channel_names_type& channel_names );

  %nds_doc_method_begin(connection,fetch,)
  %nds_doc_param(buffer::gps_second_type gps_start,%nds_type_gps_seconds( ),"Start GPS Time")
  %nds_doc_param(buffer::gps_second_type gps_stop,%nds_type_gps_seconds( ),"Stop GPS Time")
  %nds_doc_param(const channel_names_type channel_names,%nds_type_channel_names( ), "The channel list")
  %nds_doc_body_begin( )
  %nds_doc_brief("Retreive data from the server")
  %nds_doc_details("Given a start/stop time and a channel list retreive the associated data.")
  %nds_doc_returns("A list of buffers containing the request data.")
  %nds_doc_remark("This command can respond to missing data and high latency situations in various ways.  Use the (get/set)_parameter function to set how data gaps or data on tape situations are handled.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  buffers_type
  fetch( buffer::gps_second_type gps_start,
         buffer::gps_second_type gps_stop,
         const channel_names_type& channel_names );

  %nds_doc_method_begin(connection,getHost,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Return the host that is currently connected.")
  %nds_doc_returns("The hostname")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  host_type get_host( ) const;

  %nds_doc_method_begin(connection,getPort,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Return the port number of the current connection.")
  %nds_doc_returns("The connection port number")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  port_type get_port( ) const;

  %nds_doc_method_begin(connection,getProtocol,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Return the protocol version in use.")
  %nds_doc_returns("The protoclol version (PROTOCOL_ONE|PROTOCOL_TWO)")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  protocol_type get_protocol( ) const;

  %nds_doc_method_begin(connection,setParameter,)
  %nds_doc_param(const std::string parameter,%nds_type_string,"Parameter name")
  %nds_doc_param(const std::string value,%nds_type_string,"The value represented as a string")
  %nds_doc_body_begin( )
  %nds_doc_brief("Change the default behavior of the connection.")
  %nds_doc_details("The connection object has a series of parameters that can be set.  Currently the parameters that can be set are \"ALLOW_DATA_ON_TAPE\", \"GAP_HANDLER\", and \"ITERATE_USE_GAP_HANDLERS\"")
  %nds_doc_remark("ALLOW_DATA_ON_TAPE (NDS2 only).  The NDS2 server may serve data that resides on a high latency storage layer, such as a tape system.  This may lead to data requests taking minutes or hours to complete, depending on the load on the storage system.  As of version 0.12 of the client the default is to raise an error when accessing data that is on a high latency storage layer.  This allows the application to provide feedback (if needed) to a users regarding amount of time that a request may take.  If this parameter is set to a true value (\"True\", \"1\", \"yes\") then an error will not be raised when requesting data on a high latency storage.")
  %nds_doc_remark("GAP_HANDLER For a given request there may not be be data available to fill the request completely.  This happens due to issues upstream of the NDS server.  How this is handled is application specific.  Setting the \"GAP_HANDLER\" parameter allows the application to specify what to do.  This includes options such as abort, zero fill the data, ...")
  %nds_doc_remark("ITERATE_USE_GAP_HANDLERS The iterate methods have a special case.  Unlike fetch operations which work on a single block, the iterate methods retrieve chunks of data that may not need to be contigous.  Setting ITERATE_USE_GAP_HANDLERS to \"false\" configures the connection to simply skip any gaps in the data and only return the data that is available.")
  %nds_doc_remark("Please note that if you are asking for multiple channels that do not have identical gaps the NDS servers will return a data not found error if ITERATE_USE_GAP_HANDLERS is set to false.")
  %nds_doc_returns("True if the value could be set, else false")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  bool set_parameter(const std::string &parameter, const std::string &value);

  %nds_doc_method_begin(connection,getParameter,)
  %nds_doc_param(const std::string parameter,%nds_type_string,"Parameter name")
  %nds_doc_body_begin( )
  %nds_doc_brief("Retreive the current parameter setting on a connection.")
  %nds_doc_remark("See documentation for set_parameter for details on the parameters.")
  %nds_doc_returns("The parameter value, or the empty string if an invalid parameter is requested")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  std::string get_parameter( const std::string &parameter ) const;

  %nds_doc_method_begin(connection,get_parameters,)
  %nds_doc_body_begin( )
  %nds_doc_returns("Returns a list of parameter names that may be used with set_parameter or get_parameter")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  connection::parameters_type get_parameters( ) const;

  %nds_doc_method_begin(connection,parameters,)
  %nds_doc_body_begin( )
  %nds_doc_returns("Returns a the parameters object associated with this object")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  parameters parameters( ) const;

  %nds_doc_method_begin(connection,hash,)
  %nds_doc_body_begin( )
  %nds_doc_remark("NDS2 only.  The NDS2 server maintains a hash of the channel list.  Clients may use this value to determine if the channel list has changed.")
  %nds_doc_returns("A hash of the current NDS2 server channel list.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  const channel::hash_type& hash( ) const;

  %nds_doc_method_begin(connection,iterate,)
  %nds_doc_param(buffer::gps_second_type gps_start,%nds_type_gps_seconds( ),"The start time of the request.")
  %nds_doc_param(buffer::gps_second_type gps_stop,%nds_type_gps_seconds( ),"The end time of the request [gps_start, gps_stop).")
  %nds_doc_param(buffer::gps_second_type stride,%nds_type_gps_seconds( ),"The number of seconds of data to return in each chunk.")
  %nds_doc_param(const channel_names_type channel_names,%nds_type_channel_names( ),"The list of channels to retreive data for")
  %nds_doc_body_none( )
  %nds_doc_method_end( )
  connection&
  iterate( buffer::gps_second_type gps_start,
           buffer::gps_second_type gps_stop,
           buffer::gps_second_type stride,
           const channel_names_type& channel_names );

  connection&
  iterate( const channel_names_type& channel_names );

  connection&
  iterate( buffer::gps_second_type stride,
           const channel_names_type& channel_names );

  connection&
  iterate( buffer::gps_second_type gps_start,
           buffer::gps_second_type gps_stop,
           const channel_names_type& channel_names );

  %nds_doc_method_begin(connection,next,)
  %nds_doc_body_begin( )
  %nds_doc_returns("Returns the next available data block during an iteration")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  buffers_type
  next( );

  %nds_doc_method_begin(connection,requestInProgress,)
  %nds_doc_body_begin( )
  %nds_doc_returns("True if there is a data request in progress.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  bool request_in_progress( ) const;

  %nds_doc_method_begin(connection,shutdown,)
  %nds_doc_body_begin( )
  %nds_doc_remark("Same as close")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  void shutdown( );

  %nds_doc_method_begin(connection,getAvailability,)
  %nds_doc_body_begin( )
  %nds_doc_returns("True if there is another data segment available during an iteration.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  availability_list_type
  get_availability(const channel_names_type& channel_names );

};

%nds_doc_class_seperator( )

#ifndef(SWIGPYTHON)
%nds_doc_class_seperator( )

%rename(channel_predicate) channel_predicate_object;
class channel_predicate_object
{
public:
    channel_predicate_object();
    channel_predicate_object(const channel_predicate_object& other);
    channel_predicate_object& operator=(const channel_predicate_object& other);

    std::string
    glob() const;

    channel::channel_type
    channel_type_mask() const;

    channel::data_type
    data_type_mask() const;

    channel::sample_rate_type
    min_sample_rate() const;

    channel::sample_rate_type
    max_sample_rate() const;

    buffer::gps_second_type
    gps_start() const;

    buffer::gps_second_type
    gps_stop() const;

    void
    set_glob( std::string val );

    void
    set_channel_type_mask( channel::channel_type val );

    void
    set_data_type_mask( channel::data_type val );

    void
    set_frequency_range(channel::sample_rate_type min_sample_rate,
                        channel::sample_rate_type max_sample_rate);

    void
    set_timespan(buffer::gps_second_type gps_start,
                 buffer::gps_second_type gps_stop);
};

%nds_doc_class_seperator( )

%nds_doc_class_seperator( )

struct request_period
{
    %typemap(in,doc="integer") buffer::gps_second_type = long;
    %typemap(in,doc="integer") stride_type = long;

    typedef buffer::gps_second_type gps_second_type;
    typedef long stride_type;

    request_period();
    request_period(const request_period& other);

    request_period( stride_type requested_stride );
    request_period( gps_second_type requested_start, gps_second_type requested_stop );
    request_period( gps_second_type requested_start, gps_second_type requested_stop, stride_type requested_stride );

    %nds_doc_class_storage_begin(request_period,start)
    %nds_doc_class_storage_end()
    gps_second_type start;
    %nds_doc_class_storage_begin(request_period,stop)
    %nds_doc_class_storage_end()
    gps_second_type stop;
    %nds_doc_class_storage_begin(request_period,stride)
    %nds_doc_class_storage_end()
    stride_type stride;
};

%nds_doc_class_seperator( )

#endif


%nds_doc_class_seperator( )

class parameters
{
public:
    %nds_doc_method_begin(parameters,parameters,)
    %nds_doc_body_begin( )
    %nds_doc_brief("Create a configuration object to be used in setting up a connection.  Parameters are pulled from the environment.")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
    parameters();

    %nds_doc_method_begin(parameters,parameters,)
    %nds_doc_param(const host_type& host,%nds_type_string,
    "Server to connect to")
    %nds_doc_param(port_type port,%nds_type_port( ),
    "Port number to connect to (defaults to DEFAULT_PORT)")
    %nds_doc_param(protocol_type,%nds_type_protocol( ),
    "Protocol version to use (defaults to PROTOCOL_TRY)")
    %nds_doc_body_begin( )
    %nds_doc_brief("Create a configuration object to be used in setting up a connection.")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
    parameters(connection::host_type hostname,
            connection::port_type=connection::DEFAULT_PORT,
            connection::protocol_type=connection::DEFAULT_PROTOCOL);

    %nds_doc_method_begin(parameters,set,)
    %nds_doc_param(const std::string parameter,%nds_type_string,"Parameter name")
    %nds_doc_param(const std::string value,%nds_type_string,"The value represented as a string")
    %nds_doc_body_begin( )
    %nds_doc_brief("Change the default behavior of the connection.")
    %nds_doc_details("The connection object has a series of parameters that can be set.  Currently the parameters that can be set are \"ALLOW_DATA_ON_TAPE\", \"GAP_HANDLER\", and \"ITERATE_USE_GAP_HANDLERS\"")
    %nds_doc_remark("ALLOW_DATA_ON_TAPE (NDS2 only).  The NDS2 server may serve data that resides on a high latency storage layer, such as a tape system.  This may lead to data requests taking minutes or hours to complete, depending on the load on the storage system.  As of version 0.12 of the client the default is to raise an error when accessing data that is on a high latency storage layer.  This allows the application to provide feedback (if needed) to a users regarding amount of time that a request may take.  If this parameter is set to a true value (\"True\", \"1\", \"yes\") then an error will not be raised when requesting data on a high latency storage.")
    %nds_doc_remark("GAP_HANDLER For a given request there may not be be data available to fill the request completely.  This happens due to issues upstream of the NDS server.  How this is handled is application specific.  Setting the \"GAP_HANDLER\" parameter allows the application to specify what to do.  This includes options such as abort, zero fill the data, ...")
    %nds_doc_remark("ITERATE_USE_GAP_HANDLERS The iterate methods have a special case.  Unlike fetch operations which work on a single block, the iterate methods retrieve chunks of data that may not need to be contigous.  Setting ITERATE_USE_GAP_HANDLERS to \"false\" configures the connection to simply skip any gaps in the data and only return the data that is available.")
    %nds_doc_remark("Please note that if you are asking for multiple channels that do not have identical gaps the NDS servers will return a data not found error if ITERATE_USE_GAP_HANDLERS is set to false.")
    %nds_doc_returns("True if the value could be set, else false")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
    bool set(const std::string &parameter, const std::string &value);

    %nds_doc_method_begin(parameters,get,)
    %nds_doc_param(const std::string parameter,%nds_type_string,"Parameter name")
    %nds_doc_body_begin( )
    %nds_doc_brief("Retreive the current parameter setting on a connection.")
    %nds_doc_remark("See documentation for set_parameter for details on the parameters.")
    %nds_doc_returns("The parameter value, or the empty string if an invalid parameter is requested")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
    std::string get( const std::string &parameter ) const;

    %nds_doc_method_begin(parameters,parameter_list,)
    %nds_doc_body_begin( )
    %nds_doc_returns("Returns a list of parameter names that may be used with set_parameter or get_parameter")
    %nds_doc_body_end( )
    %nds_doc_method_end( )
    connection::parameters_type parameter_list( ) const;
};

%nds_doc_class_seperator( )

#if defined(SWIGPYTHON)
%rename(_fetch) fetch;
%rename(_find_channels) find_channels;
#endif
#if defined(SWIGJAVA)
%rename(_find_channels) find_channels;
#endif
namespace NDS_swig {

    namespace standalone {
        buffers_type
        fetch( buffer::gps_second_type   gps_start,
               buffer::gps_second_type   gps_stop,
               const connection::channel_names_type& channel_names );

        buffers_type
        fetch( const parameters& param,
                buffer::gps_second_type   gps_start,
                buffer::gps_second_type   gps_stop,
                const connection::channel_names_type& channel_names );

        channels_type
        find_channels(
                const parameters& params,
                std::string           channel_glob = "*",
                channel::channel_type channel_type_mask =
                channel::DEFAULT_CHANNEL_MASK,
                channel::data_type data_type_mask = channel::DEFAULT_DATA_MASK,
                channel::sample_rate_type min_sample_rate =
                channel::MIN_SAMPLE_RATE,
                channel::sample_rate_type max_sample_rate =
                channel::MAX_SAMPLE_RATE,
                epoch timespan=epoch("", 0, buffer::GPS_INF)
        );

        channels_type
        find_channels(
                std::string           channel_glob = "*",
                channel::channel_type channel_type_mask =
                channel::DEFAULT_CHANNEL_MASK,
                channel::data_type data_type_mask = channel::DEFAULT_DATA_MASK,
                channel::sample_rate_type min_sample_rate =
                channel::MIN_SAMPLE_RATE,
                channel::sample_rate_type max_sample_rate =
                channel::MAX_SAMPLE_RATE,
                epoch timespan=epoch("", 0, buffer::GPS_INF)
        );
    }
}

#endif /* SWIG__COMMON__NDS_CONNECTION_I */
