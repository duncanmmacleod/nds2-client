/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

%include "std_vector.i"

%{
#include <sstream>

#include "nds_buffer.hh"

#include "nds_swig.hh"

  using NDS::buffer;
  using NDS_swig::buffers_type;
%}

// ======================================================================
//  Buffer object
// ======================================================================

%shared_ptr( buffer );

%nds_doc_class_seperator( )

%nds_doc_class_begin(buffer)
%nds_doc_brief("The buffer class facilitates the storage of data and accompanying meta data.")
%nds_doc_class_end( )
class buffer
  : public channel
{
public:
  %typemap(in,doc="integer") gps_second_type = long;
  //---------------------------------------------------------------------
  /// \brief Type second portion of a gps time
  //---------------------------------------------------------------------
  typedef long gps_second_type;
  //---------------------------------------------------------------------
  /// \brief Type nano second portion of a gps time
  //---------------------------------------------------------------------
  typedef long gps_nanosecond_type;
  //---------------------------------------------------------------------
  /// \brief Type appropriate for length.
  //---------------------------------------------------------------------
  typedef size_t size_type;
  //---------------------------------------------------------------------
  /// \brief Storage type for the data.
  //---------------------------------------------------------------------
  typedef std::vector< unsigned char > data_type;

  static const gps_second_type GPS_INF = 1999999999;

  //---------------------------------------------------------------------
  /// \brief Retrieve the number of samples being stored
  ///
  /// \return
  ///     The number of samples stored in the buffer
  //---------------------------------------------------------------------
  %nds_doc_method_begin(buffer,Samples,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The number of samples stored in the buffer")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  size_type Samples( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the GPS start second of the data
  ///
  /// \return
  ///     The GPS start second of the data
  //---------------------------------------------------------------------
  %nds_doc_method_begin(buffer,Start,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The GPS start second of the data")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  gps_second_type Start( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the GPS start nano second of the data
  ///
  /// \return
  ///     The GPS start nano second of the data
  //---------------------------------------------------------------------
  %nds_doc_method_begin(buffer,StartNono,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The GPS start nano second of the data")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  gps_nanosecond_type StartNano( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the GPS stop second of the data
  ///
  /// \return
  ///     The GPS stop second of the data
  //---------------------------------------------------------------------
  %nds_doc_method_begin(buffer,Start,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The GPS stop second of the data")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  gps_second_type Stop( ) const;
};

%extend buffer {
  std::string ToString( )
  {
    std::ostringstream msg;

    msg << ( *self );

    return msg.str( );
  }
}

// ======================================================================
//  Collection of buffer Objects
// ======================================================================

#if !defined(%nds_buffers_type)
%define %nds_buffers_type( )
%shared_ptr( buffer );
%shared_ptr( buffers_type );
%shared_ptr( std::vector< std::shared_ptr< buffer > > );
%template(vectorBuffers) std::vector< std::shared_ptr< buffer > >;
class buffers_type
  : public std::vector< std::shared_ptr< buffer > >
{
public:
  ~buffers_type( );
};
%enddef
#endif /* !defined(%nds_buffers_type) */

%nds_buffers_type( );

%extend buffers_type {
  std::string ToString( )
  {
    std::ostringstream msg;

    msg << ( *self );

    return msg.str( );
  }
}

%nds_doc_class_seperator( )
