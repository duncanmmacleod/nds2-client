/*
 * SWIG bindings for high-level NDS client library based on the C++ interface
 *
 * Copyright (C) 2015  Edward Maros <ed.maros@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

%module nds2

%include "std_string.i"
%include "std_vector.i"
%include "exception.i"

%include "std_shared_ptr.i"


%{
#include <memory>
#include "nds_channel.hh"
#include "nds_connection.hh"

%}

%exception {
  try
  {
    $action
  }
  catch( std::bad_alloc& e )
  {
    SWIG_exception( SWIG_MemoryError, const_cast<char*>(e.what( ) ) );
  }
  catch( std::invalid_argument& e )
  {
    SWIG_exception( SWIG_TypeError, const_cast<char*>(e.what( ) ) );
  }
  catch( std::overflow_error& e )
  {
    SWIG_exception( SWIG_OverflowError, const_cast<char*>(e.what( ) ) );
  }
  catch( std::ios_base::failure& e )
  {
    SWIG_exception( SWIG_IOError, const_cast<char*>(e.what( ) ) );
  }
  catch( std::out_of_range& e )
  {
    SWIG_exception( SWIG_IndexError, const_cast<char*>(e.what( ) ) );
  }
#if HAVE_STD_SYSTEM_ERROR
  catch( std::system_error& e )
  {
    SWIG_exception( SWIG_SystemError, const_cast<char*>(e.what( ) ) );
  }
#endif /* HAVE_STD_SYSTEM_ERROR */
  catch( std::exception& e )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>(e.what( ) ) );
  }
  catch( ... )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>( "Unknown exception thrown" ) );
  }
}

/*
 *
 */

#if ! defined(%nds_type_channel_names)
%define %nds_type_channel_names() "channel_names"
%enddef
#endif /* ! defined(%nds_type_channel_names) */

#if ! defined(%nds_type_gps_seconds)
%define %nds_type_gps_seconds() "gps_second_type"
%enddef
#endif /* ! defined(%nds_type_gps_seconds) */

#if ! defined(%nds_type_port)
%define %nds_type_port() "int"
%enddef
#endif /* ! defined(%nds_type_port) */

#if ! defined(%nds_type_protocol)
%define %nds_type_protocol() "protocol"
%enddef
#endif /* ! defined(%nds_type_protocol) */

#if ! defined(%nds_type_sample_rate)
%define %nds_type_string "sample_rate"
%enddef
#endif /* ! defined(%nds_type_sample_rate) */

#if ! defined(%nds_type_string)
%define %nds_type_string "string"
%enddef
#endif /* ! defined(%nds_type_string) */

/*
 *
 */

#if ! defined(%nds_doc_body_begin)
%define %nds_doc_body_begin()
%feature("docstring")
%enddef
#endif /* ! defined(%nds_doc_body_begin) */

#if ! defined(%nds_doc_body_end)
%define %nds_doc_body_end()
;
%enddef
#endif /* ! defined(%nds_doc_body_end) */

#if ! defined(%nds_doc_body_none)
%define %nds_doc_body_none()
%feature("docstring") "";
%enddef /* %nds_doc_body_none */
#endif /* ! defined(%nds_doc_body_none) */ 

#if ! defined(%nds_doc_class_begin)
%define %nds_doc_class_begin(__CLASS__)
%feature("docstring")
%enddef
#endif /* ! defined(%nds_doc_class_begin) */

#if ! defined(%nds_doc_class_end)
%define %nds_doc_class_end()
;
%enddef
#endif /* ! defined(%nds_doc_class_end) */

#if ! defined(%nds_doc_class_storage_begin)
%define %nds_doc_class_storage_begin(__CLASS__,__STORAGE__)
%feature("autodoc","2");
%enddef
#endif /* ! defined(%nds_doc_class_storage_begin) */

#if ! defined(%nds_doc_class_storage_end)
%define %nds_doc_class_storage_end()
%enddef
#endif /* ! defined(%nds_doc_class_storage_end) */

#if ! defined(%nds_doc_method_begin)
%define %nds_doc_method_begin(__CLASS__,__METHOD__,__CALL__)
%feature("autodoc","2");
%enddef
#endif /* ! defined(%nds_doc_method_begin) */

#if ! defined(%nds_doc_method_end)
%define %nds_doc_method_end()
%enddef
#endif /* ! defined(%nds_doc_method_end) */

#if ! defined(%nds_doc_param)
%define %nds_doc_param(CALL,TYPE,DESC)
%enddef /* %nds_doc_param */
#endif /* ! defined(%nds_doc_param) */

#if ! defined(%nds_doc_remark)
%define %nds_doc_remark(TEXT)
%enddef /* %nds_doc_remark */
#endif /* ! defined(%nds_doc_remark) */ 

#if ! defined(%nds_doc_returns)
%define %nds_doc_returns(TEXT)
%enddef /* %nds_doc_returns */
#endif /* ! defined(%nds_doc_returns) */ 

#if ! defined(%nds_doc_nl)
%define %nds_doc_nl()"
"
%enddef /* %nds_doc_nl */
#endif /* ! defined(%nds_doc_nl) */

#if ! defined(%nds_doc_class_seperator)
%define %nds_doc_class_seperator()
%enddef
#endif /* ! defined(%nds_doc_class_seperator) */

/*
 * Pull in the rest of the pieces
 */

%include "nds_channel.i"
%include "nds_buffer.i"
%include "nds_epoch.i"
%include "nds_availability.i"
%include "nds_connection.i"
%include "nds_version.i"

