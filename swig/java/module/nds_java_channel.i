#ifndef NDS_JAVA_CHANNEL_I
#define NDS_JAVA_CHANNEL_I

%import "nds_java_vector_to_array.i"

%extend channel {
  std::string toString( ) {
    return $self->NameLong( );
  }

  channel_type getChannelType( )
  {
    return $self->Type( );
  }

  data_type getDataType( )
  {
    return $self->DataType( );
  }

  MATLABSTRINGTYPE getName( )
  {
    return $self->Name( );
  }

  static MATLABSTRINGTYPE channelTypeToString( channel::channel_type ChannelType )
  {
    return channel_type_to_string( ChannelType );
  }

  static MATLABSTRINGTYPE dataTypeToString( channel::data_type DataType )
  {
    return data_type_to_string( DataType );
  }

  sample_rate_type getSampleRate( )
  {
    return $self->SampleRate( );
  }

  float getSignalGain( )
  {
    return $self->Gain( );
  }

  float getSignalOffset( )
  {
    return $self->Offset( );
  }

  float getSignalSlope( )
  {
  return $self->Slope( );
  }

  std::string getSignalUnits( )
  {
    return $self->Units( );
  }

  static std::string dataTypeToJString( channel::data_type DataType )
  {
    return data_type_to_string( DataType );
  }

  static std::string typeToJString( channel::channel_type ChannelType )
  {
    return channel_type_to_string( ChannelType );
  }

}

%define %nds_channels_type( )
%nds_vector_to_array(channels_type,channel);
%enddef

#endif /* NDS_JAVA_CHANNEL_I */
