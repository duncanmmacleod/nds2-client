%import "nds_java_vector_to_array.i"


%{
  using NDS::BUFFER_type;
%}
// ======================================================================
//  Buffer Objects
// ======================================================================

%typemap(jni) BUFFER_type "jobject";
%typemap(jtype) BUFFER_type "Object";
%typemap(jstype) BUFFER_type "Object";
%typemap(javaout) BUFFER_type {
  return $jnicall;
}
%typemap(out, noblock=1) BUFFER_type {
  //---------------------------------------------------------------------
  // Return the data vector as a Java Object
  //---------------------------------------------------------------------
  jsize                       len = (jsize)($1->Samples( ));
  const void*                 dp( ($1)->cbegin<void>() );

  switch ( $1->DataType( ) )
  {
  case NDS::channel::DATA_TYPE_INT16:
    {
      $result = jenv->NewShortArray( len );
      jenv->SetShortArrayRegion( reinterpret_cast<jshortArray>( $result ),
				 0, len, (jshort *)dp );
    }
    break;
  case NDS::channel::DATA_TYPE_INT32:
  case NDS::channel::DATA_TYPE_UINT32:
    {
      $result = jenv->NewIntArray( len );
      jenv->SetIntArrayRegion( reinterpret_cast<jintArray>( $result ),
			       0, len, (jint *)dp );
    }
    break;
  case NDS::channel::DATA_TYPE_INT64:
    {
      $result = jenv->NewLongArray( len );
      jenv->SetLongArrayRegion( reinterpret_cast<jlongArray>( $result ),
				0, len, (jlong *)dp );
    }
    break;
  case NDS::channel::DATA_TYPE_FLOAT32:
    {
      $result = jenv->NewFloatArray( len );
      jenv->SetFloatArrayRegion( reinterpret_cast<jfloatArray>( $result ),
				 0, len, (jfloat *)dp );
    }
    break;
  case NDS::channel::DATA_TYPE_FLOAT64:
    {
      $result = jenv->NewDoubleArray( len );
      jenv->SetDoubleArrayRegion( reinterpret_cast<jdoubleArray>( $result ),
				  0, len, (jdouble *)dp );
    }
    break;
  default:
    // SWIG_exception(SWIG_TypeError, "Unknown NDS data type");
    break;
  }
}

%extend buffer {
  std::string toString( ) {
    return $self->NameLong( );
  }

  gps_second_type getGpsSeconds( )
  {
    return $self->Start( );
  }

  gps_nanosecond_type getGpsNanoseconds( )
  {
    return $self->StartNano( );
  }

  channel getChannel( )
  {
    channel*    retval = dynamic_cast< channel* >( $self );

    return *retval;
  }

  BUFFER_type getData( )
  {
    return $self;
  }

  long getLength( )
  {
    return (long)($self->Samples( ));
  }
}

%begin %{

#include "nds_buffer.hh"

namespace NDS
{

  typedef buffer* BUFFER_type;

}
%}

// ======================================================================
//  Collection of buffer Objects
// ======================================================================

%define %nds_buffers_type( )
%nds_vector_to_array(buffers_type,buffer);
%enddef

