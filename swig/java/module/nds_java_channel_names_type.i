#ifndef NDS_JAVA_CHANNEL_NAMES_TYPE_I
#define NDS_JAVA_CHANNEL_NAMES_TYPE_I

%typemap(in) CHANNEL_NAMES_TYPE(jint size) {
  //---------------------------------------------------------------------
  // Convert java String to channel_names_type for use by C++ layer
  //---------------------------------------------------------------------
  size = jenv->GetArrayLength( $input );
  $1 = new $1_basetype;

  for ( int i = 0;
	i < size;
	++i )
  {
    jstring 	js = (jstring)( jenv->GetObjectArrayElement( $input, i ) );
    const char* cs = jenv->GetStringUTFChars( js, 0 );

    $1->push_back( $1_basetype::value_type( cs ) );

    jenv->ReleaseStringUTFChars( js, cs );
    jenv->DeleteLocalRef( js );
  }
}

%typemap(freearg) CHANNEL_NAMES_TYPE {
  delete $1;
}

%typemap(jni) CHANNEL_NAMES_TYPE "jobjectArray";
%typemap(jtype) CHANNEL_NAMES_TYPE "String[]";
%typemap(jstype) CHANNEL_NAMES_TYPE "String[]";
%typemap(javain) CHANNEL_NAMES_TYPE "$javainput";
%typemap(javaout) CHANNEL_NAMES_TYPE {
  return $jnicall;
}

%apply CHANNEL_NAMES_TYPE { connection::channel_names_type, connection::channel_names_type& };

#endif /* NDS_JAVA_CHANNEL_NAMES_TYPE_I */
