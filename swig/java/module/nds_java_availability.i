#if ( SWIG_VERSION >= 0x030000 )

%extend availability_list_type {
    void pushBack( std::shared_ptr< availability > x ) {
      $self->push_back( x );
    }
}
%extend simple_availability_list_type {
    void pushBack( std::shared_ptr< simple_segment_list_type > x ) {
      $self->push_back( x );
    }
}
%extend segment_list_type {
    void pushBack( std::shared_ptr< segment > x ) {
      $self->push_back( x );
    }
}
%extend simple_segment_list_type {
    void pushBack( std::shared_ptr< simple_segment > x ) {
      $self->push_back( x );
    }
}
#endif /* ( SWIG_VERSION >= 0x030000 ) */
