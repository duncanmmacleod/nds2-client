% -*- mode: octave -*-
%------------------------------------------------------------------------
global unit_test;

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run test
%------------------------------------------------------------------------
data = letOriginalDataGoOutOfScope( conn );
unit_test.check( data(1), ...
		 data(2), ...
		 'Verify first data pair on returned data' );
unit_test.check( data(2), ...
		 data(3), ...
		 'Verify second data pair on returned data' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
exit( unit_test.exit_code( ) );

