/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */
import nds2.connection;
import nds2.buffer;

public final class test_ticket_288_java {
    private static unittest unit_test;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/*
	 * Need to adjust according to parameter
	 */

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port );
	conn.setParameter("ALLOW_DATA_ON_TAPE", "true");
	conn.setParameter("ITERATE_USE_GAP_HANDLERS", "false");

	/* ------------------------------------------------------------- *
	 * Ticket 288
	 * ------------------------------------------------------------- */

	int blockCount = 0;
	long lastGPS = 0;
	double[] expected = new double[]{
	    66.7626,
	    0.0,
	    67.4257,
	    0.0,
	    0.0,
	    0.0
	};
	double[] actual = new double[ expected.length ];

	int start = 1167530460;
	int end =  1178294460;
	String[] channels = new String[] {
	    "H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend"
	};

	for( buffer[] bufs : conn.iterate(start, end, channels) )
	{
	    lastGPS = bufs[ 0 ].stop( );
	    double[] data = (double[])( bufs[0].getData( ) );
	    actual[ blockCount ] = data[120];
	    blockCount += 1;
	}
	unit_test.check( blockCount, 6,
			 "Correct number of blocks were returned" );
	unit_test.check( lastGPS, end,
			 "Correct last GPS valued reported" );
	for ( int i = 0; i < expected.length; i++ )
	{
	    unit_test.check( actual[ i ],
			     expected[ i ],
			     "Verifying channel contents" );
	}

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}
