/*
 * Unit tests against mock-up NDS 1 server
 *
 * Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import nds2.buffer;
import nds2.connection;
import nds2.channel;

public final class test_mockup_java {

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                  + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static ByteBuffer[] rawBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("B000")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("DEADBEEF")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("00000000FEEDFACE")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("FEEDFACE")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static ByteBuffer[] minBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("0101")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("02020202")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("0303030303030303")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static ByteBuffer[] maxBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("1111")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("12121212")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("1313131313131313")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static ByteBuffer[] meanBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("1A1A1A1A1A1A1A1A")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("2A2A2A2A2A2A2A2A")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("3A3A3A3A3A3A3A3A")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static ByteBuffer[] rmsBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("1B1B1B1B1B1B1B1B")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("2B2B2B2B2B2B2B2B")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("3B3B3B3B3B3B3B3B")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static ByteBuffer[] nBytepatterns = {
	ByteBuffer.wrap(hexStringToByteArray("1C1C1C1C")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("2C2C2C2C")).order(ByteOrder.LITTLE_ENDIAN),
	ByteBuffer.wrap(hexStringToByteArray("3C3C3C3C")).order(ByteOrder.LITTLE_ENDIAN),
    };

    public static void main(String[] args) throws java.io.UnsupportedEncodingException
    {
	Map<String, String> env = System.getenv();
	int port = Integer.parseInt(env.get("NDS_TEST_PORT"));

	connection conn = new connection("localhost", port, connection.PROTOCOL_ONE);
	assert conn.getPort() == port;
	assert conn.getProtocol() == 1;

	/* this test needs to be updated when the mock server's channel list changes */
	channel[] channels = conn.findChannels("*");
	int base_channel_count = 9;
	assert channels.length == base_channel_count * (5*2 +1);

	channels = conn.findChannels("X*:{A,B,C,G,H,I}");
	assert channels.length == 6;
	assert channels[0].toString().equals("<X1:A (16Hz, ONLINE, INT16)>");
	assert channels[1].toString().equals("<X1:B (16Hz, ONLINE, INT32)>");
	assert channels[2].toString().equals("<X1:C (16Hz, ONLINE, INT64)>");
	assert channels[3].toString().equals("<X1:G (16Hz, ONLINE, UINT32)>");
	assert channels[4].toString().equals("<X1:H (65536Hz, ONLINE, FLOAT32)>");
	assert channels[5].toString().equals("<X1:I (16384Hz, ONLINE, UINT32)>");

	channels = conn.findChannels("X1:G*");
	Set<String> actualNames = new HashSet<String>();
	for (int i = 0; i < channels.length; i++) {
		actualNames.add(channels[i].toString());
	}
	Set<String> expectedNames = new HashSet<String>(Arrays.asList(new String[]{"<X1:G (16Hz, ONLINE, UINT32)>", "<X1:G.max,m-trend (0.0166667Hz, MTREND, UINT32)>", 
    "<X1:G.max,s-trend (1Hz, STREND, UINT32)>", "<X1:G.mean,m-trend (0.0166667Hz, MTREND, FLOAT64)>", 
    "<X1:G.mean,s-trend (1Hz, STREND, FLOAT64)>", "<X1:G.min,m-trend (0.0166667Hz, MTREND, UINT32)>", 
    "<X1:G.min,s-trend (1Hz, STREND, UINT32)>", "<X1:G.rms,m-trend (0.0166667Hz, MTREND, FLOAT64)>", 
    "<X1:G.rms,s-trend (1Hz, STREND, FLOAT64)>", "<X1:G.n,m-trend (0.0166667Hz, MTREND, INT32)>",
    "<X1:G.n,s-trend (1Hz, STREND, INT32)>"}));
	assert actualNames.equals(expectedNames);

	buffer[] bufs = conn.fetch(1000000000, 1000000004,
				   new String[] {"X1:A", "X1:B", "X1:C", "X1:G"});
	assert bufs.length == 4;
	for (int i = 0; i < bufs.length; i++) {
	    buffer buf = bufs[i];
	    assert buf.getGpsSeconds() == 1000000000;
	    assert buf.getGpsNanoseconds() == 0;

	    channel channel = buf.getChannel();
	    int chantype = channel.getDataType();
	    Object dataraw = buf.getData();

	    if (chantype == channel.DATA_TYPE_INT16) {
		short[] data = (short[]) dataraw;

		assert data.length == 4 * channel.getSampleRate();

		short[] test = new short[data.length];
		Arrays.fill(test, rawBytepatterns[i].getShort(0));
		assert Arrays.equals(data, test);

	    } else if (chantype == channel.DATA_TYPE_INT32 || chantype == channel.DATA_TYPE_UINT32) {
		int[] data = (int[]) dataraw;

		assert data.length == 4 * channel.getSampleRate();

		int[] test = new int[data.length];
		Arrays.fill(test, rawBytepatterns[i].getInt(0));
		assert Arrays.equals(data, test);

	    } else if (chantype == channel.DATA_TYPE_INT64) {
		long[] data = (long[]) dataraw;

		assert data.length == 4 * channel.getSampleRate();

		long[] test = new long[data.length];
		Arrays.fill(test, rawBytepatterns[i].getLong(0));
		assert Arrays.equals(data, test);

	    }
	}

	for (int j = 0; j < 2; j++) {
		int start = 1000000000;
        int end = 1000000020;
        int mult = 1;
        int samples = 20;
        String trend_type = "s-trend";
		if (j != 0) {
			start = 1000000020;
	        end = 1000000140;
	        mult = 60;
	        samples = 2;
	        trend_type = "m-trend";
		}

		String[] channel_list = new String[] {"X1:A.min,"+trend_type, "X1:B.min,"+trend_type, "X1:C.min,"+trend_type};
		bufs = conn.fetch(start, end, channel_list);
		assert bufs.length == 3;
		for (int i = 0; i < bufs.length; i++) {
		    buffer buf = bufs[i];
		    assert buf.getGpsSeconds() == start;
		    assert buf.getGpsNanoseconds() == 0;

		    channel channel = buf.getChannel();
		    int chantype = channel.getDataType();
		    Object dataraw = buf.getData();

		    channel cross_check = conn.findChannels(channel_list[i])[0];
		    assert chantype == cross_check.getDataType();

		    if (chantype == channel.DATA_TYPE_INT16) {
			short[] data = (short[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			short[] test = new short[data.length];
			Arrays.fill(test, minBytepatterns[i].getShort(0));
			assert Arrays.equals(data, test);

		    } else if (chantype == channel.DATA_TYPE_INT32) {
			int[] data = (int[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			int[] test = new int[data.length];
			Arrays.fill(test, minBytepatterns[i].getInt(0));
			assert Arrays.equals(data, test);

		    } else if (chantype == channel.DATA_TYPE_INT64) {
			long[] data = (long[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			long[] test = new long[data.length];
			Arrays.fill(test, minBytepatterns[i].getLong(0));
			assert Arrays.equals(data, test);

		    } else {
		    	// unexpected data type
		    	assert false;
		    }
		}

		channel_list = new String[] {"X1:A.max,"+trend_type, "X1:B.max,"+trend_type, "X1:C.max,"+trend_type};
		bufs = conn.fetch(start, end, channel_list);
		assert bufs.length == 3;
		for (int i = 0; i < bufs.length; i++) {
		    buffer buf = bufs[i];
		    assert buf.getGpsSeconds() == start;
		    assert buf.getGpsNanoseconds() == 0;

		    channel channel = buf.getChannel();
		    int chantype = channel.getDataType();
		    Object dataraw = buf.getData();

		    channel cross_check = conn.findChannels(channel_list[i])[0];
		    assert chantype == cross_check.getDataType();

		    if (chantype == channel.DATA_TYPE_INT16) {
			short[] data = (short[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			short[] test = new short[data.length];
			Arrays.fill(test, maxBytepatterns[i].getShort(0));
			assert Arrays.equals(data, test);

		    } else if (chantype == channel.DATA_TYPE_INT32) {
			int[] data = (int[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			int[] test = new int[data.length];
			Arrays.fill(test, maxBytepatterns[i].getInt(0));
			assert Arrays.equals(data, test);

		    } else if (chantype == channel.DATA_TYPE_INT64) {
			long[] data = (long[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			long[] test = new long[data.length];
			Arrays.fill(test, maxBytepatterns[i].getLong(0));
			assert Arrays.equals(data, test);

		    } else {
		    	// unexpected data type
		    	assert false;
		    }
		}

		channel_list = new String[] {"X1:A.mean,"+trend_type, "X1:B.mean,"+trend_type, "X1:C.mean,"+trend_type};
		bufs = conn.fetch(start, end, channel_list);
		assert bufs.length == 3;
		for (int i = 0; i < bufs.length; i++) {
		    buffer buf = bufs[i];
		    assert buf.getGpsSeconds() == start;
		    assert buf.getGpsNanoseconds() == 0;

		    channel channel = buf.getChannel();
		    int chantype = channel.getDataType();
		    Object dataraw = buf.getData();

		    channel cross_check = conn.findChannels(channel_list[i])[0];
		    assert chantype == cross_check.getDataType();
		    assert chantype == channel.DATA_TYPE_FLOAT64;

		    if (chantype == channel.DATA_TYPE_FLOAT64) {
			double[] data = (double[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			double[] test = new double[data.length];
			Arrays.fill(test, meanBytepatterns[i].getDouble(0));
			assert Arrays.equals(data, test);
		    } else {
		    	// unexpecte type
		    	assert false;
		    }
		}

		channel_list = new String[] {"X1:A.rms,"+trend_type, "X1:B.rms,"+trend_type, "X1:C.rms,"+trend_type};
		bufs = conn.fetch(start, end, channel_list);
		assert bufs.length == 3;
		for (int i = 0; i < bufs.length; i++) {
		    buffer buf = bufs[i];
		    assert buf.getGpsSeconds() == start;
		    assert buf.getGpsNanoseconds() == 0;

		    channel channel = buf.getChannel();
		    int chantype = channel.getDataType();
		    Object dataraw = buf.getData();

		    channel cross_check = conn.findChannels(channel_list[i])[0];
		    assert chantype == cross_check.getDataType();
		    assert chantype == channel.DATA_TYPE_FLOAT64;

		    if (chantype == channel.DATA_TYPE_FLOAT64) {
			double[] data = (double[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			double[] test = new double[data.length];
			Arrays.fill(test, rmsBytepatterns[i].getDouble(0));
			assert Arrays.equals(data, test);
		    } else {
		    	// unexpecte type
		    	assert false;
		    }
		}

		channel_list = new String[] {"X1:A.n,"+trend_type, "X1:B.n,"+trend_type, "X1:C.n,"+trend_type};
		bufs = conn.fetch(start, end, channel_list);
		assert bufs.length == 3;
		for (int i = 0; i < bufs.length; i++) {
		    buffer buf = bufs[i];
		    assert buf.getGpsSeconds() == start;
		    assert buf.getGpsNanoseconds() == 0;

		    channel channel = buf.getChannel();
		    int chantype = channel.getDataType();
		    Object dataraw = buf.getData();

		    channel cross_check = conn.findChannels(channel_list[i])[0];
		    assert chantype == cross_check.getDataType();
		    assert chantype == channel.DATA_TYPE_INT32;

		    if (chantype == channel.DATA_TYPE_INT32) {
		    int[] data = (int[]) dataraw;

			assert data.length == samples * (channel.getSampleRate() * mult);

			int[] test = new int[data.length];
			Arrays.fill(test, nBytepatterns[i].getInt(0));
			assert Arrays.equals(data, test);
		    } else {
		    	// unexpecte type
		    	assert false;
		    }
		}
	}

	// connection automatically closed when garbage collected,
	// but test explicit close anyway
	conn.close();
	System.exit( 0 );
    }
}
