/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;
import nds2.channel;

public final class test_ticket_289_java {
    private static unittest unit_test;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/*
	 * Need to adjust according to parameter
	 */

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port );
	conn.setParameter("ALLOW_DATA_ON_TAPE", "true");
	conn.setParameter("ITERATE_USE_GAP_HANDLERS", "false");

	/* ------------------------------------------------------------- *
	 * Ticket 289
	 * ------------------------------------------------------------- */

	channel[] chans = conn.findChannels( "*", channel.CHANNEL_TYPE_RDS );

	unit_test.check( chans.length, 3,
			 "Correct number of channels" );
	unit_test.check( chans[0].name( ), "X1:PEM-1",
			 "Validate channel zero name" );
	unit_test.check( chans[0].dataType( ), channel.DATA_TYPE_UNKNOWN,
			 "Validate channel zero data type" );
	unit_test.check( chans[2].name( ), "X1:PEM-3",
			 "Validate channel two name" );
	unit_test.check( chans[2].dataType( ), channel.DATA_TYPE_FLOAT32,
			 "Validate channel two data type" );

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}
