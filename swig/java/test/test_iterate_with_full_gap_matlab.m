% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;
use_gap_handler = 'true';
gap_handler = 'STATIC_HANDLER_NEG_INF';

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = nds2.connection(hostname, port, protocol );
conn.setParameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
conn.setParameter( 'GAP_HANDLER', gap_handler );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1218312574;
STOP = 1218312578;
STRIDE = 2;
CHANNELS = { 'X1:PEM-1' };

iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasNext() )
    bufs = iter.next( );

    if bufs( 1 ).getGpsSeconds( ) == START
        expected = -2147483648;
        actual = bufs( 1 ).getData( );
        unit_test.check( expected, actual( 1 ), 'Verify data');
    else
        expected = 14;
        actual = bufs( 1 ).getData( );
        unit_test.check( expected, actual( 1 ), 'Verify data');
    end
end

% change to a different period where we expect X1:PEM-1 to be ambigious
% expect failure
exception_caught = 0;
try
    START = 1218312578;
    STOP = 1218312582;
    iter = conn.iterate( START, STOP, STRIDE, CHANNELS );
catch ME
    exception_caught = 1;
end

unit_test.check( 1, exception_caught, 'Check that the second iterate resulted in an exception.');

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );