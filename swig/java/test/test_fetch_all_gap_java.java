/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.buffer;
import nds2.connection;
import nds2.channel;

public final class test_fetch_all_gap_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

	connection conn = new connection(hostname, port, connection.PROTOCOL_TWO );
    unit_test.check( conn.setParameter( "GAP_HANDLER", "STATIC_HANDLER_NEG_INF" ), "Set the gap handler" );

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */

	int start = 1219524930;
	int end = start + 4;

	String[] channels = new String[]{
	    "X1:PEM-1"
	};

	buffer[] buffers = conn.fetch( start, end, channels );

    buffer buf = buffers[0];
    final int[] data= (int[])( buf.getData( ) );
    for (int i = 0; i < data.length; i++) {
        unit_test.check( data[i] == -2147483648,
                     "Validating delta in gap" );
    }
	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}