/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.buffer;
import nds2.channel;
import nds2.connection;
import nds2.nds2;
import nds2.parameters;

public final class test_fetch_java_standalone {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

    parameters params = new parameters(hostname, port, connection.PROTOCOL_ONE );

	boolean caught_error = false;

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */
	   
	int start = 1108835634;
	int end = start + 4;

	String[] channels = new String[]{
	    "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
	    "H1:SUS-BS_BIO_M1_MSDELAYON",
	    "H1:SUS-BS_COMMISH_STATUS",
	    "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
	    "H1:SUS-BS_DCU_ID",
	    "H1:SUS-BS_DITHERP2EUL_1_1",
	    "H1:SUS-BS_DITHERP2EUL_2_1",
	    "H1:SUS-BS_DITHERY2EUL_1_1",
	    "H1:SUS-BS_DITHERY2EUL_2_1",
	    "H1:SUS-BS_DITHER_P_IPCERR"
	};

	buffer[] data = nds2.fetch( params, start, end, channels );

	unit_test.exit( );
    }
}