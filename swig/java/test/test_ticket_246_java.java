/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_ticket_246_java {
    private static unittest unit_test;

    private static float[] letOriginalDataGoOutOfScope( connection conn )
    {
	buffer[] buf = conn.fetch( 1163880064,
				   1163880064+30,
				   new String[]{ "H1:DAQ-BCST0_FAST_DATA_CRC" } );
	float[] data = (float[])( buf[0].getData( ) );
	unit_test.check( data[0],
			 data[1],
			 "Verify first data pair after fetch" );
	unit_test.check( data[1],
			 data[2],
			 "Verify second data pair after fetch" );
	return (float[])( buf[ 0 ].getData( ) );
    }

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/*
	 * Need to adjust according to parameter
	 */
	int protocol = connection.PROTOCOL_TWO;

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	float[] data = letOriginalDataGoOutOfScope( conn );
	unit_test.check( data[0],
			 data[1],
			 "Verify first data pair on returned data" );
	unit_test.check( data[1],
			 data[2],
			 "Verify second data pair on returned data" );

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}