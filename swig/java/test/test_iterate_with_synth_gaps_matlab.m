% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global START;

unit_test = unittest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';
NO_GAPS=true;
set_handlers = true;

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasOption( '-proto-1' ) )
  protocol = nds2.connection.PROTOCOL_ONE;
end
if ( unit_test.hasOption( '-proto-2' ) )
  protocol = nds2.connection.PROTOCOL_TWO;
end
if ( unit_test.hasOption( '-default-gap-handling' ) )
  set_handlers = false;
  NO_GAPS = false;
elseif ( unit_test.hasOption( '-no-gap' ) )
  use_gap_handler = 'false';
  NO_GAPS = false;
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port, protocol );
if ( set_handlers )
  conn.setParameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
  conn.setParameter( 'GAP_HANDLER', 'STATIC_HANDLER_ZERO' );
end

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1770000000;
STOP = START + 70;
STRIDE = 10;
CHANNELS = { 'X1:PEM-1',
	     'X1:PEM-2'
	    };
EXPECTED_I = 6;

last_start = 0;
cur_gps = START;

i = 1;
iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasNext( ) )
  bufs = iter.next( );
  comp = bufs(1).getGpsSeconds( ) >= cur_gps;
  unit_test.check( true, comp, ...
 		  [ sprintf( 'Verifying GPS second of buffer 1: %d >?= %d', ...
 			    bufs(1).getGpsSeconds( ), ...
 			    cur_gps ) ] );
  if( NO_GAPS )
    unit_test.check( bufs(1).getGpsSeconds( ), ...
 		    cur_gps, ...
		    'Verifying GPS second of buffer 1 when no gaps' );
  end
  delta = bufs(1).stop( ) - bufs(1).start( );
  %----------------------------------------------------------------------
  unit_test.check( bufs(1).samples( ), ...
 		   256 * delta, ...
 		   'Verifying samples of buffer 1: ' );
  unit_test.check( true, ...
		   [ checkData_iwsgm( bufs(1), 1.5 ) ], ...
  		   'Verifying data of buffer 1: ' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).samples( ), ...
 		   512 * delta, ...
 		   'Verifying samples of buffer 2: ' );
  unit_test.check( true, ...
		   [checkData_iwsgm( bufs(2), 2.75 ) ], ...
 		   'Verifying data of buffer 2: ' );
  %----------------------------------------------------------------------
  cur_gps = bufs(1).stop( );
  i = i + 1;
end
unit_test.check( true, [ i >= EXPECTED_I ], ...
 		 'Verifying number of iterations' );
if ( NO_GAPS )
  unit_test.check( cur_gps, ...
 		   STOP, ...
 		   'Verifying final GPS time' );
end
 
%% 	/* ------------------------------------------------------------- *
%% 	    Make sure we can do other operations
%% 	 * ------------------------------------------------------------- */

conn.setEpoch( START, STOP );
conn.findChannels( 'X1:PEM-1' );
conn.findChannels( 'X1:PEM-2' );
 
%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );
 
