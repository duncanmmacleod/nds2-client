% -*- mode: octave -*-
% -------------------------------------------------------------------
%   Simple test to verify availability function
% -------------------------------------------------------------------

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_ONE;

conn = nds2.connection(hostname, port, protocol );

chans = conn.findChannels('*', nds2.channel.CHANNEL_TYPE_TEST_POINT);
test_cond = false;
if (length(chans) > 0)
    test_cond = true;
end
unit_test.check( test_cond, true, 'Verify that there are test points');

chans = conn.findChannels('X1:TEST_POINT_2*');
unit_test.check( length(chans), 1, 'Verify that there is 1 X1:TEST_POINT_2*');
unit_test.check( chans(1).type(), nds2.channel.CHANNEL_TYPE_TEST_POINT, 'Verify that the channel type is a test point');

chans = conn.findChannels('X1:TEST_POINT_1*');
tp = 0;
mtrend = 0;
strend = 0;
online = 0;
other = 0;

for index = 1:length(chans)
    if chans(index).name == 'X1:TEST_POINT_1'
        unit_test.check( chans(index).type(), nds2.channel.CHANNEL_TYPE_TEST_POINT, 'Verify that it is a testpoint');
    elseif startsWith( chans(index).name(), 'X1:TEST_POINT_1' ) == 1
        test_cond = true;
        if chans(index).type() == nds2.channel.CHANNEL_TYPE_TEST_POINT
            false;
        end
        unit_test.check( test_cond, true, 'Verify that it is not a testpoint');
    end
    if chans(index).type() == nds2.channel.CHANNEL_TYPE_TEST_POINT
        tp = tp + 1;
    elseif chans(index).type() == nds2.channel.CHANNEL_TYPE_MTREND
        mtrend = mtrend + 1;
    elseif chans(index).type() == nds2.channel.CHANNEL_TYPE_STREND
        strend = strend + 1;
    elseif chans(index).type() == nds2.channel.CHANNEL_TYPE_ONLINE
        online = online + 1;
    else
        other = other + 1;
    end
end
unit_test.check( tp, 1, 'Verify that there was 1 test point');
unit_test.check( online, 1, 'Verify that there was 1 online channel');
unit_test.check( mtrend, 5, 'Verify that there were 5 mtrend channels');
unit_test.check( strend, 5, 'Verify that there were 5 strend channels');
unit_test.check( other, 0, 'Verify that there were no other channels');

iter = conn.iterate({'X1:TEST_POINT_1'});
iter.hasNext( );
bufs = iter.next( );
unit_test.check( bufs(1).type(), nds2.channel.CHANNEL_TYPE_TEST_POINT, 'Verify that buffers get set to test point as well');

conn.close();
exit( unit_test.exit_code());