import java.lang.Integer;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;


public class unittest {
    private int Status = 0;
    private int VerboseLevel = 0;
    private Set<String> Options;

    public unittest( ) {
	Status = 0;
	VerboseLevel = 0;
    }

    public unittest( String[] args ) {
	Status = 0;
	VerboseLevel = 0;
	Options
	    = new HashSet<String>( Arrays.asList( args ) );
    }

    public void check( boolean condition, String frmt, Object... args )
    {
	String lead;

	if ( condition )
	{
	    lead = "-- PASS: ";
	}
	else
	{
	    lead = "-- FAIL: ";
	    Status = 1;
	}
	String text = lead + String.format( frmt, args );
	message( text );
    }

    public void check( boolean actual, boolean expected, String lead )
    {
	check( actual == expected,
	       lead + "actual(%b) =?= expected(%b)",
	       actual, expected );
    }

    public void check( long actual, long expected, String lead )
    {
	check( actual == expected,
	       lead + "actual(%d) =?= expected(%d)",
	       actual, expected );
    }

    public void check( float actual, float expected, String lead )
    {
	final float EPSILON = (float)( 0.0001 );

	Boolean comparison = ( actual == expected )
	    ? true
	    : Math.abs( actual - expected ) < EPSILON;

	check( comparison,
	       lead + "actual(%f) =?= expected(%f) [diff: %g]: ",
	       actual, expected, Math.abs( actual - expected ) );
    }

    public void check( double actual, double expected, String lead )
    {
	final double EPSILON = 0.0001;

	Boolean comparison = ( actual == expected )
	    ? true
	    : Math.abs( actual - expected ) < EPSILON;

	check( comparison,
	       lead + "actual(%g) =?= expected(%g) [diff: %g]: ",
	       actual, expected, Math.abs( actual - expected ) );
    }

    public void check( int actual, int expected, String lead )
    {
	check( actual == expected,
	       lead + "actual(%d) =?= expected(%d)",
	       actual, expected );
    }

    public void check( String actual, String expected, String lead )
    {
	check( actual.equals( expected ),
	       lead + "actual(%s) =?= expected(%s)",
	       actual, expected );
    }

    public void exit( )
    {
	System.out.flush( );
	System.exit( Status );
    }

    public int exit_code( )
    {
	return( Status );
    }

    public static String hostname( )
    {
	return getenv( "NDS_TEST_HOST", "localhost" );
    }

    public void message( String text )
    {
	System.out.println( text );
	System.out.flush( );
    }

    public void msgInfo( String frmt, Object... args )
    {
	String text = "-- INFO: " + String.format( frmt, args );
	message( text );
    }

    public boolean hasOption( String Option )
    {
	return( Options.contains( Option ) );
    }

    public static int port( )
    {
	return( new Integer( getenv( "NDS_TEST_PORT", "31200" ) ) );
    }

    private static String getenv( String name, String dflt )
    {
	String retval = "";

	try
	{
	    retval = System.getenv( name );
	}
	catch( Exception e )
	{
	}
	if ( retval == null || retval.isEmpty( ) )
	{
	    retval = dflt;
	}
	return retval;
    }
}