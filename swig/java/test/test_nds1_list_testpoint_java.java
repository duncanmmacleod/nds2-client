/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.channel;
import nds2.buffer;

public final class test_nds1_list_testpoint_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

	connection conn = new connection(hostname, port, connection.PROTOCOL_ONE);

	channel[] chans = conn.findChannels("*", channel.CHANNEL_TYPE_TEST_POINT);
	unit_test.check( chans.length > 0, "Validating that a test point was returned");

	chans = conn.findChannels("X1:TEST_POINT_2*");
	unit_test.check( chans.length == 1, "Validating that a single channel was returned");
	unit_test.check( chans[0].type() == channel.CHANNEL_TYPE_TEST_POINT,
	    "Verifying that the returned channel was a test point.");

    chans = conn.findChannels("X1:TEST_POINT_1*");
    int tp = 0;
    int mtrend = 0;
    int strend = 0;
    int online = 0;
    int other = 0;

    for (int i = 0; i < chans.length; i++)
    {
        channel chan = chans[i];

        if (chan.name().equals("X1:TEST_POINT_1"))
        {
            unit_test.check( chan.type() == channel.CHANNEL_TYPE_TEST_POINT,
                "verify that the channel is a test point");
        }
        else if (chan.name().startsWith("X1:TEST_POINT_1"))
        {
            unit_test.check( chan.type() != channel.CHANNEL_TYPE_TEST_POINT,
                "verify that the channel is not a test point");
        }
        if (chan.type() == channel.CHANNEL_TYPE_TEST_POINT) {
            tp++;
        }
        else if (chan.type() == channel.CHANNEL_TYPE_MTREND) {
            mtrend++;
        }
        else if (chan.type() == channel.CHANNEL_TYPE_STREND) {
            strend++;
        }
        else if (chan.type() == channel.CHANNEL_TYPE_ONLINE) {
            online++;
        } else {
            other++;
        }
    }
    unit_test.check( tp == 1 , "Check that there was 1 test point");
    unit_test.check( online == 1, "Check that there was 1 online channel");
    unit_test.check( mtrend == 5, "Check that there were 5 mtrend channels");
    unit_test.check( strend == 5, "Check that there were 5 strend channels");
    unit_test.check( other == 0, "Check that there were no other channels");

    String[] channel_list = new String[]{
        "X1:TEST_POINT_1"
    };
    for (buffer[] bufs : conn.iterate(channel_list)) {
        unit_test.check( bufs[0].type() == channel.CHANNEL_TYPE_TEST_POINT, "Verify that iterate returns test point type buffers");
        break;
    }

	conn.close();
	unit_test.exit();
	}
}
