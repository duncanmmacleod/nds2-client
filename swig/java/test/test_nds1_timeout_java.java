/* ------------------------------------------------------------------- *
 *   Simple test to verify version function
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.channel;

public final class test_nds1_timeout_java {
    private static String getenv( String name, String dflt )
    {
	String retval = "";

	try
	{
	    retval = System.getenv( name );
	}
	catch( Exception e )
	{
	}
	if ( retval == null || retval.isEmpty( ) )
	{
	    retval = dflt;
	}
	return retval;
    }

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	String host = getenv( "NDS_TEST_HOST", "localhost" );
	String portstr = getenv( "NDS_TEST_PORT", "31200" );
	int port = Integer.parseInt( portstr );
	int protocol = 1;

	connection c = new connection( host, port, protocol );
	   
	try
	{
	   channel[] lst = c.findChannels("*");
	}
	catch( Exception e )
	{
	   System.out.println( "Caught Runtime error, should be from a timeout, but we currently cannot get a good error message" );
	   System.exit(0);
	}
	System.out.println( "Did not test timeouts!!!!" );
	System.exit( 1 );
    }
}