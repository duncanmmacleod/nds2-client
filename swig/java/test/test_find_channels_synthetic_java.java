/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.channel;

public final class test_find_channels_synthetic_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port );

	boolean caught_error = false;

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */
	   
	channel[] lst = conn.findChannels("*");
	unit_test.check( lst.length,
			 5,
			 "Validating number of channels for pattern '*'" );

	String[] chans = new String[]{ "<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
				       "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
				       "<X1:PEM-2 (256Hz, RAW, FLOAT32)>",
				       "<X1:PEM-2 (1Hz, STREND, FLOAT32)>",
				       "<X1:PEM-3 (256Hz, RAW, FLOAT32)>"
	};

	for(  int i = 0; i < chans.length; ++i )
	{
	    unit_test.check( chans[i],
			     lst[ i ].toString( ),
			     "Validating all channel names for pattern '*'" );
	}

	lst = conn.findChannels("X1*1");
	unit_test.check( lst.length,
			 2,
			 "Validating number of channels for pattern 'X1*1'" );

	chans = new String[]{ "<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
			      "<X1:PEM-1 (1Hz, STREND, FLOAT32)>"
	};

	for(  int i = 0; i < chans.length; ++i )
	{
	    unit_test.check( chans[i],
			     lst[ i ].toString( ),
			     "Validating all channel names for pattern 'X1*1'" );
	}

	lst = conn.findChannels("X1*", channel.CHANNEL_TYPE_STREND);
	unit_test.check( lst.length,
			 2,
			 "Validating number of strend channels for pattern 'X1*'" );


	chans = new String[]{ "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
			      "<X1:PEM-2 (1Hz, STREND, FLOAT32)>"
	};

	for(  int i = 0; i < chans.length; ++i )
	{
	    unit_test.check( chans[i],
			     lst[ i ].toString( ),
			     "Validating all strend channel names for pattern 'X1*'" );
	}

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}