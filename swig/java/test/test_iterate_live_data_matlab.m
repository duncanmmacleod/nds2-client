% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global CHANNELS;
global EXPECTED_I

unit_test = unittest( argv );

EXPECTED_I = 4;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasOption( '-proto-1' ) )
  protocol = nds2.connection.PROTOCOL_ONE;
  CHANNELS = { 'X1:PEM-1', ...
	       'X1:PEM-2', ...
	      };
end
if ( unit_test.hasOption( '-proto-2' ) )
  protocol = nds2.connection.PROTOCOL_TWO;
  CHANNELS = { 'X1:PEM-1,online', ...
	       'X1:PEM-2,online', ...
	    };
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------

expected = { 1.5, 2.75 };

i = 0;
iter = conn.iterate(CHANNELS);
while( iter.hasNext( ) )
  bufs = iter.next( );
  %----------------------------------------------------------------------
  unit_test.check( bufs(1).samples( ), ...
		   256, ...
		   'Verifying samples of buffer 1' );
  unit_test.check( [ checkData_ildm( bufs(1), expected{1} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).samples( ), ...
		   512, ...
		   'Verifying samples of buffer 0' );
  unit_test.check( [ checkData_ildm( bufs(2), expected{2} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  i = i + 1;
  if ( i == EXPECTED_I )
    % On a real system this would continue indefinitly
    % however, this test is set to only send 5 seconds
    % of data.
    break;
  end
end
unit_test.check( i, ...
		 EXPECTED_I, ...
		 'Verifying number of iterations: ' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );
