/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_iterate_with_synth_gaps_java {

    private static unittest unit_test;
    private static boolean NO_GAPS = true;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	String use_gap_handler = "true";
	int port = unit_test.port( );
	boolean set_handlers = true;
	/* ----------------------------------------------------------- *
	   Need to adjust according to parameter
	 * ----------------------------------------------------------- */
	int protocol = connection.PROTOCOL_TWO;
	if ( unit_test.hasOption( "-proto-1" ) )
	{
	    protocol = connection.PROTOCOL_ONE;
	}
	if ( unit_test.hasOption( "-proto-2" ) )
	{
	    protocol = connection.PROTOCOL_TWO;
	}
	if ( ( unit_test.hasOption( "-default-gap-handling" ) ) )
	{
	    NO_GAPS = false;
	    set_handlers = false;
	}
	else if ( ( unit_test.hasOption( "-no-gap" ) ) )
	{
	    NO_GAPS = false;
	    use_gap_handler = "false";
	}

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );
	if ( set_handlers )
	{
	    conn.setParameter( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );
	    conn.setParameter( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );
	}

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */


	int start = 1770000000;
	int end = start + 70;
	int stride = 10;
	String[] channels = new String[] {
	    "X1:PEM-1",
	    "X1:PEM-2",
	};

	long last_start = 0;
	int cur_gps = start;
	int i = 0;

	for( buffer[] bufs : conn.iterate(start, end, stride, channels) )
	{
	    unit_test.check( bufs[0].getGpsSeconds( ) >= cur_gps,
			     "Verifying GPS second of buffer 0: %d >?= %d",
			     bufs[0].getGpsSeconds( ),
			     cur_gps);
	    if( NO_GAPS )
	    {
		unit_test.check( bufs[0].getGpsSeconds( ),
				 cur_gps,
				 "Verifying GPS second of buffer 0 when no gaps" );
	    }
	    int delta = bufs[0].stop( ) - bufs[0].start( );
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[0].samples( ),
			     256 * delta,
			     "Verifying samples of buffer 0: " );
	    unit_test.check( checkData( bufs[0], 1.5 ),
			     "Verifying data of buffer 0: " );
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[1].samples( ),
			     512 * delta,
			     "Verifying samples of buffer 1: " );
	    unit_test.check( checkData( bufs[1], 2.75 ),
			     "Verifying data of buffer 1: " );
	    /* ------------------------------------------------------- */
	    cur_gps = bufs[0].stop( );
	    ++i;
	}
	unit_test.check( i >= 5,
			 "Verifying number of iterations" );
	if ( NO_GAPS )
	{
	    unit_test.check( cur_gps,
			     end,
			     "Verifying final GPS time" );
	}

	/* ------------------------------------------------------------- *
	    Make sure we can do other operations
	 * ------------------------------------------------------------- */

	conn.setEpoch( start, end );
	conn.findChannels( "X1:PEM-1" );
	conn.findChannels( "X1:PEM-2" );

	/* ------------------------------------------------------------- *
	   Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }

    static boolean isGapSecond( int GPS )
    {
	int gps = GPS - 1770000000;
	if ( (gps < 1)
	     || (gps == 9 )
	     || (gps == 18 )
	     || (gps >= 32 && gps < 40)
	     || (gps >= 50 && gps < 61)
	     || (gps > 67) )
	{
	    return true;
	}
	return( false );
    }

    static boolean checkData( buffer Buf, double Value )
    {
	int samples_per_sec = (int)Buf.getSampleRate( );
	final int CHANNEL_TYPE = Buf.getDataType( );

	unit_test.msgInfo( "samples_per_sec: %d", samples_per_sec );
	int offset = 0;
	for( int cur_gps = Buf.start( ); cur_gps != Buf.stop( ); ++cur_gps )
	{
	    double expected = Value;
	    if ( isGapSecond( cur_gps ) )
	    {
		expected = 0.0;
	    }
	    for( int i = 0; i < samples_per_sec; ++i )
	    {
		if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT32 )
		{
		    if ( ((float[])Buf.getData( ))[offset] != (float)expected )
		    {
			return( false );
		    }
		}
		else if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT64 )
		{
		    if ( ( (double[])Buf.getData( ))[offset] != expected )
		    {
			return( false );
		    }
		}
		offset++;
	    }
	}
	return( true );
    }
}