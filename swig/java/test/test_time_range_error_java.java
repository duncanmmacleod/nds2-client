/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_time_range_error_java {

    private static unittest unit_test;

    public static void main( String[] args )
        throws java.io.UnsupportedEncodingException
    {
        unit_test = new unittest( args );

        String hostname = unit_test.hostname( );
        String use_gap_handler = "true";
        int port = unit_test.port( );
        /* ----------------------------------------------------------- *
           Need to adjust according to parameter
         * ----------------------------------------------------------- */
        int protocol = connection.PROTOCOL_TWO;
        if ( unit_test.hasOption( "-proto-1" ) )
        {
            protocol = connection.PROTOCOL_ONE;
        }
        if ( unit_test.hasOption( "-proto-2" ) )
        {
            protocol = connection.PROTOCOL_TWO;
        }
        if ( unit_test.hasOption( "-no-gap" ) )
        {
            use_gap_handler = "false";
        }

        /* ------------------------------------------------------------- *
           Establish the connection
         * ------------------------------------------------------------- */

        connection conn = new connection(hostname, port, protocol );
        conn.setParameter( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );

        /* ------------------------------------------------------------- *
           Run the test
         * ------------------------------------------------------------- */


        boolean correct_exception = false;
        int start = 1770000000;
        int end = start + 20;
        int stride = 10;
        String[] channels = new String[] {
            "X1:PEM-1",
            "X1:PEM-2",
        };

        long last_start = 0;
        int cur_gps = start;

        try
        {
            correct_exception = false;
            for( buffer[] bufs : conn.iterate(end, start, stride, channels) )
            {
            }
            unit_test.check( true, false, "No exception thrown" );
        }
        catch ( IllegalArgumentException e )
        {
            correct_exception = true;
        }
        catch( Exception e )
        {
        }
        unit_test.check( correct_exception, true, "Correct exception thrown" );

        for( buffer[] bufs : conn.iterate(start, end, stride, channels) )
        {
        }

        /* ------------------------------------------------------------- *
           Make sure we can do other operations
         * ------------------------------------------------------------- */

        try
        {
            correct_exception = false;
            conn.setEpoch( end, start );
            conn.findChannels( "X1:PEM-1" );
            conn.findChannels( "X1:PEM-2" );
            unit_test.check( true, false, "No exception thrown" );
        }
        catch ( IllegalArgumentException e )
        {
            correct_exception = true;
        }
        catch( Exception e )
        {
        }
        unit_test.check( correct_exception, true, "Correct exception thrown" );

        conn.setEpoch( start, end );
        conn.findChannels( "X1:PEM-1" );
        conn.findChannels( "X1:PEM-2" );

        /* ------------------------------------------------------------- *
           Finish
         * ------------------------------------------------------------- */
        conn.close( );

        unit_test.exit( );
    }
}
