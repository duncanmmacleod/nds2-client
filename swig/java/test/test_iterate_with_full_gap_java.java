/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.buffer;
import nds2.connection;
import nds2.channel;

public final class test_iterate_with_full_gap_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

	connection conn = new connection(hostname, port, connection.PROTOCOL_TWO );
    unit_test.check( conn.setParameter( "ITERATE_USE_GAP_HANDLERS", "True" ), "Enable the gap handler for iterate" );
    unit_test.check( conn.setParameter( "GAP_HANDLER", "STATIC_HANDLER_NEG_INF" ), "Set the gap handler" );

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */

	int start = 1218312574;
	int end = 1218312578;

	String[] channels = new String[]{
	    "X1:PEM-1"
	};

    /* this iterate has a leading gap which should be gap filled */
    for( buffer[] buffers: conn.iterate(start, end, 2, channels ) ) {
        buffer buf = buffers[0];
        final int[] data= (int[])( buf.getData( ) );
        int expectedValue = -2147483648;
        if (buf.start() != start) {
            expectedValue = 14;
        }
        for (int i = 0; i < data.length; i++) {
            unit_test.check( data[i] == expectedValue,
                         "Validating data" );
        }
    }
    /* Now change the parameters and request a period that
     * does not have a unique X1:PEM-1, this should fail
     */
    start = 1218312578;
    end = 1218312582;
    boolean exceptionRaised = false;
    try {
        for ( buffer[] bufs:conn.iterate(start, end, 2, channels ) ) {
            unit_test.check( false, "Shouldn't reach this point");
        }
        unit_test.check( false, "Shouldn't reach this point");
    } catch(Exception e) {
        exceptionRaised = true;
    }
    unit_test.check( exceptionRaised, "Verify that the second iterate raised an exception");


	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}