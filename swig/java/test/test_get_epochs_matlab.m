% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

argv
unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;

if ( strcmp( argv, '-proto-1' ) )
    protocol = nds2.connection.PROTOCOL_ONE;
elseif ( strcmp( argv, '-proto-2' ) )
	protocol = nds2.connection.PROTOCOL_TWO;
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
conn = nds2.connection(hostname, port, protocol );
	   
cur = conn.currentEpoch( );

%------------------------------------------------------------------------
% Verify default as returning ALL epocs
%------------------------------------------------------------------------
unit_test.check( cur.getGpsStart( ), ...
		 0, ...
		 'Check GPS start of current epoch: ' );
unit_test.check( cur.getGpsStop( ), ...
		 1999999999, ...
		 'Check GPS start of current epoch: ' );

%------------------------------------------------------------------------
% Verify list of epoch data
%------------------------------------------------------------------------
available_epochs = conn.getEpochs( );
expected_epochs = getExpectedEpochs( conn.getProtocol( ) );
unit_test.check( length( expected_epochs ), ...
		 available_epochs.size( ), ...
		 'Verify size of epoch lists' );
%------------------------------------------------------------------------
% Protocol dependent behaviors
%------------------------------------------------------------------------
if ( protocol == nds2.connection.PROTOCOL_TWO )
  %----------------------------------------------------------------------
  % Test all epochs (except 'NONE') by name
  %----------------------------------------------------------------------
  for  i = 1:length(expected_epochs)
    epoch = available_epochs.get( i - 1 );
    if ( strcmp( epoch.getName( ), 'NONE' ) )
      continue;
    end
    conn.setEpoch( epoch.getName( ) );

    cur = conn.currentEpoch( );
%		unit_test.check( epoch.getGpsStart( ),
%				 cur.getGpsStart( ),
%				 'Epics by name GPS start' );
%		unit_test.check( epoch.getGpsStop( ),
%				 cur.getGpsStop( ),
%				 'Epics by name GPS stop' );
  end % for - name
  %----------------------------------------------------------------------
  % Test all epochs (except 0-0) by time range
  %----------------------------------------------------------------------
  for i = 1:length(expected_epochs)
    epoch = available_epochs.get( i - 1);
    if ( epoch.getGpsStop( ) == 0 )
      continue;
    end
    conn.setEpoch( epoch.getGpsStart( ), ...
		  epoch.getGpsStop( ) );

    cur = conn.currentEpoch( );
%		unit_test.check( epoch.getGpsStart( ),
%				 cur.getGpsStart( ),
%				 'Epics by range GPS start' );
%		unit_test.check( epoch.getGpsStop( ),
%				 cur.getGpsStop( ),
%				 'Epics by range GPS stop' );
  end % for - time range
end % PROTOCOL_TWO
%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

exit( unit_test.exit_code( ) );


