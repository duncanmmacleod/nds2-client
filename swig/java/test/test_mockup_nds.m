%
% Unit tests against mock-up NDS 1 server
%
% Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
%

bytepatterns = {
    cast([hex2dec('B0'); hex2dec('00')], 'uint8'),
    cast([hex2dec('DE'); hex2dec('AD'); hex2dec('BE'); hex2dec('EF')], 'uint8'),
    cast([hex2dec('00'); hex2dec('00'); hex2dec('00'); hex2dec('00');
	  hex2dec('FE'); hex2dec('ED'); hex2dec('FA'); hex2dec('CE')], 'uint8')
};

port = str2num(getenv('NDS_TEST_PORT'))

conn = nds.connection('localhost', port, nds.connection.PROTOCOL_ONE)
assert (conn.getPort() == port)
assert (conn.getProtocol() == 1)

channels = conn.findChannels('X*:{A,B,C}')
assert (length(channels) == 3)
assert (strcmp(channels(1).toString(), '<X1:A (16Hz, ONLINE, INT16)>'))
assert (strcmp(channels(2).toString(), '<X1:B (16Hz, ONLINE, INT32)>'))
assert (strcmp(channels(3).toString(), '<X1:C (16Hz, ONLINE, INT64)>'))

bufs = conn.fetch(1000000000, 1000000004, {'X1:A', 'X1:B', 'X1:C'})
assert (length(bufs) == 3)
for i = 1:length(bufs)
    buf = bufs(i);
    bytepattern = bytepatterns{i};
    assert (buf.getGpsSeconds() == 1000000000)
    assert (buf.getGpsNanoseconds() == 0)
    assert (length(buf.getData()) == 4 * buf.getChannel().getSampleRate())
    assert (all(typecast(buf.getData(), 'uint8') == repmat(bytepattern, length(buf.getData()), 1)))
end

conn.iterate({'X1:A', 'X1:B', 'X1:C'})
bufs = conn.next()
assert (length(bufs) == 3)
for i = 1:length(bufs)
    buf = bufs(i);
    bytepattern = bytepatterns{i};
    assert (buf.getGpsSeconds() == 1000000000)
    assert (buf.getGpsNanoseconds() == 0)
    assert (length(buf.getData()) == buf.getChannel().getSampleRate())
    assert (all(typecast(buf.getData(), 'uint8') == repmat(bytepattern, length(buf.getData()), 1)))
end

% connection automatically closed when garbage collected,
% but test explicit close anyway
conn.close()
