/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.buffer;
import nds2.connection;
import nds2.channel;

public final class test_fetch_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, connection.PROTOCOL_ONE );

	boolean caught_error = false;

	/* ------------------------------------------------------------- *
	   Run test
	 * ------------------------------------------------------------- */
	   
	int start = 1108835634;
	int end = start + 4;

	channel[] lst = conn.findChannels("H1:SUS-BS*");

	String[] channels = new String[]{
	    "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
	    "H1:SUS-BS_BIO_M1_MSDELAYON",
	    "H1:SUS-BS_COMMISH_STATUS",
	    "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
	    "H1:SUS-BS_DCU_ID",
	    "H1:SUS-BS_DITHERP2EUL_1_1",
	    "H1:SUS-BS_DITHERP2EUL_2_1",
	    "H1:SUS-BS_DITHERY2EUL_1_1",
	    "H1:SUS-BS_DITHERY2EUL_2_1",
	    "H1:SUS-BS_DITHER_P_IPCERR"
	};

	buffer[] data = conn.fetch( start, end, channels );

	for( buffer ch1 : data )
	{
	    channel ch2 = conn.findChannels(ch1.name( ))[0];

	    /*
	      print "ch1={} ch2={}\n".format(str(ch1), str(ch2))
	    */

	    unit_test.check( ch1.name( ),
			     ch2.name( ),
			     "Validate name" );
	    unit_test.check( ch1.getDataType( ),
			     ch2.getDataType( ),
			     "Validate data type" );
	    unit_test.check( ch1.getChannelType( ),
			     ch2.getChannelType( ),
			     "Validate channel type" );
	    unit_test.check( ch1.getSampleRate( ),
			     ch2.getSampleRate( ),
			     "Validate sample rate" );
	    unit_test.check( ch1.getSignalGain( ),
			     ch2.getSignalGain( ),
			     "validate signal gain" );
	    unit_test.check( ch1.getSignalOffset( ),
			     ch2.getSignalOffset( ),
			     "validate signal offset" );
	    unit_test.check( ch1.getSignalSlope( ),
			     ch2.getSignalSlope( ),
			     "validate signal slope" );
	    unit_test.check( ch1.getSignalUnits( ),
			     ch2.getSignalUnits( ),
			     "validate signal units" );
	}
	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}