/* ------------------------------------------------------------------- *
 *   Simple test to verify version function
 * ------------------------------------------------------------------- */

import java.lang.Math;

import nds2.buffer;
import nds2.channel;
import nds2.connection;

public final class test_dissimilar_gaps_java {
    private static final int START = 1112749800;
    private static final int STOP = 1112762400;
    private static final String[] CHANNELS
	= new String[] { "H1:ASC-INP2_Y_INMON.mean,m-trend",
	    "L1:ASC-INP2_Y_INMON.mean,m-trend",
	    "H1:ASC-INP2_Y_INMON.n,m-trend",
	    "L1:ASC-INP2_Y_INMON.n,m-trend" };

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/*
	 * Need to adjust according to parameter
	 */
	int protocol = connection.PROTOCOL_TWO;

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	   Gap Handler setup
	 * ------------------------------------------------------------- */
	conn.setParameter( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );

	buffer[] bufs = conn.fetch( START, STOP, CHANNELS );
	unit_test.check( bufs[0].dataType( ),
			 channel.DATA_TYPE_FLOAT64,
			 "Validating data type" );
	unit_test.check( bufs[0].samples( ),
			 ( STOP - START ) / 60,
			 "Validating sample rate" );
	   
	/* ------------------------------------------------------------- *
	   look for a zero filled gap at [1112752800-1112756400) on the
	   first channel
	 * ------------------------------------------------------------- */
	final int gap_start = 1112752800;
	final int gap_stop = 1112756400;
	final int start_index = (gap_start - START) / 60;
	final int stop_index = (gap_stop - START) / 60;
	final double[] data= (double[])( bufs[0].getData( ) );

	for ( int i = start_index; i < stop_index; ++i )
	{
	    unit_test.check( data[ i ] == 0.0,
			     "Validating gap data" );
	}
	/* ------------------------------------------------------------- *
	   look for  ~0.110272226 at time 1112752740 (sample prior to gap)
	   * ------------------------------------------------------------- */
	int index = (1112752740 - START) / 60;
	double delta = Math.abs( data[ index ] - 0.110272226 );

	unit_test.check( delta <= 0.0000000001,
			 "Validating delta prior to gap" );
	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}