% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port );

%------------------------------------------------------------------------
% Run test
%------------------------------------------------------------------------
	   
lst = conn.findChannels('*');
unit_test.check( length(lst), ...
		 5, ...
		 'Validating number of channels for pattern "*"' );

chans = { '<X1:PEM-1 (256Hz, RAW, FLOAT32)>',
	  '<X1:PEM-1 (1Hz, STREND, FLOAT32)>',
	  '<X1:PEM-2 (256Hz, RAW, FLOAT32)>',
	  '<X1:PEM-2 (1Hz, STREND, FLOAT32)>',
	  '<X1:PEM-3 (256Hz, RAW, FLOAT32)>'
	 };

for i = 1:length(chans )
  unit_test.check( chans(i), ...
		   lst( i ).toString( ), ...
		   'Validating all channel names for pattern "*"' );
end

%------------------------------------------------------------------------

lst = conn.findChannels('X1*1');
unit_test.check( length(lst), ...
		 2, ...
		 'Validating number of channels for pattern "X1*1"' );

chans = { '<X1:PEM-1 (256Hz, RAW, FLOAT32)>',
	  '<X1:PEM-1 (1Hz, STREND, FLOAT32)>'
	 };

for i = 1:length(chans)
   unit_test.check( chans(i), ...
		    lst( i ).toString( ), ...
		    'Validating all channel names for pattern "X1*1"' );
end

%------------------------------------------------------------------------

lst = conn.findChannels('X1*', nds2.channel.CHANNEL_TYPE_STREND);
unit_test.check( length(lst), ...
		 2, ...
		 'Validating number of strend channels for pattern "X1"' );


chans = { '<X1:PEM-1 (1Hz, STREND, FLOAT32)>',
	  '<X1:PEM-2 (1Hz, STREND, FLOAT32)>'
	 };

for i = 1:length(chans)
  unit_test.check( chans(i), ...
		   lst( i ).toString( ), ...
		   'Validating all strend channel names for pattern "X1*"' );
end

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

exit( unit_test.exit_code( ) );
