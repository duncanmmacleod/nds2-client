% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = unittest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasOption( '-proto-1' ) )
  protocol = nds2.connection.PROTOCOL_ONE;
end
if ( unit_test.hasOption( '-proto-2' ) )
  protocol = nds2.connection.PROTOCOL_TWO;
end
if ( unit_test.hasOption( '-no-gap' ) )
  use_gap_handler = 'false';
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port, protocol );
conn.setParameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1770000000;
STOP = START + 20;
STRIDE = 10;
CHANNELS = { 'X1:PEM-1',
	     'X1:PEM-2'
	    };

last_start = 0;
cur_gps = START;
expected = { 1.5, 2.75 };

iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasNext( ) )
  bufs = iter.next( );
  %----------------------------------------------------------------------
  % MATLAB indexing starts at 1
  %----------------------------------------------------------------------
  unit_test.check( bufs( 1 ).getGpsSeconds( ), ...
		  cur_gps, ...
		  'Verifying GPS second of buffer 1' );
  unit_test.check( bufs( 1 ).samples( ), ...
		  256 * STRIDE, ...
		  'Verifying samples of buffer 1' );
  unit_test.check( true, ...
		   [ checkData_iadm( bufs( 1 ), expected{1} ) ], ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).getGpsSeconds( ), ...
		   cur_gps, ...
		   'Verifying GPS second of buffer 2' );
  unit_test.check( bufs(2).samples( ), ...
		   512 * STRIDE, ...
		   'Verifying samples of buffer 2' );
  unit_test.check( true, ...
		   [ checkData_iadm( bufs(2), expected{2} ) ], ...
		   'Verifying data of buffer 2' );
  %----------------------------------------------------------------------
  cur_gps = bufs(1).stop( );
  expected = { 3.5, 4.75 };
end

%------------------------------------------------------------------------
% Make sure we can do other operations
%------------------------------------------------------------------------

conn.setEpoch( START, STOP );
conn.findChannels( 'X1:PEM-1' );
conn.findChannels( 'X1:PEM-2' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );

