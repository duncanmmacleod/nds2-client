% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

try
  nds2;
  UnitTest;
catch err
  fdisp( stderr, ...
        sprintf( "Caught unexpected exception: %s\n", ...
		 err.message ) );
end

try
  unit_test_ARGV = argv;

  hostname = unit_test_hostname( );
  port = unit_test_port( );

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  conn = nds2.connection(hostname, port );
  conn.set_parameter("ALLOW_DATA_ON_TAPE", "true");
  conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false");

  %----------------------------------------------------------------------
  % Ticket 288
  %----------------------------------------------------------------------

  unit_test_message( "Made connection" );
  blockCount = 0;
  lastGPS = 0;
  expected = [ ...
	      66.7626, ...
	      0.0, ...
	      67.4257, ...
	      0.0, ...
	      0.0, ...
	      0.0 ...
	      ];
  actual = [ ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0 ...
	    ];
  start = 1167530460;
  finish =  1178294460;
  channels = { ...
	      "H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend" ...
	      };

  iter = conn.iterate( start, finish, channels );
  try
    while( true )
      bufs = iter.next( );
      blockCount = blockCount + 1;
      lastGPS = bufs{ 1 }.stop( );
      data = bufs{ 1 }.data( );
      actual( blockCount ) = data( 121 );
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror )
    end
  end
  unit_test_check( blockCount, 6, ...
		  "Correct number of blocks were returned" );
  unit_test_check( lastGPS, finish, ...
		  "Correct last GPS valued reported" );
  for i = 1:numel( expected )
    unit_test_check( actual( i ), ...
		    expected( i ), ...
		    "Verifying channel contents" );
  end

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------
  conn.close( );
catch err
  s = unit_test_dump_stack( );
  unit_test_check( false, ...
		  true, ...
		  sprintf( "Caught unexpected exception: %s\n%s", ...
			  err.message, ...
			  s ) );
end
% exit( unit_test_exitcode( ) );
