% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
nds2;
UnitTest;

global CHANNELS;
global EXPECTED_I;

EXPECTED_I = 4;

%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = isgapsecond( GPS )
  RETVAL = false;
end

function RETVAL = checkdata( Buf, Value )
  samples_per_sec = Buf.sample_rate( );
  CHANNEL_TYPE = Buf.data_type( );

  unit_test_msginfo( 'samples_per_sec: %d', ...
		     int32(samples_per_sec) );
  offset = 1;
  %======================================================================
  % MATLAB loops are inclusive so need to trim
  % the tail by 1 sec
  %======================================================================
  rs = Buf.start( );
  re = Buf.stop( ) - 1;
  for cur_gps = rs:re
    expected = Value;
    if ( isgapsecond( cur_gps ) )
      expected = 0.0;
    end
    d = Buf.data( );
    for i = 1:samples_per_sec
      if ( d( offset ) ~= expected )
	RETVAL = false;
	break
      end
      offset = offset + 1;
    end
  end
  RETVAL = true;
end

%------------------------------------------------------------------------
% Main
%------------------------------------------------------------------------
try
  hostname = unit_test_hostname( );
  port = unit_test_port( );
  protocol = 'unknown';

  %----------------------------------------------------------------------
  % Need to adjust according to parameter
  %----------------------------------------------------------------------
  if ( unit_test_hasoption( '-proto-1' ) )
    protocol = nds2.connection.PROTOCOL_ONE;
    CHANNELS = { 'X1:PEM-1', ...
		 'X1:PEM-2', ...
		};
  end
  if ( unit_test_hasoption( '-proto-2' ) )
    protocol = nds2.connection.PROTOCOL_TWO;
    CHANNELS = { 'X1:PEM-1,online', ...
		 'X1:PEM-2,online', ...
		 };
  end

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  conn = nds2.connection(hostname, port, protocol );

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------

  expected = { 1.5, 2.75 };

  i = 0;
  iter = conn.iterate(CHANNELS);
  try
    while( true )
      bufs = iter.next( );
      %------------------------------------------------------------------
      unit_test_check( bufs{1}.samples( ), ...
		       256, ...
		       'Verifying samples of buffer 1' );
      unit_test_check( [ checkdata( bufs{1}, expected{1} ) ], ...
		       true, ...
		       'Verifying data of buffer 1' );
      %------------------------------------------------------------------
      unit_test_check( bufs{2}.samples( ), ...
		       512, ...
		       'Verifying samples of buffer 2' );
      unit_test_check( [ checkdata( bufs{2}, expected{2} ) ], ...
		       true, ...
		       'Verifying data of buffer 2' );
      %------------------------------------------------------------------
      i = i + 1;
      if ( i == EXPECTED_I )
        % On a real system this would continue indefinitly
        % however, this test is set to only send 5 seconds
        % of data.
	break;
      end
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end
  unit_test_check( i, ...
		   EXPECTED_I, ...
		   'Verifying number of iterations: ' );

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------

  conn.close( );
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end
unit_test_exit( );
 
