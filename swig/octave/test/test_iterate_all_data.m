% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
nds2;
UnitTest;

hostname = unit_test_hostname( );
port = unit_test_port( );
protocol = 0;
use_gap_handler = "true";

%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = isgapsecond( GPS )
  RETVAL = false;
endfunction

function RETVAL = checkdata( Buf, Value )
  class(Buf)
  samples_per_sec = Buf.sample_rate( );
  CHANNEL_TYPE = Buf.data_type( );

  unit_test_msgdebug( 10, ...
		      "samples_per_sec: %d", ...
		      int32(samples_per_sec) );
  offset = 1;
  %======================================================================
  % MATLAB loops are inclusive so need to trim
  % the tail by 1 sec
  %======================================================================
  rs = Buf.start( );
  re = Buf.stop( ) - 1;
  for cur_gps = rs:re
    unit_test_msgdebug( 10, ...
		        "cur_gps: %d", ...
		        cur_gps );
    expected = Value;
    if ( isgapsecond( cur_gps ) )
      expected = 0.0;
    end
    d = Buf.data( );
    for i = 1:samples_per_sec
      if ( d( offset ) ~= expected )
	RETVAL = false;
	break
      end
      offset = offset + 1;
    end
  end
  RETVAL = true;
endfunction

try
  %----------------------------------------------------------------------
  % Need to adjust according to parameter
  %------------------------------------------------------------------------
  if ( unit_test_hasoption( "-proto-1" ) )
    protocol = nds2.connection.PROTOCOL_ONE;
    unit_test_msgdebug( 10, "Doing proto one" );
  end
  if ( unit_test_hasoption( "-proto-2" ) )
    protocol = nds2.connection.PROTOCOL_TWO;
    unit_test_msgdebug( 10, "Doing proto two" );
  end
  if ( unit_test_hasoption( "-no-gap" ) )
    use_gap_handler = "false";
    unit_test_msgdebug( 10, "Doing no gaps" );
  end

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  unit_test_msgdebug( 10, ...
		      "About to connection: %s - %d - %d", ...
		      hostname, port, protocol );
  conn = nds2.connection(hostname, port, protocol );
  unit_test_msgdebug( 10, ...
		      "Setting parameter: %s - %s", ...
		      "ITERATE_USE_GAP_HANDLERS", ...
		      use_gap_handler );
  conn.set_parameter( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );
  unit_test_msgdebug( 10, "Set parameters" );

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------
  START = 1770000000;
  STOP = START + 20;
  STRIDE = 10;
  CHANNELS = { "X1:PEM-1",
	       "X1:PEM-2"
	      };

  last_start = 0;
  cur_gps = START;
  expected = { 1.5, 2.75 };

  iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
  unit_test_msgdebug( 10, "Allocated iterator" );
  try
    while( true )
      bufs = iter.next( );
      %------------------------------------------------------------------
      % MATLAB indexing starts at 1
      %------------------------------------------------------------------
      unit_test_check( bufs{1}.start( ), ...
  		       cur_gps, ...
  		       "Verifying GPS second of buffer 1" );
      unit_test_check( bufs{1}.samples( ), ...
		       256 * STRIDE, ...
		       "Verifying samples of buffer 1" );
      unit_test_check( true, ...
    		       [ checkdata( bufs{1}, expected{1} ) ], ...
      		       "Verifying data of buffer 1" );
      %------------------------------------------------------------------
      unit_test_check( bufs{2}.start( ), ...
  		       cur_gps, ...
  		       "Verifying GPS second of buffer 2" );
      unit_test_check( bufs{2}.samples( ), ...
  		       512 * STRIDE, ...
  		       "Verifying samples of buffer 2" );
      unit_test_check( true, ...
 		       [ checkdata( bufs{2}, expected{2} ) ], ...
  		       "Verifying data of buffer 2" );
      %------------------------------------------------------------------
      cur_gps = bufs{1}.stop( );
      expected = { 3.5, 4.75 };
    end
  catch err
    if ( ! strcmp( err.message, "No Next (SWIG_IndexError)" ) )
      rethrow( lasterror );
    end
  end

  %----------------------------------------------------------------------
  % Make sure we can do other operations
  %----------------------------------------------------------------------

  conn.set_epoch( START, STOP );
  conn.find_channels( "X1:PEM-1" );
  conn.find_channels( "X1:PEM-2" );

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------
  conn.close( );
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end
unit_test_exit( );

