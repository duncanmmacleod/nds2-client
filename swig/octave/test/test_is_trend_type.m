% -*- mode: octave -*-
% -----------------------------------------------------------------------
%   Simple test to verify version function
% -----------------------------------------------------------------------

path( )

nds2;

% -----------------------------------------------------------------------

function TREND = create_mTrends( )
  TREND = { "A,m-trend",
	   "X1:ABC,m-trend",
	   "X1:ABC.mean,m-trend",
	   "A:,m-trend"
	   };
endfunction

% -----------------------------------------------------------------------

function TREND = create_sTrends( )
  TREND = { "A,s-trend",
	   "X1:ABC,s-trend",
	   "X1:ABC.mean,s-trend",
	   "A:,s-trend"
	   };
endfunction

% -----------------------------------------------------------------------

function TREND = create_junk( )
  TREND = { "",
	   "s-trend",
	   ",s-trend",
	   "m-trend",
	   ",m-trend",
	   "S-TREND",
	   ",S-TREND",
	   "M-TREND",
	   ",M-TREND",
	   ",mtrend",
	   "STREND",
	   ",strend",
	   "MTREND",
	   "X1:ABCCDEFGs-trend",
	   "X1:ABCDEFGS-TREND",
	   "X1:ABCCDEFGm-trend",
	   "X1:ABCDEFGM-TREND",
	   "X1:ABCDEFGM"
	   };
endfunction

% -----------------------------------------------------------------------
%   Minute Trend
% -----------------------------------------------------------------------
trends = create_mTrends( );
  
for i = trends
  assert( nds2.channel.IsMinuteTrend( i{1} ) == true );
  assert( nds2.channel.IsSecondTrend( i{1} ) == false );
end

% -----------------------------------------------------------------------
%   Second Trend
% -----------------------------------------------------------------------
trends = create_sTrends( );
  
for i = trends
  assert( nds2.channel.IsMinuteTrend( i{1} ) == false );
  assert( nds2.channel.IsSecondTrend( i{1} ) == true );
end

% -----------------------------------------------------------------------
%   Junk Trend
% -----------------------------------------------------------------------
trends = create_junk( );
  
for i = trends
  assert( nds2.channel.IsMinuteTrend( i{1} ) == false );
  assert( nds2.channel.IsSecondTrend( i{1} ) == false );
end

