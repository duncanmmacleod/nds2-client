% -*- mode: octave -*-
nds2;
UnitTest;

host = unit_test_hostname( );
port = unit_test_port( );

protocol = nds2.connection.PROTOCOL_ONE;

retval = false;

try;
  c = nds2.connection( host, port, protocol );
  lst = c.find_channels( "*"  );
  c.close( );
catch except;
  retval = true;
end_try_catch;
unit_test_check( retval,
		true,
		'Validation of timeout handling' );

unit_test_exit( );
