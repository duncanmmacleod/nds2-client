% Import module
nds2;


try
	% GPS time for range
	gpsoffset =  315964818;
	gpsend = time() - gpsoffset;
	gpsstart = gpsend - 4;
	disp(sprintf('time: %d gpsstart: %d gpsend: %d', time(), gpsstart, gpsend));

	% Open connection and print some information about the server
	conn = nds2.connection('llodts0.ligo-la.caltech.edu');
	disp('Connection:'); disp(conn);
	disp(sprintf('Host: %s', conn.get_host()));
	disp(sprintf('Port: %d', conn.get_port()));
	disp(sprintf('Protocol: %d', conn.get_protocol()));

	% Get list of available channels and print some information about them
	channels = conn.find_channels('X2:HPI-BS*MON');
	disp(sprintf('Number matching of channels: %d', length(channels)));
	disp('First channel:'); disp(channels{1});
	disp(sprintf('First channel name: %s', channels{1}.name));

	% Fetch some data
	data = conn.fetch(gpsstart, gpsend, ...
			  {channels{1}.name, channels{2}.name, channels{3}.name});
	disp('A buffer:'); disp(data{1});
	disp('Its data:'); disp(data{1}.data);
	disp('Its channel:'); disp(data{1}.channel);

	disp('Now block by block...');
	conn.iterate(gpsstart, gpsend, 1, {channels{1}.name, channels{2}.name, channels{3}.name})
	while conn.has_next()
	    bufs = conn.next();
	    disp(bufs);
        end
catch
end_try_catch
