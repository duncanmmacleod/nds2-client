#ifndef NDS_OCTAVE_BUFFER_I
#define NDS_OCTAVE_BUFFER_I

// ======================================================================
//  Buffer object
// ======================================================================

%begin %{
#include "nds_buffer.hh"
using NDS::buffer;

%}

%{
static inline channel*
buffer_channel_get( buffer* Buf )
{
  return reinterpret_cast< channel * >( Buf );
}

static inline void
buffer_channel_set( buffer* Buf, const channel* Chan )
{
  throw std::runtime_error( "Channel member of Buffer class is not settable" );
}

static buffer::gps_second_type
buffer_gps_seconds_get( buffer* Value )
{
  return Value->Start( );
}

static buffer::gps_nanosecond_type
buffer_gps_nanoseconds_get( buffer* Value )
{
  return Value->StartNano( );
}

static octave_value
buffer_data_get( buffer* Value )
{
  const size_t                  SIZE( Value->Samples( ) );
  dim_vector                    dims( SIZE, 1 );
  const void*                   dp( Value->cbegin<void>() );
  octave_value                  retval;

  switch( Value->DataType( ) )
  {
  case NDS::channel::DATA_TYPE_INT16:
    {
      int16NDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(octave_int16));
      retval = arr;
    }
    break;
  case NDS::channel::DATA_TYPE_INT32:
    {
      int32NDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(octave_int32));
      retval = arr;
    } break;
  case NDS::channel::DATA_TYPE_INT64:
    {
      int64NDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(octave_int64));
      retval = arr;
    }
    break;
  case NDS::channel::DATA_TYPE_FLOAT32:
    {
      FloatNDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(float));
      retval = arr;
    }
    break;
  case NDS::channel::DATA_TYPE_FLOAT64:
    {
      NDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(double));
      retval = arr;
    }
    break;
  case NDS::channel::DATA_TYPE_COMPLEX32:
    {
      FloatComplexNDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(double));
      retval = arr;
    }
    break;
  case NDS::channel::DATA_TYPE_UINT32:
    {
      uint32NDArray arr(dims);
      memcpy((void *) arr.data(), dp, SIZE * sizeof(octave_uint32));
      retval = arr;
    }
    break;
  default:
    SWIG_exception(SWIG_TypeError, "Unknown NDS data type");
    break;
  }

fail:
  return retval;
}

%}

%extend buffer {
  std::string __str__( ) {
    return $self->NameLong( );
  }

  buffer::gps_second_type start( ) {
    return $self->Start( );
  }

  buffer::gps_second_type stop( ) {
    return $self->Stop( );
  }

  size_t samples( ) {
    return $self->Samples( );
  }
  const gps_second_type gps_seconds;
  const gps_second_type gps_nanoseconds;
  const octave_value 	data;

  const channel*	channel;
}

// ======================================================================
//  Collection of buffer Objects
// ======================================================================

%define %nds_buffers_type( )
%nds_vector_to_array(buffers_type,buffer);
%enddef /* %nds_buffers_type( ) */

#endif /* NDS_OCTAVE_BUFFER_I */
