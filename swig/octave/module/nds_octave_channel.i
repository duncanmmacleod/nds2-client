#ifndef NDS_OCTAVE_CHANNEL_I
#define NDS_OCTAVE_CHANNEL_I

%import "nds_octave_vector_to_array.i"

// ======================================================================
//   Channel Objects
// ======================================================================

%begin %{
#include "nds_channel.hh"

using NDS::channel;
%}

%{
#define ML(V,R,F)          \
  static inline R \
  channel_##V##_get( channel* Value ) \
  { \
    return Value->F( );\
  } //

#define M(V,F)                     \
  ML(V,channel::V##_type,F)

#define MS(V,F)                  \
  static inline const char*      \
  channel_##V##_get( channel* Value ) \
  { \
    return Value->F( ).c_str( );                \
  } //

  MS(name,Name);
  ML(channel_type,channel::channel_type,Type);
  ML(data_type,channel::data_type,DataType);
  M(sample_rate,SampleRate);
  M(signal_gain,Gain);
  M(signal_slope,Slope);
  M(signal_offset,Offset);
  MS(signal_units,Units);

#undef MS
#undef M
%}

#if 1
%define CHANNEL_TYPE( )
  %typedef int channel_type;
  %immutable;
  static int CHANNEL_TYPE_UNKNOWN = 0;		  ///< Unknown
  static int CHANNEL_TYPE_ONLINE = (1 << 0);	  ///< Online channel
  static int CHANNEL_TYPE_RAW = (1 << 1);	  ///< Raw channel
  static int CHANNEL_TYPE_RDS = (1 << 2);	  ///< Reduced data set
  static int CHANNEL_TYPE_STREND = (1 << 3);	  ///< Second trend
  static int CHANNEL_TYPE_MTREND = (1 << 4);	  ///< Minute trend
  static int CHANNEL_TYPE_TEST_POINT = (1 << 5);  ///< Test point data
  static int CHANNEL_TYPE_STATIC = (1 << 6);	  ///< Static data
  %mutable;
%enddef

%define CHANNEL_DATA_TYPE( )
  /// %typedef int data_type;
  %immutable;
  static int DATA_TYPE_UNKNOWN = 0;		///< Unkown
  static int DATA_TYPE_INT16 = (1 << 0);	///< 16 bit signed integer
  static int DATA_TYPE_INT32 = (1 << 1);	///< 32 bit signed integer
  static int DATA_TYPE_INT64 = (1 << 2);	///< 64 bit signed integer
  static int DATA_TYPE_FLOAT32 = (1 << 3);	///< 32 bit float value
  static int DATA_TYPE_FLOAT64 = (1 << 4);	///< 64 bit float value
  static int DATA_TYPE_COMPLEX32 = (1 << 5);	///< Complex value, two 32 bit floats
  static int DATA_TYPE_UINT32 = (1 << 6);	///< 32 bit unsigned integer value
  %mutable;
%enddef

%extend channel {
  CHANNEL_TYPE( )
  CHANNEL_DATA_TYPE( )

  std::string __str__( ) {
    return $self->NameLong( );
  }

  %immutable;
  char*                   name;
  channel_type            channel_type;
  data_type               data_type;
  sample_rate_type        sample_rate;
  signal_gain_type        signal_gain;
  signal_slope_type       signal_slope;
  signal_offset_type      signal_offset;
  char*                   signal_units;
}
#endif /* 0 */

// ======================================================================
//   Collection of Channel Objects
// ======================================================================

%define %nds_channels_type( )
%nds_vector_to_array(channels_type,channel);
%enddef /* %nds_channels_type( ) */

// ======================================================================
//   Channel Names
// ======================================================================

%typemap( in, noblock=1 ) connection::channel_names_type ( Array<std::string> cellstr ) {

  if ( ! $input.is_cellstr( ) )
  {
    SWIG_exception(SWIG_TypeError, "channel_names must be a cell array of strings");
  }

  cellstr = $input.cellstr_value( );

  for ( octave_idx_type i = 0;
        i < cellstr.nelem();
        ++i )
  {
    $1.push_back( cellstr( i ) );
  }
}

%typemap( in, noblock=1 ) const connection::channel_names_type& ( Array<std::string> cellstr, $1_basetype names ) {

  if ( ! $input.is_cellstr( ) )
  {
    SWIG_exception(SWIG_TypeError, "channel_names must be a cell array of strings");
  }

  cellstr = $input.cellstr_value( );

  $1 = &names;
  for ( octave_idx_type i = 0;
        i < cellstr.nelem();
        ++i )
  {
    $1->push_back( cellstr( i ) );
  }
}

%typemap( freearg ) const connection::channel_names_type& {
  //---------------------------------------------------------------------
  // This is a no-op as the argument is assigned to the address
  //   of a stack allocated object.
  //---------------------------------------------------------------------
}

#endif /* NDS_OCTAVE_CHANNEL_I */
