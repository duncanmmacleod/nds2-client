#ifndef NDS_OCTAVE_CONNECTION_I
#define NDS_OCTAVE_CONNECTION_I

%import "nds_octave_buffer.i"
%import "nds_octave_channel.i"

//-----------------------------------------------------------------------
// Connections
//-----------------------------------------------------------------------

%define CONNECTION_PROTOCOL( )
  %typedef int protocol_type;
  %immutable;
  static int PROTOCOL_INVALID = -1;
  static int PROTOCOL_ONE = 1;
  static int PROTOCOL_TWO = 2;
  static int PROTOCOL_TRY = 3;
  %mutable;
%enddef

%extend connection {

  std::string __str__( ) {
    std::ostringstream  name;
    name << "<"
         << $self->get_host( )
         << ":"
         << $self->get_port( )
         << " "
         << "(protocol version " << $self->get_protocol( ) << ")"
         << ">"
      ;
    return name.str( );
  }
}

#endif /* NDS_OCTAVE_CONNECTION_I */
