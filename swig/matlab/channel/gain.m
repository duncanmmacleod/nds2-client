function value = gain( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the gain of the channel
%
%   Retrieves the gain of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.gain( )
end
