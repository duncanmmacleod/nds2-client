function chan = channel( base ) % -*- mode: octave; -*-
% CHANNEL constructor for the ndsm.channel class
%
%   CHAN = channel( BASE )
%     Creates a new instance
%
%   INPUTS:
%     BASE           : 
%
%   OUTPUTS:
%     CHAN           : ndsm.channel instance
  switch( nargin )
    case 0
      chan = ndsm.channel( );
    case 1
      switch( class( base ) )
    	case 'nds.channel'
    	  chan = ndsm.channel( base );
       end
    otherwise
      chan = ndsm.channel( );
  end
end
