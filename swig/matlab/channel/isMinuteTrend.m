function value = isMinuteTrend( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the isMinuteTrend of the channel
%
%   Retrieves the isMinuteTrend of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.isMinuteTrend( )
end
