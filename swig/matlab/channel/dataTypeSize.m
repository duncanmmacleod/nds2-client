function value = dataTypeSize( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the dataTypeSize of the channel
%
%   Retrieves the dataTypeSize of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.dataTypeSize( )
end
