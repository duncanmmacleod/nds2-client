% -*- mode: octave -*-
%------------------------------------------------------------------------
global unit_test


unit_test = UnitTest(  );
hostname = unit_test.hostname( );
port = unit_test.port( );

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = ndsm.Connection(hostname, port );
conn.setparameter('ALLOW_DATA_ON_TAPE', 'true');
conn.setparameter('ITERATE_USE_GAP_HANDLERS', 'false');

%------------------------------------------------------------------------
% Ticket 289
%------------------------------------------------------------------------

chans = conn.findchannels( '*', ndsm.Channel.CHANNEL_TYPE_RDS );

unit_test.check( length( chans ), 3, ...
		 'Correct number of channels' );
unit_test.check( chans( 1 ).name( ), 'X1:PEM-1', ...
		 'Validate channel zero name' );
unit_test.check( chans( 1 ).datatype( ), ndsm.Channel.DATA_TYPE_UNKNOWN, ...
		 'Validate channel zero data type' );
unit_test.check( chans( 3 ).name( ), 'X1:PEM-3', ...
		 'Validate channel two name' );
unit_test.check( chans( 3 ).datatype( ), ndsm.Channel.DATA_TYPE_FLOAT32, ...
		 'Validate channel two data type' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

exit( unit_test.exitcode( ) );
