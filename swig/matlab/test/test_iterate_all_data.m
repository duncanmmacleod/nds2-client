% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = UnitTest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasoption( '-proto-1' ) )
  protocol = ndsm.Connection.PROTOCOL_ONE;
  display( 'Doing proto one' );
end
if ( unit_test.hasoption( '-proto-2' ) )
  protocol = ndsm.Connection.PROTOCOL_TWO;
  display( 'Doing proto two' );
end
if ( unit_test.hasoption( '-no-gap' ) )
  use_gap_handler = 'false';
  display( 'Doing no gaps' );
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
display( [ sprintf( 'About to connection: %s - %d - %d', ...
		   hostname, port, protocol ) ] );
conn = ndsm.Connection(hostname, port, protocol );
conn.setparameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
START = 1770000000;
STOP = START + 20;
STRIDE = 10;
CHANNELS = { 'X1:PEM-1',
	     'X1:PEM-2'
	    };

last_start = 0;
cur_gps = START;
expected = { 1.5, 2.75 };

iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasnext( ) )
  bufs = iter.next( );
  %----------------------------------------------------------------------
  % MATLAB indexing starts at 1
  %----------------------------------------------------------------------
  unit_test.check( bufs( 1 ).start( ), ...
		  cur_gps, ...
		  'Verifying GPS second of buffer 1' );
  unit_test.check( bufs( 1 ).samples( ), ...
		  256 * STRIDE, ...
		  'Verifying samples of buffer 1' );
  unit_test.check( true, ...
		   [ checkdata_iad( bufs( 1 ), expected{1} ) ], ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).start( ), ...
		   cur_gps, ...
		   'Verifying GPS second of buffer 2' );
  unit_test.check( bufs(2).samples( ), ...
		   512 * STRIDE, ...
		   'Verifying samples of buffer 2' );
  unit_test.check( true, ...
		   [ checkdata_iad( bufs(2), expected{2} ) ], ...
		   'Verifying data of buffer 2' );
  %----------------------------------------------------------------------
  cur_gps = bufs(1).stop( );
  expected = { 3.5, 4.75 };
end

%------------------------------------------------------------------------
% Make sure we can do other operations
%------------------------------------------------------------------------

conn.setepoch( START, STOP );
conn.findchannels( 'X1:PEM-1' );
conn.findchannels( 'X1:PEM-2' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
unit_test.exit( );
