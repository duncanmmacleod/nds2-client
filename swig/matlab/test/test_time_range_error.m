% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
global unit_test;

unit_test = UnitTest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';

%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

try
  %----------------------------------------------------------------------
  % Need to adjust according to parameter
  %------------------------------------------------------------------------
  if ( unit_test.hasoption( '-proto-1' ) )
    protocol = ndsm.Connection.PROTOCOL_ONE;
    unit_test.msgdebug( 10, 'Doing proto one' );
  end
  if ( unit_test.hasoption( '-proto-2' ) )
    protocol = ndsm.Connection.PROTOCOL_TWO;
    unit_test.msgdebug( 10, 'Doing proto two' );
  end
  if ( unit_test.hasoption( '-no-gap' ) )
    use_gap_handler = 'false';
    unit_test.msgdebug( 10, 'Doing no gaps' );
  end

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------

  unit_test.msgdebug( 10, ...
		      'About to connection: %s - %d - %d', ...
		      hostname, port, protocol );
  conn = ndsm.Connection(hostname, port, protocol );
  unit_test.msgdebug( 10, ...
		      'Setting parameter: %s - %s', ...
		      'ITERATE_USE_GAP_HANDLERS', ...
		      use_gap_handler );
  conn.setparameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
  unit_test.msgdebug( 10, 'Set parameters' );

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------
  START = 1770000000;
  STOP = START + 20;
  STRIDE = 10;
  CHANNELS = { 'X1:PEM-1',
	       'X1:PEM-2'
	      };

  last_start = 0;

  iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
  unit_test.msgdebug( 10, 'Allocated iterator' );
  while( iter.hasnext( ) )
    bufs = iter.next( );
  end

  %----------------------------------------------------------------------
  % Make sure we can do other operations
  %----------------------------------------------------------------------

  state = false;
  try
    conn.setepoch( STOP, START );
    conn.findchannels( 'X1:PEM-1' );
    conn.findchannels( 'X1:PEM-2' );
  catch err
    if ( isa( err, 'matlab.exception.JavaException' ) )
      jerr = err.ExceptionObject;
      fprintf( 'Debug: java exception class: %s\n', class( jerr ) )
      assert(isjava( jerr ) )
      if ( isa( jerr, 'java.lang.IllegalArgumentException' ) )
          state = true
      end
    end
  end
  unit_test.check( true, state, 'Catch IllegalArgumentExecption in iterator' );

  conn.setepoch( START, STOP );
  conn.findchannels( 'X1:PEM-1' );
  conn.findchannels( 'X1:PEM-2' );

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------
  conn.close( );
catch err
  s = '<need to dump stack trace>';
  % s = unit_test.dump_stack( err.stack );
  % unit_test.msginfo( s );
  unit_test.check( false, ...
		   true, ...
		   sprintf( 'Caught unexpected excption: %s\n%s', ...
			   err.message, ...
			   s ) );
end
unit_test.exit( );

