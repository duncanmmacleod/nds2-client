% -*- mode: octave -*-
% -------------------------------------------------------------------
%   Simple test to verify availability function
% -------------------------------------------------------------------

unit_test = UnitTest;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_TWO;

conn = ndsm.Connection(hostname, port, protocol );

gps_start = 1116733655;
gps_stop = 1116733675;

cn = { 'H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ',
       'H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ',
       'H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ',
       'H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ',
       'H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ',
       'H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ'
      };

conn.setepoch(gps_start, gps_stop);

avail = conn.getavailability( cn );

%unit_test.msgInfo( avail.toString( ) );
unit_test.check( length( avail ), length(cn), 'number of epochs: ' );
	    
for i = 1:length( avail )
  entry = avail(i);
  lead = sprintf( 'offset: %d ', ...
		  i );

  data = entry.getdata( );

  %unit_test.check( entry.getname( ), ...
  %		   cn{ i }, ...
  %		   [ lead  'entry.getname(): ' ] );
  unit_test.check( length( data ), ...
		   2, ...
		   [ lead 'length( entry.getdata( ) ): ' ] );

  if ( length( data ) == 2 )
    unit_test.check( data( 1 ).getframetype( ), ...
		     'H-H1_C', ...
		     [ lead 'frameType(1): ' ] );
    unit_test.check( data( 2 ).getframetype( ), ...
		     'H-H1_R', ...
		     [ lead 'frameType(2): ' ] );
    %--------------------------------------------------------------------
    unit_test.check( data( 1 ).getgpsstart( ), ...
		     data( 2 ).getgpsstart( ), ...
		     [ lead  'start time equal: offset-0: ' ] );
    unit_test.check( data( 1 ).getgpsstart( ), ...
		     gps_start, ...
		     [ lead 'start time correct: ' ] );
    %--------------------------------------------------------------------
    unit_test.check( data( 1 ).getgpsstop( ), ...
		     data( 2 ).getgpsstop( ), ...
		     [ lead 'stop time equal: offset-0: ' ] );
    unit_test.check( data( 1 ).getgpsstop( ), ...
		     gps_stop, ...
		     [ lead 'stop time correct: ' ] );
  end % if ( length( data ) == 2 )
end

if ( false )
  expected = '( <H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';
  unit_test.check( avail.tostring.substring(0,length(expected)), ...
		   expected, ...
		   'Verify availability list name is correct: ' );
end

if ( false ) %
  expected = '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';

  actual = avail( 1 ).tostring( )
  unit_test.check( actual( 1:length(expected) ), ...
		   expected, ...
		   'Verify availability list name is correct: ' );
end

if ( true )
  entry = ndsm.Availability( );

  data( 1 ) = ndsm.Segment( 'X-X1_C',  gps_start, gps_start + 1024 );
  data( 2 ) = ndsm.Segment( 'X-X1_R',  gps_start, gps_start + 100 );
  data( 3 ) = ndsm.Segment( 'X-X1_R',  gps_start + 1024, gps_start + 1024+100 );
  entry.setname( 'X1:TEST_1' );
  entry.setdata( data );
  test_avail( 1 ) = entry;
	    
  simple_test = test_avail.simplelist( );
    
  %unit_test.check( simple_test.toString( ), ...
  %		   '( ( 1116733655-1116734779 ) )', ...
  %		   'simple_list range: ' );

  unit_test.check( length( simple_test ),  1, 'simple_list size: ' );
  % unit_test.check( simple_test( 1 ).size( ), 1, 'simple_list[ 0 ] size: ' );
  unit_test.check( simple_test( 1 ).getgpsstart( ), ...
		   gps_start, ...
		   'simple_list[ 0 ]  GPS Start: ' );
  unit_test.check( simple_test( 1 ).getgpsstop( ), ...
		   gps_start +1024+100, ...
		   'simple_list[ 0 ]  GPS Stop: ' );
end

%------------------------------------------------------------------------
% test the implicit availability call in fetch
%------------------------------------------------------------------------
bufs = conn.fetch( gps_start, gps_stop, cn );

%if ( false )
%  expected = '(\'<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (GPS time 1116733655, 10240 samples)>\',\'<H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ (GPS time 1116733655, 10240 samples)>\',\'';
%  actual = bufs.tostring( ).substring(0, expected.length( ) );
%
%  unit_test.check( actual( 1:length( expected ) ), ...
%		    expected, ...
%		    'Check name of buffer container: ' );
%end % if

unit_test.check( bufs(1).tostring( ), ...
		 '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (512Hz, RAW, FLOAT32)>', ...
		 'Check name of first data buffer: ' );

%------------------------------------------------------------------------
% Test a larger availability.  This particular one will be > 128k
%------------------------------------------------------------------------
conn.setepoch(0, 1999999999);
cn = { 'H1:GDS-CALIB_STRAIN,reduced'};
avail = conn.getavailability( cn );

%------------------------------------------------------------------------
% Close the connection
%------------------------------------------------------------------------
conn.close( );
%------------------------------------------------------------------------
% Report on testing status
%------------------------------------------------------------------------
unit_test.exit( )
