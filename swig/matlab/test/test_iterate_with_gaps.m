% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

unit_test = UnitTest( argv );

hostname = unit_test.hostname( );
port = unit_test.port( );
if ( unit_test.hasoption( '-proto-1' ) )
    protocol = ndsm.Connection.PROTOCOL_ONE;
elseif ( unit_test.hasoption( '-proto-2' ) )
  protocol = ndsm.Connection.PROTOCOL_TWO;
else
  protocol = ndsm.Connection.PROTOCOL_TWO;
end


%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------

last_start = 0;

if( protocol == ndsm.Connection.PROTOCOL_ONE )
  %----------------------------------------------------------------------
  % connection.PROTOCOL_ONE
  %----------------------------------------------------------------------
  start = 1130797740;
  stride = 3000;
  finish = start + stride * 3;
  samples_per_segment = stride / 60;
  channels = { 'X1:CDS-DACTEST_COSINE_OUT_DQ.n,m-trend'
	      };
else
  %----------------------------------------------------------------------
  % Default is to use connection.PROTOCOL_TWO
  %----------------------------------------------------------------------
  start = 1116286100;
  finish = start + 200;
  stride = 50;
  data_start = 1116286200;
  samples_per_segment = stride;
  channels = { 'H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend'
	      };
end

conn.setparameter('ITERATE_USE_GAP_HANDLERS', 'false');

iter = conn.iterate(start, finish, channels)
while( iter.hasnext( ) )
  % Read off buffers
  bufs = iter.next( );
end

count = 0;
iter = conn.iterate(start, finish, channels)
while( iter.hasnext( ) )
  % Read off buffers
  bufs = iter.next( );
  count = count + 1;
end

conn.setparameter('ITERATE_USE_GAP_HANDLERS', 'true');
conn.setparameter('GAP_HANDLER', 'STATIC_HANDLER_ZERO');

iter = conn.iterate(start, finish, stride, channels);
while( iter.hasnext( ) )
  bufs = iter.next( );
  unit_test.check( length( bufs ), ...
		   1, ... 
		  'Stride returns 1 buffer' );
  if( last_start == 0 )
    unit_test.check( bufs(1).start( ), ...
		     start, ...
		    'Validate initial GPS start' );
  else
    unit_test.check( bufs(1).start( ), ...
		     last_start + stride, ...
		    'Validate offset GPS start' );
  end
  unit_test.check( bufs( 1 ).stop( ) == finish || ...
		   bufs( 1 ).samples( ) == samples_per_segment, ...
		   true, ...
		  'Verify lenth of buffer' );
  last_start = bufs(1).start( );
end

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

unit_test.exit( );
