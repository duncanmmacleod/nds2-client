%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = isgapsecond_iwsg( GPS )
global START;

gps = GPS - START;
if ( (gps < 1) ...
    || (gps == 9 ) ...
    || (gps == 18 ) ...
    || (gps >= 32 && gps < 40) ...
    || (gps >= 50 && gps < 61) ...
    || (gps > 67) )
  RETVAL = true;
else
  RETVAL = false;
end
end