classdef SegmentListType < handle % -*- mode: octave; -*-
    % List of Segment instances
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
    end
    
    methods (Hidden, Access = public)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = SegmentListType( varargin )
         % OBJ constructor for the ndsm.AvailabilityListType class
         %
         %   OUTPUTS:
         %     OBJ : ndsm.AvailabilityListType instance
	 switch( nargin )
	   case 0
	     h = nds2.segment_list_type( );
	   case 1
	     h = varargin{1};
	 end
	 OBJ.handle = h;
       end

       function RETVAL = size( self )
	 % Retrieve the number of Segments in the list
	 %
	 % s = self.size( )
	 %
	 %   INPUTS:
	 %     self   : ndsm.SegmentListType instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The number of Segments in the list
	 RETVAL = self.gethandle( ).size( );
       end

       function RETVAL = get( self, offset )
	 % Retrieve the segment at an offset.
	 %
	 % seg = self.get( off )
	 %
	 %   INPUTS:
	 %     self   : ndsm.SegmentListType instance
	 %     offset : offset postion of element starting at 0
	 %   OUTPUTS:
	 %     RETVAL : The segment at the specified offset
	 RETVAL = self.gethandle( ).get( offset );
       end

       function pushback( self, segment )
	 % Append a segment to the list
	 %
	 % self.pushback( segment )
	 %
	 %   INPUTS:
	 %     self    : ndsm.SegmentListType instance
	 %     segment : Either an nds2.segment instance or an ndsm.Segment instance
	 if ( isa( segment, 'nds2.segment' ) )
	   self.gethandle( ).pushBack( segment );
	 elseif ( isa( segment, 'ndsm.Segment' ) )
	   self.gethandle( ).pushBack( segment.gethandle( ) );
	 end
       end
    end
end
