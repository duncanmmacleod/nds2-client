classdef ConnectionIterator < handle % -*- mode: octave; -*-
    % Iterator class for a Connection
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
    end
    
    methods (Hidden, Access = private)
        function HANDLE = gethandle(self)
            if isa(self.handle,'numeric')
                throw(MException( 'ndsm.ConnectionIterator:invalid', ...
				  'No valid connection iterator handle for this operation'));
            end
            HANDLE = self.handle;
        end
    end
    
    methods (Access = public)
       function OBJ = ConnectionIterator( varargin )
	 % Create an instance of a ndsm.ConnectionIterator
	 %
	 % OBJ = ndsm.ConnectionIterator( )
	 %
	 % OBJ = ndsm.ConnectionIterator( MatCI)
	 %
	 % OBJ = ndsm.ConnectionIterator( JavaCI )
	 %
	 %   INPUTS:
	 %     MatCI  : ndsm.ConnectionIterator instance
	 %     JavaCI : nds2.connection_iterator
	 %
	 %   OUTPUTS:
	 %     OBJ    : ndsm.ConnectionIterator instance
	 switch( nargin )
	   case 0
	     h = nds2.connection_iterator( );
	   case 1
	     class( varargin{1} )
	     if ( isa( varargin{1}, 'nds2.connection_iterator' ) )
	       h = varargin{1};
	     else % if
	       h = nds2.connection_iterator( varargin{1} )
	     end % if ()
	 end % switch
	 OBJ.handle = h;
       end % function - Constructor
       
       function BOOL = hasnext(this)
         % Check for more data in the current iterate request
         % Returns true if the current iterate request has more data to be read.  Data should be read using the #next() method.
         %
         % OUTPUTS
         %     BOOL : True if there is more data, else false
         %
	 BOOL = this.gethandle( ).hasNext( );
       end

       function BUFFERS = next(this)
         % Retrieve the next data block from an iterate request
         % 
         % OUTPUTS:
         %   BUFFERS  : array of buffers
	 BUFFERS = ndsm.Buffer( this.gethandle( ).next( ) );
       end

    end
end

