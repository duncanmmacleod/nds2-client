classdef Segment < handle % -*- mode: octave; -*-
    % A Segment contain information about a channel.
    % This information includes the frame type and
    % the time range.
    % The time range is in GPS seconds and includes the start
    % and excludes the stop (ex: [start, stop) )
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
	array_handle = 0;
    end
    
    methods (Hidden, Access = public)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = Segment( varargin )
         % OBJ constructor for the ndsm.Availability class
         %
	 % OBJ = Segment( )
	 %   Create an object with default values
	 %
	 % OBJ = Segment( GPS_START, GPS_STOP )
	 %   Create a segment starting at GPS_START inclusive and
	 %   ending at GPS_STOP exclusive
	 %
	 % OBJ = Segment( FRAME_TYPE, GPS_START, GPS_STOP )
	 %   Create a segment for FRAME_TYPE starting at GPS_START
	 %   inclusive and ending at GPS_STOP exclusive
	 %
         %   OUTPUTS:
         %     OBJ : ndsm.Segment instance
	 switch( nargin )
	   case 0
	     h = nds2.segment( );
	   case 1
	     if ( isa( varargin{1}, 'nds2.segment' ) )
	       disp( sprintf( 'Inside nds2.segment handler: %s', ...
			     class( varargin{1} ) ) );
	       h = varargin{1};
	     elseif ( isa( varargin{1}, 'nds2.segment_list_type' ) )
	       disp( sprintf( 'Inside list type handler: %s - size: %d', ...
			      class( varargin{1} ), size( varargin{1} ) ) );
               s = size( varargin{1} );
	       if ( s > 0 )
	         for x = 1:s
		   h = varargin{1}.get(x-1);
	           disp( sprintf( 'Generating new element for offset: %d - %s', ...
				  x, class( h ) ) );
		   OBJ(x) = ndsm.Segment( h, varargin{1} );
		 end
               else % s > 0
	         OBJ = ndsm.Segment.empty( 0 );
               end % if s > 0
	       return;
	     else % if
	       disp( sprintf( 'Inside default handler: %s', ...
			      class( varargin{1} ) ) );
	       h = nds2.segment( varargin{1} )
	     end
	   case 2
	     if ( isa( varargin{1}, 'nds2.segment' ) && ...
		  isa( varargin{2}, 'nds2.segment_list_type' ) )
	       h = varargin{1};
	       OBJ.array_handle = varargin{2};
	     end
	   case 3
	     h = nds2.segment( varargin{1}, varargin{2}, varargin{3} );
	 end
	 OBJ.handle = h;
       end

       function RETVAL = getframetype( self )
	 % Retrieve the frame type
	 %
	 %   INPUTS:
	 %     self   : ndsm.Segment instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The frame type
	 RETVAL = char( self.gethandle( ).getFrameType( ) );
       end % getframetype

       function RETVAL = getgpsstart( self )
	 % Retrieve the GPS start second of the segment
	 %
	 %   INPUTS:
	 %     self   : ndsm.Segment instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The GPS start second of the segment
	 RETVAL = self.gethandle( ).getGpsStart( );
       end % getgpsstart

       function RETVAL = getgpsstop( self )
	 % Retrieve the GPS stop second of the segment
	 %
	 %   INPUTS:
	 %     self   : ndsm.Segment instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The GPS stop second of the segment
	 RETVAL = self.gethandle( ).getGpsStop( );
       end % getgpsstop

    end
end
