classdef Availability < handle % -*- mode: octave; -*-
    % For a channel, contains the channel name and a list of segments for which the data is available.

    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
	array_handle = 0;
    end
    
    methods (Hidden, Access = public)
        function HANDLE = gethandle(this)
	    class( this.handle )
            if ( isnumeric(this.handle) )
                throw(MException('ndsm.Availability:invalid','No valid handle for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = Availability( varargin )
         % OBJ constructor for the ndsm.Availability class
	 %
	 % Contains a channel name and a list of segments in
	 %   which the data is available.
	 %
	 % OBJ = Availability( )
	 %   Create an object with default values
         %
         %   OUTPUTS:
         %     OBJ : ndsm.Availability instance
	 switch( nargin )
	   case 0
	     h = nds2.availability( );
	   case 1
	     if ( isa( varargin{1}, 'nds2.availability' ) )
	       h = varargin{1};
	     elseif ( isa( varargin{1}, 'nds2.availability_list_type' ) )
	       for x = 1:size( varargin{1} )
		 OBJ(x) = ndsm.Availability( varargin{1}.get(x-1) );
	       end
	       return;
	     else % if
	       disp( sprintf( 'Inside default handler' ) );
	       h = nds2.availability( varargin{1} );
	     end % if ()
	   case 2
	     if ( isa( varargin{1}, 'nds2.availability' ) && ...
		  isa( varargin{2}, 'nds2.availability_list_type' ) )
	       h = varargin{1};
	       OBJ.array_handle = varargin{2};
	     end
	 end % switch
	 OBJ.handle = h;
       end

       function NAME = getname( self )
	 % NAME = self.getname( )
	 %
	 %   OUTPUTS:
	 NAME = char( self.gethandle( ).getName( ) );
       end

       function DATA = getdata( self )
	 % Retrieve start stop pairs associated with availability set.
	 %
	 %   INPUTS:
	 %     self  : ndsm.Availability instance
	 %
	 %   OUTPUTS:
	 %     DATA  : start stop pairs associated with availability set.
	 disp( sprintf( 'Doing getdata on %s', ...
		        self.getname( ) ) );
	 DATA = ndsm.Segment( self.gethandle( ).getData( ) );
       end

       function setdata( self, DATA )
	 % Add data to the availability set
	 %
	 %   INPUTS:
	 %     self  : ndsm.Availability instance
	 %     DATA  : data to be associated with the availability set.
	 for x = 1:length( DATA )
	   self.gethandle( ).getData( ).pushBack( DATA( x ).gethandle( ) );
	 end
       end

       function setname( self, name )
	 % Establish name of the availability set
	 %
	 %   INPUTS:
	 %     self  : ndsm.Availability instance
	 %     name  : Name to be associated with the availability set
	 self.gethandle( ).setName( name );
       end

       function RETVAL = simplelist( self )
	 % Retrieve a simple list of availability
	 %
	 %   INPUTS:
	 %     self  : ndsm.Availability instance
	 %
	 %   OUTPUTS:
	 %     RETVAL   :  Simple list of availabilities.
	 RETVAL = ndsm.SimpleSegment( self.gethandle( ).simpleList( ) );
       end % function - simplelist

       function RETVAL = tostring( self )
	 % Retrieve string representation of the availability information
	 %
	 %   INPUTS:
	 %     self  : ndsm.Availability instance

	 %   OUTPUTS:
	 %     RETVAL  : string representation
	 RETVAL = char( self.gethandle( ).toString( ) );
       end
    end
end
