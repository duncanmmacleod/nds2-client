function VER = version( ) % -*- mode: octave -*-
  % Retrieve the version of the ndsm package
  %
  % VER = ndsm.version( )
  %
  %   OUTPUTS:
  %     VER : The version of the ndsm package
  VER = char(nds2.nds2.version( ));
end
