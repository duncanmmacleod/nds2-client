classdef Buffer < ndsm.Channel % -*- mode: octave; -*-
    % Channel information along with frame data
    properties (Constant, GetAccess = public)
       GPS_INF = nds2.buffer.GPS_INF;
    end

    methods (Access = public)
        
       function BUFFER = Buffer( varargin )
         % BUFFER constructor for the ndsm.buffer class
         %
         %   OUTPUTS:
         %     BUFFER : ndsm.buffer instance
	 switch( nargin )
	   case 0
	     superclass_args = nds2.buffer( );
	   case 1
	     if ( isa( varargin{1}, 'nds2.buffer' ) )
	       superclass_args = { varargin{1} };
	     elseif ( isa( varargin{1}, 'nds2.buffer[]' ) )
	       superclass_args = { varargin{1} };
	     else % if
	       superclass_args = { varargin{1} };
	     end % if ()
	   case 2
	     if ( isa( varargin{1}, 'nds2.buffer' ) && ...
		  isa( varargin{2}, 'nds2.buffer[]' ) )
	       superclass_args = { varargin{1}, varargin{2} };
	     end
	 end % switch
	 BUFFER = BUFFER@ndsm.Channel( superclass_args{:} );
       end

       function CHANNEL = getchannel( self )
	 % CHANNEL retrieve buffer as channel base type
	 %
	 % OUTPUTS:
	 %    CHANNEL : The buffer downgraded to a channel
	 CHANNEL = self;
       end

       function VALUE = getdata( self )
	 % Retrieve frame data associate with channel
	 %
	 %   INPUTS:
	 %     self  : ndsm.Buffer instance
	 %
	 %   OUTPUTS:
	 %     VALUE : Frame data associated with channel
	 VALUE = self.gethandle( ).getData( );
       end

       function VALUE = samples( self )
	 % Retrieve the number of samples being stored
	 %
	 % OUTPUTS:
	 %     VALUE : The number of samples being stored
	 VALUE = self.gethandle( ).samples( );
       end

       function VALUE = start( self )
	 % Retrieve GPS start second of the data
	 %
	 %   INPUTS:
	 %     self  : ndsm.Buffer instance
	 %
	 %   OUTPUTS:
	 %     VALUE : The GPS start second of the data
	 VALUE = self.gethandle( ).start( );
       end

       function VALUE = startnano( self )
	 % Retrieve GPS nano second offset of the data
	 %
	 % The nano second value is to be used in conjunction
	 % with the GPS start second to get the actual
	 % GPS start time of the data.
	 %
	 %   INPUTS:
	 %     self  : ndsm.Buffer instance
	 %
	 %   OUTPUTS:
	 %     VALUE : The GPS nano second offset of the data
	 VALUE = self.gethandle( ).startNano;
       end

       function VALUE = stop( self )
	 % Retrieve GPS stop second of the data
	 %
	 %   INPUTS:
	 %     self  : ndsm.Buffer instance
	 %
	 %   OUTPUTS:
	 %     VALUE : The GPS stop second of the data
	 VALUE = self.gethandle( ).stop( );
       end

    end
end
