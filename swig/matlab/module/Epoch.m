classdef Epoch < handle % -*- mode: octave; -*-
    %Connection Primary NDS class for communicating with a server
    %   See the detailed docs on the web, I'm sure it's there somewhere
    
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
    end
    
    methods (Hidden, Access = private)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.epoch:invalid','No valid epoch handle for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = Epoch( varargin )
         % OBJ constructor for the ndsm.Epoch class
         %
	 %   OBJ = Epoch( )
	 %     Create a new instance sutable to store epoch information
	 %
	 %   OBJ = Epoch( NAME )
	 %     Create a new instance sutable to store epoch information
	 %
	 %   OBJ = Epoch( NAME, GPS_START, GPS_END )
	 %     Create a new instance sutable to store epoch information
	 %
	 % INPUTS:
	 %   NAME      : string
	 %   GPS_START : int
	 %   GPS_END   : int
	 %
         % OUTPUTS:
         %   OBJ : ndsm.Epoch instance
	 switch( nargin )
	   case 0
	     h = nds2.epoch( );
	   case 1
	     if ( isa( varargin{1}, 'nds2.epoch' ) )
	       h = varargin{1};
	     elseif ( isa( varargin{1}, 'nds2.epochs_type' ) )
	       for x = 1:size( varargin{1} )
		 OBJ(x) = ndsm.Epoch( varargin{1}.get(x-1) );
	       end
	       return;
	     else % if
	       h = nds2.epoch( varargin{1} )
	     end % if ()
	   case 3
	     h = nds2.epoch( varargin{1}, varargin{2}, varargin{3} );
	 end % switch
	 OBJ.handle = h;
       end % function - Constructor

       function NAME = getname( this )
         % NAME of epoch
         %
	 %   NAME = getname( )
	 %     Retrieve the name associated with the epoch
	 %
	 % OUTPUTS:
	 %   NAME      : string
	 NAME = char(this.gethandle( ).getName( ));
       end % function - getName

       function GPS_START = getgpsstart( this )
         % GPS_START of epoch
         %
	 %   GPS_START = getgpsstart( )
	 %     Retrieve the GPS start second associated with the epoch
	 %
	 % OUTPUTS:
	 %   GPS_START : int
	 GPS_START = this.gethandle( ).getGpsStart( );
       end % function - getGpsStart

       function GPS_STOP = getgpsstop( this )
         % GPS_STOP of epoch
         %
	 %   GPS_STOP = getgpsstop( )
	 %     Retrieve the GPS stop second associated with the epoch
	 %
	 % OUTPUTS:
	 %   GPS_STOP : int
	 GPS_STOP = this.gethandle( ).getGpsStop( );
       end % function - getGpsStop

    end % methods
end

