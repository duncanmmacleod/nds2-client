#========================================================================
# Conditionaly set the CMake policy
#
# This routine tests if the policy is available and set it to STATE
#   if present.
#
# POLICY_ID = The CMake policy identifier
# STATE     = NEW or OLD
#
#------------------------------------------------------------------------
function(cond_set_policy POLICY_ID STATE)
  if( POLICY ${POLICY_ID} )
    cmake_policy( SET ${POLICY_ID} ${STATE})
  endif( POLICY ${POLICY_ID} )
endfunction(cond_set_policy)
#========================================================================
# Retrieve the swig property
#
# var      = Variable to set with the swig property value
# property = Property to be queried
# target   = SWIG target to query
#
# NOTE:
#   MODULE_OUTPUT_NAME is introduced for NDS use.
#
#------------------------------------------------------------------------
function(get_swig_property var property target)
  get_target_property(
    ${var}
    ${SWIG_MODULE_${target}_REAL_NAME}
    ${property}
  )
  if ( "${${var}}" STREQUAL "${var}-NOTFOUND" )
    if ( ${property} STREQUAL "PREFIX" )
      set( ${var} ${CMAKE_SHARED_MODULE_PREFIX} )
    elseif ( ${property} STREQUAL "SUFFIX" )
      set( ${var} ${CMAKE_SHARED_MODULE_SUFFIX} )
    elseif ( ${property} STREQUAL "OUTPUT_NAME" )
      set( ${var} ${SWIG_MODULE_${target}_REAL_NAME} )
    elseif ( ${property} STREQUAL "MODULE_OUTPUT_NAME" )
      get_swig_property( prefix PREFIX ${target} )
      get_swig_property( suffix SUFFIX ${target} )
      get_swig_property( base OUTPUT_NAME ${target} )
      set( ${var} "${prefix}${base}${suffix}" )
    endif ( ${property} STREQUAL "PREFIX" )
  endif ( "${${var}}" STREQUAL "${var}-NOTFOUND" )
  #----------------------------------------------------------------------
  # Elevate the variable
  #----------------------------------------------------------------------
  message( STATUS "${var} set to ${${var}}" )
  set( ${var} ${${var}} PARENT_SCOPE )
endfunction(get_swig_property )
#========================================================================
# CALCULATE_LIBTOOL_VERSION
#
# library_name = Variable to set with the swig property value
# current      = Property to be queried
# revision     = SWIG target to query
# age          =
#------------------------------------------------------------------------
function( NDS_LIBTOOL_VERSION_CALCULATION library_name current revision age )
  math( EXPR ${library_name}_VERSION_MAJOR "${current} - ${age}" )
  set( ${library_name}_VERSION_MAJOR ${${library_name}_VERSION_MAJOR}
    PARENT_SCOPE )
  set( ${library_name}_VERSION_MINOR ${age} PARENT_SCOPE )
  set( ${library_name}_VERSION_MICRO ${revision} PARENT_SCOPE )
  set( ${library_name}_VERSION
    ${${library_name}_VERSION_MAJOR}.${age}.${revision}
    PARENT_SCOPE )
  set( ${library_name}_SOVERSION
    ${${library_name}_VERSION_MAJOR}
    PARENT_SCOPE )
endfunction( NDS_LIBTOOL_VERSION_CALCULATION )
#========================================================================
# NDS_POST_INSTALL
#------------------------------------------------------------------------
macro( NDS_POST_INSTALL )
  if ( POST_INSTALL )
    install( CODE "${POST_INSTALL}" )
  endif ( POST_INSTALL )
endmacro( NDS_POST_INSTALL )

#========================================================================
# NDS_SO_SYMLINK
#
# library_name = Variable to set with the swig property value
#------------------------------------------------------------------------
function( NDS_SO_SYMLINK library_name var target )
  if ( APPLE )
    set( ${var}_NAME ${CMAKE_SHARED_LIBRARY_PREFIX}${library_name} )
    set( ${var}_LINK ${${var}_NAME}.${${var}_VERSION_MAJOR}${CMAKE_SHARED_LIBRARY_SUFFIX} )
    set( ${var}_TARGET ${${var}_NAME}.${${var}_SOVERSION}${CMAKE_SHARED_LIBRARY_SUFFIX} )
  else ( APPLE )
    set( ${var}_NAME ${CMAKE_SHARED_LIBRARY_PREFIX}${library_name}${CMAKE_SHARED_LIBRARY_SUFFIX} )
    set( ${var}_LINK ${${var}_NAME}.${${var}_VERSION_MAJOR} )
    set( ${var}_TARGET ${${var}_NAME}.${${var}_SOVERSION} )
  endif ( APPLE )
  set( ${var}_NAME ${${var}_NAME} PARENT_SCOPE )
  set( ${var}_LINK ${${var}_LINK} PARENT_SCOPE )
  set( ${var}_TARGET ${${var}_TARGET} PARENT_SCOPE )
  if ( IS_ABSOLUTE ${LIBRARY_DESTINATION} )
    set( _destination ${LIBRARY_DESTINATION} )
  else ( IS_ABSOLUTE ${LIBRARY_DESTINATION} )
    set( _destination ${CMAKE_INSTALL_PREFIX}/${LIBRARY_DESTINATION} )
  endif ( IS_ABSOLUTE ${LIBRARY_DESTINATION} )
  add_custom_command( TARGET ${target} 
    POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${${var}_TARGET} ${${var}_LINK}
    COMMENT "-- Creating: symlink: ${${var}_LINK} -> ${${var}_TARGET} (cwd: ${CMAKE_CURRENT_BINARY_DIR})"
  )
  #set( POST_INSTALL "${POST_INSTALL}
  #  message(\"-- Installing: symlink: ${${var}_LINK} -> ${${var}_TARGET} (cwd: \${DESTDIR}${_destination})\")
  #  execute_process(
  #    COMMAND ln -sf ${${var}_TARGET} ${${var}_LINK}
  #    WORKING_DIRECTORY \${DESTDIR}${_destination}
  #  )"
  #  PARENT_SCOPE
  #)
endfunction( NDS_SO_SYMLINK )
