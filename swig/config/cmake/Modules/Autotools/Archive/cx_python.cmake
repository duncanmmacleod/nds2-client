#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_python( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )
include( Autotools/cm_define )

function(_cx_python_have_include _INCLUDE _VALUE)
  string(TOUPPER ${_INCLUDE} _INCLUDE_UPPER)
  string(REGEX REPLACE "[^A-Za-z0-9]" "_" _INCLUDE_UPPER ${_INCLUDE_UPPER} )
  if ( _VALUE )
    set( _VALUE 1 )
  else()
    set( _VALUE 0 )
  endif()
  cm_define(
    VARIABLE HAVE_${_INCLUDE_UPPER}
    VALUE ${_VALUE}
    DESCRIPTION
      "Define to 1 if you have the <${_INCLUDE}> header file."
      )

endfunction(_cx_python_have_include)

function( cx_python )
  set(options
    CLEAR
    )
  set(oneValueArgs
    VERSION
    INTERP
    PREFIX
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PREFIX )
    set( ARG_PREFIX PYTHON )
  endif ( )

  #----------------------------------------------------------------------
  # PythonInterp
  #----------------------------------------------------------------------
  unset(PYTHONINTERP_FOUND CACHE)
  unset(PYTHON_EXECUTABLE CACHE)
  unset(PYTHON_VERSION_STRING CACHE)
  unset(PYTHON_VERSION_MAJOR CACHE)
  unset(PYTHON_VERSION_MINOR CACHE)
  unset(PYTHON_VERSION_PATCH CACHE)
  #----------------------------------------------------------------------
  # PythonLibs
  #----------------------------------------------------------------------
  unset(PYTHONLIBS_FOUND CACHE)
  unset(PYTHON_LIBRARIES CACHE)
  unset(PYTHON_INCLUDE_PATH CACHE)
  unset(PYTHON_INCLUDE_DIRS CACHE)
  unset(PYTHON_DEBUG_LIBRARIES CACHE)
  unset(PYTHONLIBS_VERSION_STRING CACHE)
  unset(PYTHON_LIBRARY CACHE)
  unset(PYTHON_INCLUDE_DIR CACHE)
  #----------------------------------------------------------------------
  # 
  #----------------------------------------------------------------------
  unset(PYTHON_MODULE_INSTALL_DIR CACHE)
  if ( ARG_CLEAR )
    return()
  endif( ARG_CLEAR )

  # cm_msg_debug_variable(ARG_VERSION)
  if ( NOT ARG_INTERP )
    if ( PYTHON${ARG_VERSION}_EXECUTABLE )
      set( ARG_INTERP ${PYTHON${ARG_VERSION}_EXECUTABLE} )
    else( )
      set( ARG_INTERP python${ARG_VERSION} )
    endif( )
  endif ( NOT ARG_INTERP )
  find_program( PYTHON_EXECUTABLE NAMES ${ARG_INTERP} python )
  cm_msg_debug_variable(ARG_INTERP)
  cm_msg_debug_variable(PYTHON_EXECUTABLE)


  set(
    ENABLE_PYTHON "yes"
    CACHE BOOL "use python "
    )
  if ( ${ENABLE_PYTHON} )
    set(Python_ADDITIONAL_VERSIONS ${ARG_VERSION})

    find_package( PythonInterp ${ARG_VERSION} )
    if ( PYTHONINTERP_FOUND )
      find_package(PythonLibs ${ARG_VERSION})
    endif ( )
  endif ( )
  _cx_python_have_include( "Python.h" PYTHON_INCLUDE_DIRS )
  if ( PYTHONINTERP_FOUND )
    #--------------------------------------------------------------------
    # Determine where packages should be installed
    #--------------------------------------------------------------------
    set( pythondir_script "from distutils.sysconfig import get_python_lib; print( get_python_lib(1,0,\"${CMAKE_INSTALL_PREFIX}\") );" )
    set( pyexecdir_script "from distutils.sysconfig import get_python_lib; print( get_python_lib(1,0,\"${CMAKE_INSTALL_PREFIX}\") );" )

    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c "${pythondir_script}"
      OUTPUT_VARIABLE pythondir_full
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
    set( pythondir_full ${pythondir_full} PARENT_SCOPE )
    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c "${pyexecdir_script}"
      OUTPUT_VARIABLE pyexecdir_full
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
    set( pyexecdir_full ${pyexecdir_full} PARENT_SCOPE )
  endif ( )
  if( NOT PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_MODULE_INSTALL_DIR )
    set( PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_MODULE_INSTALL_DIR ${pythondir_full}
      CACHE PATH "Installation directory for Python modules" )
    # cm_msg_debug_variable(PYTHON_MODULE_INSTALL_DIR)
  endif( )
  if( NOT PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_EXTMODULE_INSTALL_DIR )
    set( PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_EXTMODULE_INSTALL_DIR ${pyexecdir_full}
      CACHE PATH "Installation directory for Python extension modules" )
    # cm_msg_debug_variable(PYTHON_EXTMODULE_INSTALL_DIR)
  endif( )

  cm_msg_debug_variable( PYTHON_EXECUTABLE )
  cm_msg_debug_variable( PYTHON_LIBRARIES )
  cm_msg_debug_variable( PYTHONLIBS_VERSION_STRING )

  set( ${ARG_PREFIX}INTERP_FOUND ${PYTHONINTERP_FOUND} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_EXECUTABLE ${PYTHON_EXECUTABLE} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_VERSION_STRING ${PYTHON_VERSION_STRING} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_VERSION_MAJOR ${PYTHON_VERSION_MAJOR} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_VERSION_MINOR ${PYTHON_VERSION_MINOR} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_VERSION_PATCH ${PYTHON_VERSION_PATCH} CACHE INTERNAL "" )

  set( ${ARG_PREFIX}LIBS_FOUND ${PYTHONLIBS_FOUND} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_LIBRARIES ${PYTHON_LIBRARIES} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_INCLUDE_PATH ${PYTHON_INCLUDE_PATH} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_DEBUG_LIBRARIES ${PYTHON_DEBUG_LIBRARIES} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}LIBS_VERSION_STRING ${PYTHONLIBS_VERSION_STRING} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_LIBRARY ${PYTHON_LIBRARY} CACHE INTERNAL "" )
  set( ${ARG_PREFIX}_INCLUDE_DIR ${PYTHON_INCLUDE_DIR} CACHE INTERNAL "" )
endfunction(cx_python)
