#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_notice( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(cm_msg_notice txt)
  set(prefix "-- ")
  set(echo_cmd "echo")
  set(options
    NO_PREFIX
    NO_NL
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if (${ARG_NO_PREFIX})
    unset(prefix)
  endif (${ARG_NO_PREFIX})
  if (${ARG_NO_NL})
    set(echo_cmd "echo_append")
  endif (${ARG_NO_NL})

  execute_process( COMMAND ${CMAKE_COMMAND} -E ${echo_cmd} "${prefix}${txt}" )
endfunction(cm_msg_notice)
