include( CMakeParseArguments )
include( Autotools/cm_msg_notice )

function(CM_MSG_RESULT MSG)
  set(options)
  set(oneValueArgs )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${MSG}" NO_PREFIX )
endfunction(CM_MSG_RESULT)
