#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#
# cm_deinfe( VARIABLE <variable> VALUE <value> [DESCRIPTION <description>] )
#
#   or
#
# cm_deinfe( <variable> <value> [<ddescription>] )
#------------------------------------------------------------------------
include( CMakeParseArguments )

# include( Autotools/Internal/ci_project_config )
include( Autotools/ch_template )

function(CM_DEFINE)
  # cm_msg_debug( "CM_DEFINE: ${ARGN}" )
  set(options)
  set(oneValueArgs VARIABLE )
  set(multiValueArgs DESCRIPTION VALUE )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_UNPARSED_ARGUMENTS )
    list( LENGTH ARG_UNPARSED_ARGUMENTS ARGN_UNPARSED)
    if ( ${ARGN_UNPARSED} == 3 )
      list( GET ARG_UNPARSED_ARGUMENTS 0 ARG_VARIABLE )
      list( GET ARG_UNPARSED_ARGUMENTS 1 ARG_VALUE )
      list( GET ARG_UNPARSED_ARGUMENTS 2 ARG_DESCRIPTION )
    endif ( ${ARGN_UNPARSED} == 3 )
  endif ( ARG_UNPARSED_ARGUMENTS )

  if ( NOT ARG_VALUE )
    set(ARG_VALUE ${${ARG_VARIABLE}})
  endif ( NOT ARG_VALUE )
  # cm_msg_debug( "cm_define: ${ARG_VARIABLE} ${ARG_VALUE}" )
  set( ${ARG_VARIABLE} "${ARG_VALUE}"
    CACHE INTERNAL "Internal: ${ARG_DESCRIPTION}" FORCE)
  set( prefix "/* " )
  set( suffix "" )
  list( GET ARG_DESCRIPTION -1 last )
  # cm_msg_debug_variable( "cm_define: last: ${last}" )
  ch_template( ${ARG_VARIABLE} "${ARG_DESCRIPTION}" )
  #foreach( line ${ARG_DESCRIPTION} )
  #  # cm_msg_debug_variable( "cm_define: line: ${line}" )
  #  if ( line STREQUAL last )
  #    set( suffix " */" )
  #  endif ( line STREQUAL last )
  #  ci_project_config(
  #    WRITE
  #    "${prefix}${line}${suffix}"
  #    )
  #  set( prefix "   " )
  #endforeach( line )
  #ci_project_config(
  #  VARIABLE ${ARG_VARIABLE} ${ARG_VALUE}
  #  )
endfunction(CM_DEFINE)
