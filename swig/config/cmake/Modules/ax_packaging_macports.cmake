#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
function(ax_packaging_macports)
  find_program(MACPORTSBUILD port)
  find_program(MACPORTSINDEX portindex)
  if (MACPORTSBUILD)
    set( PORT_PKG_DIR "/opt/local/var/macports/distfiles/${PROJECT_NAME}" )
    set( PORT_CONFIG "/opt/local/etc/macports/sources.conf" )
    set( PORT_TOP_DIR ${CMAKE_CURRENT_BINARY_DIR}/test_port )
    set( PORT_TEST_DIR ${PORT_TOP_DIR}/science/${PROJECT_NAME} )
    add_custom_target( macports)
    add_custom_target( macports_portfile
    		  DEPENDS dist
    		  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/test_port/science/${PROJECT_NAME}
    		  COMMAND sed
		        -e 's,[@]RMD160[@],'`openssl rmd160 "${CMAKE_BINARY_DIR}/${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}" | sed -e 's/^.*= //'`',g'
			      -e 's,[@]SHA256[@],'`openssl sha256 "${CMAKE_BINARY_DIR}/${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}" | sed -e 's/^.*= //'`',g'
			      -e 's,[@]SIZE[@],'`stat -f %z "${CMAKE_BINARY_DIR}/${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}"`',g'
			      -e 's,[@]PACKAGE_NAME[@],${PROJECT_NAME},g'
			      -e 's,[@]PACKAGE_VERSION[@],${${PROJECT_NAME}_VERSION},g'
			      -e 's,[@]NDS_HOME_PAGE_URL[@],${NDS_HOME_PAGE_URL},g'
			      -e 's,[@]NDS_SOURCE_URL[@],${NDS_SOURCE_URL},g'
			      -e 's,[@]CPACK_SOURCE_PACKAGE_FILE_EXTENSION[@],${CPACK_SOURCE_PACKAGE_FILE_EXTENSION},g'
			 < ${CMAKE_SOURCE_DIR}/config/cmake/Portfile.in > ${PORT_TEST_DIR}/Portfile
    )
    add_custom_target(macports_build
		      DEPENDS macports_portfile
		      WORKING_DIRECTORY ${PORT_TEST_DIR}
		      COMMAND ${CMAKE_COMMAND} -E make_directory ${PORT_PKG_DIR}
          COMMAND ${CMAKE_COMMAND} -E echo copy "${CMAKE_BINARY_DIR}/${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}" ${PORT_PKG_DIR}
		      COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}" ${PORT_PKG_DIR}
		      COMMAND ${MACPORTSBUILD} lint --nitpick
		      COMMAND ${MACPORTSBUILD} uninstall
		      COMMAND ${MACPORTSBUILD} clean
		      COMMAND ${MACPORTSBUILD} install
    )
   add_custom_target(macports_index
          DEPENDS macports_build
          WORKING_DIRECTORY ${PORT_TOP_DIR}
          COMMAND ${MACPORTSINDEX}
    )
    add_dependencies(macports macports_index)
  else(MACPORTSBUILD)
    add_custom_target(macports
		      COMMAND ${CMAKE_COMMAND} -E echo "This platform does not support building of MacPorts packages")
  endif(MACPORTSBUILD)
endfunction(ax_packaging_macports)
