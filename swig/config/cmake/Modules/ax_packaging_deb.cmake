#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(set_cache_var VAR VALUE DESC)
  if( NOT ${VAR} )
    set( ${VAR} ${VALUE} )
  endif( NOT ${VAR} )
  set( ${VAR} ${${VAR}} CACHE INTERNAL ${DESC} )
endfunction(set_cache_var)

macro( deb_file_setup_ file )
  string( TOUPPER ${file} file_uc)
  set_cache_var(
    PROJECT_DEBIAN_${file_uc}_FILENAME
    ${file}
    "Debian ${File} base name" )
  if ( NOT PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME )
    if( EXISTS "${CMAKE_SOURCE_DIR}/config/cmake/${file}.in" )
      set( PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME
	"${CMAKE_SOURCE_DIR}/config/cmake/${file}.in" )
    else( )
      set( PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME
	PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME-NOTFOUND )
    endif( )
  endif( )
  set_cache_var(
    PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME
    ${PROJECT_DEBIAN_${file_uc}_INPUT_FILENAME}
    "Debian ${File} file to use to generate distributed ${file} file" )
  if( NOT PROJECT_DEBIAN_${file_uc}_OUTPUT_FILENAME )
    set( PROJECT_DEBIAN_${file_uc}_OUTPUT_FILENAME
      "${CMAKE_BINARY_DIR}/debian/${PROJECT_DEBIAN_${file_uc}_FILENAME}" )
  endif( )
  set_cache_var(
    PROJECT_DEBIAN_${file_uc}_OUTPUT_FILENAME
    ${PROJECT_DEBIAN_${file_uc}_OUTPUT_FILENAME}
    "Debian ${File} file to use for distribution" )
endmacro( )

deb_file_setup_( control )
deb_file_setup_( rules )

function(cx_packaging_debian_file)
  set(options)
  set(oneValueArgs INPUT OUTPUT BASE)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if(NOT ARG_INPUT)
    set(ARG_INPUT "${CMAKE_SOURCE_DIR}/config/cmake/${ARG_BASE}.in")
  endif(NOT ARG_INPUT)
  if(NOT ARG_OUTPUT)
    set(ARG_OUTPUT "${CMAKE_BINARY_DIR}/debian/${ARG_BASE}")
  endif(NOT ARG_OUTPUT)
  foreach( line ${PROJECT_DESCRIPTION_LONG})
    list(APPEND new_desc_ "  ${line}")
  endforeach( )
  string( REPLACE ";" "\n" PROJECT_DESCRIPTION_LONG "${new_desc_}")
  configure_file( ${ARG_INPUT} ${ARG_OUTPUT} @ONLY )
endfunction(cx_packaging_debian_file)

function(ax_packaging_deb)
  if ( PROJECT_DEBIAN_CONTROL_INPUT_FILENAME )
    cx_packaging_debian_file(
      INPUT ${PROJECT_DEBIAN_CONTROL_INPUT_FILENAME}
      OUTPUT ${PROJECT_DEBIAN_CONTROL_OUTPUT_FILENAME}
      BASE "control" )
  elseif( EXISTS "${CMAKE_SOURCE_DIR}/config/cmake/control.in" )
    cx_packaging_debian_file( BASE "control" )
  endif ( )
  if ( PROJECT_DEBIAN_RULES_INPUT_FILENAME )
    cx_packaging_debian_file(
      INPUT ${PROJECT_DEBIAN_RULES_INPUT_FILENAME}
      OUTPUT ${PROJECT_DEBIAN_RULES_OUTPUT_FILENAME}
      BASE rules )
  elseif( EXISTS "${CMAKE_SOURCE_DIR}/config/cmake/rules.in" )
    cx_packaging_debian_file( BASE "rules" )
  endif ( PROJECT_DEBIAN_RULES_INPUT_FILENAME )
  if( "${CMAKE_GENERATOR}" MATCHES "Make" )
    if ( EXISTS ${PROJECT_DEBIAN_CONTROL_INPUT_FILENAME} )
      add_custom_target( dist_body_deb_control
	      COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/debian/control" ${STAGING_DIR}/${TAR_DIR_NAME}/debian/.
	      DEPENDS dist_body_begin
	      )
      add_dependencies( dist_body dist_body_deb_control )
    endif( )
    if ( EXISTS ${PROJECT_DEBIAN_RULES_INPUT_FILENAME} )
      add_custom_target( dist_body_deb_rules
	      COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/debian/rules" ${STAGING_DIR}/${TAR_DIR_NAME}/debian/.
	      DEPENDS dist_body_begin
	      )
      add_dependencies( dist_body dist_body_deb_rules )
    endif( )
  endif (  )
  find_program(DEBBUILD dpkg-buildpackage)
  if (DEBBUILD)
    set(DEBSOURCEPKGORIG ${PACKAGE}_$(VERSION).orig.tar)
    set(DEBSOURCEPKG ${DEBSOURCEPKGORIG}.gz)
    set(DEBSOURCEDIR ${PACKAGE}-${VERSION})
    set(TAR_DIR_NAME "${PROJECT_NAME}-${PROJECT_VERSION}")
    set(TAR_NAME "${TAR_DIR_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}")
    set(DEBPKGTESTDIR "${CMAKE_CURRENT_BINARY_DIR}/tdebian")
    add_custom_target(deb
		      COMMAND ${CMAKE_COMMAND} -E echo "Building Debian  package..."
		      COMMAND ${CMAKE_COMMAND} -E make_directory ${DEBPKGTESTDIR}
		      COMMAND ${CMAKE_COMMAND} -E chdir ${DEBPKGTESTDIR} tar xf "${CMAKE_CURRENT_BINARY_DIR}/${TAR_NAME}"
		      COMMAND ${CMAKE_COMMAND} -E chdir "${DEBPKGTESTDIR}/${TAR_DIR_NAME}" ${DEBBUILD} -rfakeroot -D -us -uc -b
		      )
    add_dependencies(deb dist)
  else (DEBBUILD)
    add_custom_target(deb
		      COMMAND ${CMAKE_COMMAND} -E echo "This platform does not support building of debian packages")
  endif(DEBBUILD)
endfunction(ax_packaging_deb)
