# Import module
import calendar
import time
import nds2

# GPS time for rannge
gpsoffset = 315964818
gpsend = calendar.timegm( time.gmtime() ) - gpsoffset - 16
gpsstart = gpsend - 4

# Open connection and print some information about the server
conn = nds2.connection('llodts0.ligo-la.caltech.edu')
print("Connection:", conn)
print("Host:", conn.get_host())
print("Port:", conn.get_port())
print("Protocol:", conn.get_protocol())

# Get list of available channels and print some information about them
channels = conn.find_channels('X2:HPI-BS*MON')
print("Number matching of channels:", len(channels))
print("First channel:", channels[0])
print("First channel name:", channels[0].name)
print("Second channel name:", channels[1].name)
print("Third channel name:", channels[2].name)

# Fetch some data
data = conn.fetch(gpsstart, gpsend, [ channels[0].name ])
print("A buffer:", data[0])
print("Its data:", data[0].data)
print("Its channel:", data[0].channel)

print("Now block by block...")
for bufs in conn.iterate(gpsstart, gpsend, 1, [channels[0].name, channels[1].name, channels[2].name ]):
    print(bufs)
