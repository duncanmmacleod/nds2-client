import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

if conn.get_protocol() == nds2.connection.PROTOCOL_ONE:
    assert( conn.count_channels("*") == 4*11 )
    assert( conn.count_channels("*4*") == 11 )
    assert( conn.count_channels("*4*", nds2.channel.DEFAULT_CHANNEL_MASK, nds2.channel.DEFAULT_DATA_MASK, 0.9, 1.1) == 5 )
    assert( conn.count_channels("*", nds2.channel.CHANNEL_TYPE_ONLINE) == 4 )
    assert( conn.count_channels("*", nds2.channel.CHANNEL_TYPE_ONLINE, nds2.channel.DEFAULT_DATA_MASK, 10.0, 300.0) == 4 )
else:
    assert( conn.count_channels("*") == 12 )
    assert( conn.count_channels("*4*") == 3 )
    assert( conn.count_channels("*4*", nds2.channel.DEFAULT_CHANNEL_MASK, nds2.channel.DEFAULT_DATA_MASK, 0.9, 1.1) == 1)
    assert( conn.count_channels("*", nds2.channel.CHANNEL_TYPE_RAW) == 4 )
    assert( conn.count_channels("*", nds2.channel.CHANNEL_TYPE_RAW, nds2.channel.DEFAULT_DATA_MASK, 10.0, 300.0) == 4 )