from __future__ import print_function

import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

start = 1108835634
end = start + 4

channels = [
    "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
    "H1:SUS-BS_BIO_M1_MSDELAYON",
    "H1:SUS-BS_COMMISH_STATUS",
    "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
    "H1:SUS-BS_DCU_ID",
    "H1:SUS-BS_DITHERP2EUL_1_1",
    "H1:SUS-BS_DITHERP2EUL_2_1",
    "H1:SUS-BS_DITHERY2EUL_1_1",
    "H1:SUS-BS_DITHERY2EUL_2_1",
    "H1:SUS-BS_DITHER_P_IPCERR",
]

if '-explicit-args' in sys.argv:
    params = nds2.parameters(host, port, nds2.connection.PROTOCOL_ONE)
    data = nds2.fetch(channels, start, end, params=params)
elif '-named-params' in sys.argv:
    data = nds2.fetch(channels, start, end, hostname=host, port=port, protocol=nds2.connection.PROTOCOL_ONE)
else:
    os.environ['NDSSERVER'] = "{0}:{1}".format(host, port)
    os.environ['NDS2_CLIENT_PROTOCOL_VERSION'] = '1'
    data = nds2.fetch(channels, start, end)
assert(len(data) == len(channels))
for i in range(len(channels)):
    assert(data[i].name == channels[i])
