import sys
import nds2

# This test requires the version number to be passed on the command line

assert nds2.__version__ == nds2.version()
assert nds2.__version__ == sys.argv[1]