from __future__ import print_function
import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
    chans = ["X1:FEC-117_CPU_METER",]
    expected_samples = 1
    abort_at = 16
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO
    chans = ["X1:FEC-117_CPU_METER,online",]
    expected_samples = 16
    abort_at = 1

path2 = '-option-2' in sys.argv

conn = nds2.connection(host, port, proto)

if not path2:
    i = 0
    for bufs in conn.iterate(nds2.connection.FAST_STRIDE, chans):
        print("{0} {1} {2}".format(bufs[0].Start(), bufs[0].Stop(), bufs[0].Samples()))
        assert( bufs[0].Samples() == expected_samples )
        i += 1
        if i >= abort_at:
            break

else:
    for bufs in conn.iterate(0, 1, nds2.connection.FAST_STRIDE, chans):
        assert( bufs[0].Samples() == expected_samples )
