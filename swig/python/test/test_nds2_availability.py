from __future__ import print_function
import os

import nds2 as nds

hostname = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    hostname = os.environ['NDS_TEST_HOST']

port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

conn = nds.connection(hostname, port, nds.connection.PROTOCOL_TWO)

start = 1116733655
stop = 1116733675

channels = [
    "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ",
    "H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ",
    "H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ",
    "H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ",
    "H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ",
    "H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ",
    "H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ",
    "H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ",
    "H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ",
    "H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ",
    "H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ",
    "H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ",
    "H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ",
    "H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ",
    "H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ",
    "H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ",
]

conn.set_epoch(start, stop)

avail = conn.get_availability(channels)
assert( len(channels) == len(avail) )

for i in range(len(channels)):
    entry = avail[i]
    assert(channels[i] == entry.name)
    assert(len(entry.data) == 2)
    assert(entry.data[0].frame_type == "H-H1_C")
    assert(entry.data[1].frame_type == "H-H1_R")
    assert(entry.data[0].gps_start == entry.data[1].gps_start)
    assert(entry.data[0].gps_stop == entry.data[1].gps_stop)
    assert(entry.data[0].gps_start == start)
    assert(entry.data[0].gps_stop == stop)

simple_list = avail.simple_list()
assert(len(simple_list) == len(avail))

entry = simple_list[0]
assert(len(entry) == 1)
assert(entry[0].gps_start == start)
assert(entry[0].gps_stop == stop)

# there is an implicit availability check here
conn.fetch(start, stop, channels)

# test a larger availability request
conn.set_epoch(0, 1999999999)
conn.get_availability(['H1:GDS-CALIB_STRAIN,reduced',])