from __future__ import print_function

import nds2
import os

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

c = nds2.connection(host, port, nds2.connection.PROTOCOL_ONE)

start = 1108835634
end = start + 4

lst = c.find_channels('H1:SUS-BS*')

# channels = []
# for i in range(10):
#     j = i*55
#     name = lst[j].name
#     while (name.find("-trend") >= 0) or (name.find('_EXC') >= 0) or (channels.count(name) > 0) or (name.find('_M2') >= 0) or (name.find('_DITHERINF') >= 0):
#         j = j + 1
#         name = lst[j].name
#     #print "%s %d %d %d" % (name, name.find("-trend"), name.find('_EXC'), channels.count(name))
#     channels.append(name)
# print "Lookup on:"
# for name in channels:
#     print " ", name
channels = [
    "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
    "H1:SUS-BS_BIO_M1_MSDELAYON",
    "H1:SUS-BS_COMMISH_STATUS",
    "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
    "H1:SUS-BS_DCU_ID",
    "H1:SUS-BS_DITHERP2EUL_1_1",
    "H1:SUS-BS_DITHERP2EUL_2_1",
    "H1:SUS-BS_DITHERY2EUL_1_1",
    "H1:SUS-BS_DITHERY2EUL_2_1",
    "H1:SUS-BS_DITHER_P_IPCERR",
]

data = c.fetch(start, end, channels)

for stream in data:
    ch1 = stream.channel
    ch2 = c.find_channels(ch1.name)[0]

    print( "ch1={} ch2={}\n".format(str(ch1), str(ch2)) )

    assert ch1.name == ch2.name
    assert ch1.data_type == ch2.data_type
    assert ch1.channel_type == ch2.channel_type
    assert ch1.sample_rate == ch2.sample_rate
    assert ch1.signal_gain == ch2.signal_gain
    assert ch1.signal_offset == ch2.signal_offset
    assert ch1.signal_slope == ch2.signal_slope
    assert ch1.signal_units == ch2.signal_units
c.close()
