import nds2
import os
import sys


host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

channels = [
    "H1:DAQ-DC0_GPS",
]

start = 1169928640
end = start + 2
stride = 1

conn.set_parameter("GAP_HANDLER", "STATIC_HANDLER_ZERO")
conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")

error_w_gap_handler = False
try:
    for bufs in conn.iterate(start, end, 1, channels):
        assert( bufs[0].data[0] == 0.0 )
except RuntimeError:
    error_w_gap_handler = True

assert (error_w_gap_handler == False)

error_wo_gap_handler = False
try:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
    for bufs in conn.iterate(start, end, channels):
        assert( False )
except RuntimeError:
    error_wo_gap_handler = True
assert (error_wo_gap_handler == False)