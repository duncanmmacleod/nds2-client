import nds2
import os
import sys


def is_gap_second(input_gps):
    # no gaps in this test
    return False


def check_data(buf, val):
    samples_per_sec = int(buf.sample_rate)

    offset = 0
    for cur_gps in range(buf.Start(), buf.Stop()):
        expected = val
        if is_gap_second(cur_gps):
            expected = 0.0
        for i in range(samples_per_sec):
            assert( buf.data[offset] == expected )
            offset += 1


host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
    chans = ["X1:PEM-1", "X1:PEM-2"]
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO
    chans = ["X1:PEM-1,online", "X1:PEM-2,online"]

conn = nds2.connection(host, port, proto)

if "-no-gap" in sys.argv:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
else:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")


i = 0
for bufs in conn.iterate(0, 20, 10, chans):
    multiplier = 10
    if proto == nds2.connection.PROTOCOL_ONE:
        multiplier = 1

    expected = [1.5, 2.75]
    if bufs[0].gps_seconds >= 1770000010:
        expected = [3.5, 4.75]

    assert( bufs[0].Samples() == 256 * multiplier )
    check_data(bufs[0], expected[0])
    assert( bufs[1].Samples() == 512 * multiplier )
    check_data(bufs[1], expected[1])
    i += 1

if proto == nds2.connection.PROTOCOL_ONE:
    assert( i == 20 )
else:
    assert( i == 2 )


# make sure we can do other operations
conn.set_epoch(1770000000, 1770000000+20)
conn.find_channels("X1:PEM-1,online")
conn.find_channels("X1:PEM-2,online")

i = 0
for bufs in conn.iterate(0, 2, chans):
    assert( bufs[0].Samples() == 256 )
    check_data(bufs[0], 1.5)
    assert( bufs[1].Samples() == 512 )
    check_data(bufs[1], 2.75)
    i += 1

assert( i == 2 )