from __future__ import print_function

import nds2
import os

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

c = nds2.connection(host, port, nds2.connection.PROTOCOL_TWO)

assert( c.set_parameter("GAP_HANDLER", "STATIC_HANDLER_NEG_INF") )

channels = ['X1:PEM-1',]
buffers = c.fetch(1219524930, 1219524930 + 4, channels)
buf = buffers[0]

for entry in buf.data:
    assert(entry == -2147483648)