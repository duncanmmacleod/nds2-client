import nds2
import os
import re
import sys


print( "DEBUG: entering client"); sys.stdout.flush( )
host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

if "-no-gap" in sys.argv:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
else:
    conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")

gps_start = 1770000000
gps_stop  = 1770000020
chans = ["X1:PEM-1", "X1:PEM-2"]

range_except_pattern = re.compile( "GPS start time is greater than GPS stop time" )
correct_exception = False
try:
    cur_gps = gps_stop
    for bufs in conn.iterate(gps_stop, gps_start, 10, chans):
        cur_gps = bufs[0].Stop( )
except Exception as e:
    print( "DEBUG: type: %s" % str( type(e) ) )
    if ( range_except_pattern.match( repr( e ) ) ):
        correct_exception = True
finally:
    assert( correct_exception, "Caught range error exception" )

cur_gps = gps_start
for bufs in conn.iterate(gps_start, gps_stop, 10, chans):
    cur_gps = bufs[0].Stop( )

correct_exception = False
try:
    print( "DEBUG: Starting exception test" )
    conn.set_epoch(gps_stop, gps_start)
    conn.find_channels("X1:PEM-1")
    conn.find_channels("X1:PEM-2")
except Exception as e:
    print( "DEBUG: type: %s" % str( type(e) ) )
    if ( range_except_pattern.match( repr( e ) ) ):
        correct_exception = True
finally:
    assert( correct_exception, "Caught range error exception" )

# make sure we can do other operations
conn.set_epoch(gps_start, gps_stop)
conn.find_channels("X1:PEM-1")
conn.find_channels("X1:PEM-2")
print( "DEBUG: exiting client")
