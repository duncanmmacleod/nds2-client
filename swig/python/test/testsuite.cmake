# -*- coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
include( CMakeParseArguments )

function(mock_server_test name command)
    if( WIN32 )
        set(TPATH ${NDS_LIBRARY_DIR})
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR};${TPATH}" )
        endif( )
    else( WIN32 )
        set(TPATH "$ENV{PATH}")
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR}:${TPATH}" )
        endif( )
    endif( WIN32 )
    add_test(
        NAME ${name}
        COMMAND
        "${PYTHON_EXEC}" "${PROG_NDS_TESTER}" ${NDS_TESTER_FLAGS}
        --nds1-mock-server
        "${TEST_SOURCE_DIR}/${command}" )
    set_tests_properties( ${name}
        PROPERTIES
        ENVIRONMENT
        "PYTHONPATH=${TPATH};PYTHON=${PYTHON_EXEC}"
        SKIP_RETURN_CODE 77 )
    set( NDS_CUSTOM_MEMCHECK_IGNORE
        ${NDS_CUSTOM_MEMCHECK_IGNORE} ${name}
        CACHE INTERNAL "" )
endfunction(mock_server_test)

function(replay_server_test name command json)
    Message("" "replay_test name=${name} command=${command} json=${json} argn=${ARGN}")
    if( WIN32 )
        set(TPATH ${NDS_LIBRARY_DIR})
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR};${TPATH}" )
        endif( )
    else( WIN32 )
        set(TPATH "$ENV{PATH}")
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR}:${TPATH}" )
        endif( )
    endif( WIN32 )
    if ( REPLAY_BLOB_CACHE_DIR )
        set_nds_protocols(protocols ${name})
        foreach ( proto ${protocols} )
            set(test_name "${name}${proto}_")
            add_test(
                NAME ${test_name}
                COMMAND
                "${PYTHON_EXEC}" "${PROG_NDS_TESTER}" ${NDS_TESTER_FLAGS}
                --replay-server
                --replay-json-filename ${json}
                --replay-protocol ${proto}
                "${TEST_SOURCE_DIR}/${command}" ${ARGN} )
            set_tests_properties( ${test_name}
                PROPERTIES
                ENVIRONMENT
                "PYTHONPATH=${TPATH};REPLAY_BLOB_REMOTE_CACHE=${REPLAY_BLOB_CACHE_DIR};PYTHON=${PYTHON_EXEC}"
                SKIP_RETURN_CODE 77 )
            set( NDS_CUSTOM_MEMCHECK_IGNORE
                ${NDS_CUSTOM_MEMCHECK_IGNORE} ${test_name}
                CACHE INTERNAL "" )
        endforeach ( proto )
    endif ( REPLAY_BLOB_CACHE_DIR )
endfunction(replay_server_test)

function(add_python_test target)
    list(GET ARGN 0 cmd )
    list(REMOVE_AT ARGN 0)
    if( WIN32 )
        set(TPATH ${NDS_LIBRARY_DIR})
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR};${TPATH}" )
        endif( )
    else( WIN32 )
        set(TPATH "$ENV{PATH}")
        if ( PYTHON_MODULE_DIR )
            set( TPATH "${PYTHON_MODULE_DIR}:${TPATH}" )
        endif( )
    endif( WIN32 )
    add_test( ${target} 
        "${PYTHON_EXEC}" "${PROG_NDS_TESTER}" ${NDS_TESTER_FLAGS}
        "${TEST_SOURCE_DIR}/${cmd}" ${ARGN})
    set_tests_properties( ${target}
        PROPERTIES
        ENVIRONMENT "PYTHONPATH=${TPATH};PYTHON=${PYTHON_EXEC}"
        SKIP_RETURN_CODE 77 )
endfunction(add_python_test)
#-----------------------------------------------------------------------
#-----------------------------------------------------------------------

function(do_python_tests)
    set(options)
    set(oneValueArgs NAMESPACE PREFIX PYTHON_MODULE_DIR TEST_SOURCE_DIR)
    set(multiValueArgs)
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if ( NOT ARG_PREFIX )
        set( ARG_PREFIX PYTHON )
    endif( )

    if ( ARG_NAMESPACE )
        set( PYTHON_NAMESPACE ${ARG_NAMESPACE} )
    else ( )
        set( PYTHON_NAMESPACE "python" )
    endif ( )
    if ( ARG_PYTHON_MODULE_DIR )
        set( PYTHON_MODULE_DIR ${ARG_PYTHON_MODULE_DIR} )
    endif ( )
    if ( ARG_TEST_SOURCE_DIR )
        set( TEST_SOURCE_DIR ${ARG_TEST_SOURCE_DIR} )
    else ( )
        set( TEST_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} )
    endif ( )

    set( PYTHON_EXEC ${${ARG_PREFIX}_EXECUTABLE} )

    if (WIN32)
        set( NDS_LIBRARY_DIR "" )
        set( NDS_LIBRARY_DIR $ENV{PATH} )
        set( GENERATOR "Debug" )
        list( INSERT NDS_LIBRARY_DIR 0 "${CMAKE_BINARY_DIR}/bin/${GENERATOR}" )
        set( TMP_NDS_LIBRARY_DIR "" )
        foreach( TMP ${NDS_LIBRARY_DIR} )
            file( TO_NATIVE_PATH ${TMP} TMP )
            string( REPLACE "\\" "\\\\" TMP ${TMP} )
            #list( APPEND TMP_NDS_LIBRARY_DIR ${TMP} )
            string( LENGTH "${TMP_NDS_LIBRARY_DIR}" L )
            if( $L LESS 1 )
                set( TMP_NDS_LIBRARY_DIR ${TMP} )
            else( $L LESS 1 )
                set( TMP_NDS_LIBRARY_DIR "${TMP_NDS_LIBRARY_DIR}\\\\;${TMP}" )
            endif( $L LESS 1 )
        endforeach( TMP )
        #join( ${TMP_NDS_LIBRARY_DIR} ";" NDS_LIBRARY_DIR )
        set( NDS_LIBRARY_DIR ${TMP_NDS_LIBRARY_DIR} )
        string( REPLACE "\\" "\\\\" NDS_LIBRARY_DIR ${NDS_LIBRARY_DIR} )
        #get_target_property( NDS_LIBRARY_DIR nds2JNI LIBRARY_OUTPUT_DIRECTORY )
        message( STATUS "NDS_LIBRARY_DIR: ${NDS_LIBRARY_DIR}" )
    endif(WIN32)

    #====================================================================
    # These tests require a minimum of Python
    #====================================================================
    # Final variable substitution before use
    #--------------------------------------------------------------------
    string(CONFIGURE "${NDS_TESTER_FLAGS}" NDS_TESTER_FLAGS )
    #--------------------------------------------------------------------
    add_python_test(_${PYTHON_NAMESPACE}_is-trend-type_ test_is_trend_type.py)
    #--------------------------------------------------------------------
    add_python_test(_${PYTHON_NAMESPACE}_version_ test_nds_version.py "${PACKAGE_VERSION}")
    #--------------------------------------------------------------------
    mock_server_test( _${PYTHON_NAMESPACE}_mockup_ test_mockup.py )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_test_nds2_iterate_with_full_gap_
        test_iterate_with_full_gap.py
        nds2-iterate-with-full-gap.json)
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_test_nds2_fetch_all_gap_
        test_fetch_all_gap.py
        nds2-fetch-all-gap.json
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_fetch_
        test_fetch.py
        lho-prod.json )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_list_testpoint_
            test_nds1_list_testpoint.py
            nds1-list-testpoint.json
            -proto-1
    )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_find-channels-synthentic-standalone_
            test_nds2_find_channels_synthetic_standalone.py
            nds2-find-channels-synthetic-standalone.json
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_find-channels-synthetic_
        test_nds2_find_channels_synthetic.py
        nds2-find-channels-synthetic.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_availability_
        test_nds2_availability.py
        nds2-availability.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_get-epochs_
        test_get_epochs.py
        nds1-get-epoch.json -proto-1 )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_get-epochs_
        test_get_epochs.py
        nds2-get-epoch.json -proto-2 )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_dissimilar-gaps_
        test_nds2_dissimilar_gaps.py
        nds2-dissimilar_gaps.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_ticket-169_
        test_ticket_169.py
        nds2-ticket-169.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_ticket-242_
        test_ticket_242.py
        nds2-ticket-242.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_ticket-246_
        test_ticket_246.py
        nds2-ticket-246.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-with-gaps_
        test_iterate_with_gaps.py
        nds1-iterate_with_leading_gap.json
        -proto-1 )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-with-gaps_
        test_iterate_with_gaps.py
        nds2-iterate_with_leading_gap.json
        -proto-2 )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-no-data_
        test_nds2_iterate_no_data.py
        nds2-iterate_with_no_data.json
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_count-channels_
        test_nds_count_channels.py
        nds1-count-channels-synthetic.json
        -proto-1 )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_count-channels_
        test_nds_count_channels.py
        nds2-count-channels-synthetic.json
        -proto-2 )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_fetch_standalone_namedparams_
            test_fetch_standalone.py
            nds1-test-fetch-standalone.json
            -named-params
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_fetch_standalone_
            test_fetch_standalone.py
            nds1-test-fetch-standalone.json
            -explicit-args
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_fetch_standalone_env_
            test_fetch_standalone.py
            nds1-test-fetch-standalone.json
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_timeout_
        test_timeout.py
        nds1-timeout.json
        -proto-1)
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_timeout_
        test_timeout.py
        nds2-timeout.json
        -proto-2)
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_ticket-288_
        test_ticket_288.py
        nds2-ticket-288.json)
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_ticket-289_
        test_ticket_289.py
        nds2-ticket-289.json)
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone_namedparams_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -named-params
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -explicit-args
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone-nogaphandler_namedparams_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -no-gap -named-params
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone-nogaphandler_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -no-gap -explicit-args
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone-env_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_iterate-all-data-standalone-env-nogaphandler_
            test_nds_iterate_all_data_standalone.py
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -no-gap
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-all-data_
        test_nds_iterate_all_data.py
        nds2-iterate-all-data-synthetic.json
        -proto-2
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-all-data-nogaphandler_
        test_nds_iterate_all_data.py
        nds2-iterate-all-data-synthetic.json
        -proto-2 -no-gap
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-all-data_
        test_nds_iterate_all_data.py
        nds1-iterate-all-data-synthetic.json
        -proto-1
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-all-data-nogaphandler_
        test_nds_iterate_all_data.py
        nds1-iterate-all-data-synthetic.json
        -proto-1 -no-gap
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-with-synth-gaps_
        test_nds_iterate_with_synth_gaps.py
        nds1-iterate-with-gaps-synthetic.json
        -proto-1
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-with-synth-gaps-nogaphandler_
        test_nds_iterate_with_synth_gaps.py
        nds1-iterate-with-gaps-synthetic.json
        -proto-1 -no-gap
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-with-synth-gaps-defaultgaphandler_
        test_nds_iterate_with_synth_gaps.py
        nds1-iterate-with-gaps-synthetic.json
        -proto-1 -default-gap-handling
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-live-data-bounded_
        test_nds_iterate_live_data_bounded.py
        nds2-iterate-live-data-bounded-synthetic.json
        -proto-2
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-live-data-bounded_
        test_nds_iterate_live_data_bounded.py
        nds1-iterate-live-data-bounded-synthetic-multiconn.json
        -proto-1
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_iterate-live-data_
        test_nds_iterate_live_data.py
        nds2-iterate-live-data-synthetic.json
        -proto-2
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_iterate-live-data_
        test_nds_iterate_live_data.py
        nds1-iterate-live-data-synthetic.json
        -proto-1
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds1_get_parameters_
        test_nds_get_parameters.py
        nds1-get-parameters.json
        -proto-1
        )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_get_parameters_
        test_nds_get_parameters.py
        nds2-get-parameters.json
        -proto-2
        )
    #--------------------------------------------------------------------
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_env_connection_nds2client_
        test_env_connection.py
        nds2-env_connection.json
        -option1
    )
    replay_server_test(
        _${PYTHON_NAMESPACE}_nds2_env_connection_ndsserver_
        test_env_connection.py
        nds2-env_connection.json
        -option2
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_env_connection_nds2client_params_
            test_env_connection.py
            nds2-env_connection.json
            -option1 -params
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_env_connection_ndsserver_params_
            test_env_connection.py
            nds2-env_connection.json
            -option2 -params
    )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_fast_data_
            test_nds_fast_data.py
            nds1-fast-data.json
            -proto-1 -option-1
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_fast_data_
            test_nds_fast_data.py
            nds2-fast-data.json
            -proto-2 -option-1
    )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds1_fast_data_bounded_
            test_nds_fast_data.py
            nds1-fast-data-bounded.json
            -proto-1 -option-2
    )
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_fast_data_bounded_
            test_nds_fast_data.py
            nds2-fast-data-bounded.json
            -proto-2 -option-2
    )
    #--------------------------------------------------------------------
    replay_server_test(
            _${PYTHON_NAMESPACE}_nds2_time-range_
            test_nds_time_range_error.py
            nds2-iterate-all-data-synthetic.json
            -proto-2
    )
    #--------------------------------------------------------------------
endfunction( )
