import nds2
import os
import sys

# This python code would fail
#
# The cause of it was the availability check changing the channel from a
# s-trend to an m-trend
#
# Test availability and the actual data transfer
#
# c = nds.connection('nds.ligo-wa.caltech.edu', 31200)
# dat = c.fetch(1107878416, 1107878420, H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend)

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

channels = [
    "H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend",
]

start = 1116286100
end = start + 200
stride = 50
data_start = 1116286200
last_start = 0
samples_per_segment = stride

if proto == nds2.connection.PROTOCOL_ONE:
    start = 1130797740
    stride = 3000
    end = start + stride*3
    samples_per_segment = stride/60

    channels = [
        "X1:CDS-DACTEST_COSINE_OUT_DQ.n,m-trend",
    ]

conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
for bufs in conn.iterate(start, end, channels):
    pass


count = 0
for bufs in conn.iterate(start, end, channels):
    count += 1

conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")
conn.set_parameter("GAP_HANDLER", "STATIC_HANDLER_ZERO");

for bufs in conn.iterate(start, end, stride, channels):
    assert( len(bufs) == 1 )
    if last_start == 0:
        assert( bufs[0].gps_seconds == start )
    else:
        assert( bufs[0].gps_seconds == last_start + stride )
    assert( bufs[0].Stop() == end or bufs[0].length == samples_per_segment )
    last_start = bufs[0].gps_seconds
