
if (nds_check_cpp11_included)
    return()
endif(nds_check_cpp11_included)
set(nds_check_cpp11_included TRUE)

INCLUDE(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG(-std=c++0x HAS_CXX_0X)
CHECK_CXX_COMPILER_FLAG(-std=c++11 HAS_CXX_11)

if (${HAS_CXX_11})
    set(CPPSTD_FLAG "-std=c++11")
else (${HAS_CXX_11})
    if (${HAS_CXX_0X})
        set(CPPSTD_FLAG "-std=c++0x")
    else (${HAS_CXX_0X})
        set(CPPSTD_FLAG "")
    endif (${HAS_CXX_0X})
endif (${HAS_CXX_11})

# To make this easy, we want cmake 3.1+
# it is simple if that is present, else we need to autodetect flags
if (${CMAKE_VERSION} VERSION_GREATER "3.0.99")

    macro(target_requires_cpp11 target mode)
        target_compile_features(${target} ${mode} cxx_auto_type)
    endmacro()

else (${CMAKE_VERSION} VERSION_GREATER "3.0.99")

    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
        MESSAGE(FATAL "When building with MSVC please use a newer CMAKE (3.1+) to allow C++11 support to be specified")
    endif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")

    macro(target_requires_cpp11 target mode)
        target_compile_options(${target} ${mode} ${CPPSTD_FLAG})
    endmacro()

endif (${CMAKE_VERSION} VERSION_GREATER "3.0.99")
