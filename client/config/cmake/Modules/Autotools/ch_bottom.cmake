#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( Autotools/Internal/ci_cache )
include( Autotools/Internal/ci_join )

#------------------------------------------------------------------------
# ch_bottom( text )
#
#   Place all text at the bottom of the configuration file
#
# NOTE:
#   This is part of the autoheader look and feel
#------------------------------------------------------------------------
function( ch_bottom )
  unset( CH_BOTTOM_TEXT CACHE )
  ci_join( CH_BOTTOM_TEXT "\n" ${ARGN} )
  cm_cache( CH_BOTTOM_TEXT VALUE ${CH_BOTTOM_TEXT} )
endfunction()

