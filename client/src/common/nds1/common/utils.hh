//
// Created by jonathan.hanks on 5/4/18.
//

#ifndef NDS2_CLIENT_NDS1_COMMON_UTILS_HH
#define NDS2_CLIENT_NDS1_COMMON_UTILS_HH

#include "common/utils.hh"

namespace nds_impl
{
    namespace nds1
    {
        namespace common
        {

            template < typename It >
            nds_impl::common::Span< char >
            identify_padded_string( It begin, It end )
            {
                nds_impl::common::Span< char >::size_type length = 0;
                for ( It cur = begin; cur != end; ++cur, ++length )
                {
                    if ( *cur == '\0' || *cur == ' ' )
                    {
                        break;
                    }
                }
                return nds_impl::common::Span< char >( &( *begin ), length );
            }
        }
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include <vector>
#include <string>

#include "common/utils.hh"
#include "catch.hpp"

TEST_CASE( "There are several fields in NDS1 that are ' ' or 0 padded, given a "
           "fixed length buffer, identify when the string ends",
           "[nds1]" )
{

    std::vector< char > buf1{
        '0', '1', '2', '3', '4', '5', ' ', ' ', ' ', ' '
    };

    nds_impl::common::Span< char > span1 =
        nds_impl::nds1::common::identify_padded_string( buf1.begin( ),
                                                        buf1.end( ) );
    REQUIRE( span1.data( ) == buf1.data( ) );
    REQUIRE( span1.size( ) == 6 );

    std::vector< char > buf2{ '0', '1', '2', '3', '4', '5', 0, 0, 0, 0 };

    nds_impl::common::Span< char > span2 =
        nds_impl::nds1::common::identify_padded_string( buf2.begin( ),
                                                        buf2.end( ) );
    REQUIRE( span2.data( ) == buf2.data( ) );
    REQUIRE( span2.size( ) == 6 );

    std::vector< char > buf3{
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    nds_impl::common::Span< char > span3 =
        nds_impl::nds1::common::identify_padded_string( buf3.begin( ),
                                                        buf3.end( ) );
    REQUIRE( span3.data( ) == buf3.data( ) );
    REQUIRE( span3.size( ) == 10 );

    std::vector< char > buf4;
    buf4.push_back( '0' ); // just make sure it is not empty

    nds_impl::common::Span< char > span4 =
        nds_impl::nds1::common::identify_padded_string( buf4.begin( ),
                                                        buf4.begin( ) );
    REQUIRE( span4.data( ) == buf4.data( ) );
    REQUIRE( span4.size( ) == 0 );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS2_CLIENT_NDS1_COMMON_UTILS_HH
