//
// Created by jonathan.hanks on 5/7/18.
//
#include <cstdint>
#include <sstream>
#include <string>

#include "nds1_test_utilities.hh"

#include "nds1/v12_2/channel_listing.hh"

namespace nds_testing
{

    namespace
    {
        std::string
        right_pad_string( std::string input, char pad_value, size_t width )
        {
            std::vector< char > buf( width, pad_value );
            if ( input.size( ) > width )
            {
                input = input.substr( 0, width );
            }
            std::copy( input.begin( ), input.end( ), buf.begin( ) );
            return std::string{ buf.data( ), buf.size( ) };
        }

        std::string
        left_pad_string( std::string input, char pad_value, size_t width )
        {
            std::vector< char > buf( width, pad_value );
            if ( input.size( ) > width )
            {
                input = input.substr( 0, width );
            }
            std::copy( input.rbegin( ), input.rend( ), buf.rbegin( ) );
            return std::string{ buf.data( ), buf.size( ) };
        }

        template < typename T >
        std::string
        to_hex( T value )
        {
            std::array< char, 16 > lookup{ '0', '1', '2', '3', '4', '5',
                                           '6', '7', '8', '9', 'a', 'b',
                                           'c', 'd', 'e', 'f' };
            std::array< char, sizeof( T ) * 2 > buf;

            auto cur = buf.rbegin( );
            for ( auto i = 0; i < sizeof( T ); ++i )
            {
                int nibble = value & 0x0f;
                value >>= 4;
                *cur = lookup[ nibble ];
                ++cur;
                nibble = value & 0x0f;
                value >>= 4;
                *cur = lookup[ nibble ];
                ++cur;
            }
            return std::string( buf.data( ), buf.size( ) );
        }

        template <>
        std::string
        to_hex< float >( float value )
        {
            return to_hex( *reinterpret_cast< uint32_t* >( &value ) );
        }

        template <>
        std::string
        to_hex< double >( double value )
        {
            return to_hex( *reinterpret_cast< uint64_t* >( &value ) );
        }
    }

    std::vector< char >
    generate_sc2_channel_stream( int count )
    {
        std::vector< char > results(
            8 + sizeof( nds_impl::nds1::v12_2::raw_sc2_channel ) * count );
        auto dest = results.begin( );

        auto write_uint32 = [&dest]( std::uint32_t val ) -> void {
            std::string tmp = to_hex( val );
            dest = std::copy( tmp.begin( ), tmp.end( ), dest );
        };
        auto write_uint16 = [&dest]( std::uint16_t val ) -> void {
            std::string tmp = to_hex( val );
            dest = std::copy( tmp.begin( ), tmp.end( ), dest );
        };
        auto write_float = [&dest]( float val ) -> void {
            std::string tmp = to_hex( val );
            dest = std::copy( tmp.begin( ), tmp.end( ), dest );
        };

        write_uint32( static_cast< std::uint32_t >( count ) );
        for ( int i = 0; i < count; ++i )
        {
            sc2_channel_info cinfo = get_sc2_channel_info( i );

            std::string tmp = right_pad_string( cinfo.name, ' ', 60 );
            dest = std::copy( tmp.begin( ), tmp.end( ), dest );

            write_uint32( cinfo.rate );
            write_uint32( cinfo.tpnode );
            write_uint16( cinfo.tpnode );
            write_uint16( cinfo.type );
            write_float( cinfo.gain );
            write_float( cinfo.slope );
            write_float( cinfo.offset );

            tmp = right_pad_string( cinfo.units, ' ', 40 );
            dest = std::copy( tmp.begin( ), tmp.end( ), dest );
        }
        return results;
    }

    sc2_channel_info
    get_sc2_channel_info( int chan_num )
    {
        std::array< int, 5 > rates{
            2 * 1024, 4 * 1024, 8 * 1024, 16 * 1024, 32 * 1024
        };
        sc2_channel_info cinfo;
        std::string      name{ "Z1:CHANNEL_" };
        name += std::to_string( chan_num );

        cinfo.name = name;
        cinfo.rate = rates[ chan_num % rates.size( ) ];
        cinfo.tpnum = ( chan_num > 0 && chan_num % 1000 == 0 ? chan_num : 0 );
        cinfo.tpnode = chan_num % 1000;
        cinfo.type = 2;
        cinfo.gain = 1.0;
        cinfo.slope = 1.0;
        cinfo.offset = 0.0;
        cinfo.units = "unknown";
        return cinfo;
    }
}
