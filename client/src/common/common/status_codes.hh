//
// Created by jonathan.hanks on 4/22/17.
//

#ifndef NDS_PROXY_STATUS_CODES_HH_HH
#define NDS_PROXY_STATUS_CODES_HH_HH

#include <array>

namespace nds_impl
{
    namespace common
    {
        typedef std::array< char, 4 > status_code;

        const auto STATUS_DAQD_OK = status_code{ '0', '0', '0', '0' };
        const auto STATUS_DAQD_ERROR = status_code{ '0', '0', '0', '1' };
        const auto STATUS_DAQD_VERSION_MISMATCH =
            status_code{ '0', '0', '0', 'b' };
        const auto STATUS_DAQD_NOT_FOUND = status_code{ '0', '0', '0', 'd' };
        const auto STATUS_DAQD_SYNTAX = status_code{ '0', '0', '1', '9' };
        const auto STATUS_DAQD_ON_TAPE = status_code{ '0', '0', '1', 'a' };
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include <string>
#include "catch.hpp"

TEST_CASE( "Verify status codes exist", "[status_codes]" )
{
    auto status_to_str =
        []( const nds_impl::common::status_code& code ) -> std::string {
        return std::string( code.data( ), code.size( ) );
    };
    using namespace nds_impl::common;
    REQUIRE( status_to_str( STATUS_DAQD_OK ) == "0000" );
    REQUIRE( status_to_str( STATUS_DAQD_ERROR ) == "0001" );
    REQUIRE( status_to_str( STATUS_DAQD_VERSION_MISMATCH ) == "000b" );
    REQUIRE( status_to_str( STATUS_DAQD_SYNTAX ) == "0019" );
    REQUIRE( status_to_str( STATUS_DAQD_ON_TAPE ) == "001a" );
    REQUIRE( status_to_str( STATUS_DAQD_NOT_FOUND ) == "000d" );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS_PROXY_STATUS_CODES_HH_HH
