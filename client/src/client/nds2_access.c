/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#if HAVE_CONFIG_H
#include "daq_config.h"
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#else
#include <io.h>
#endif /* HAVE_UNISTD_H */

/*--------------------------------------------------------------------
 *
 *    Select includes depend on posix versions
 *--------------------------------------------------------------------*/
#if _POSIX_C_SOURCE < 200100L
#if HAVE_SYS_TIME_H
#include <sys/time.h>
#endif /* HAVE_SYS_TIME_H */
#include <sys/types.h>
#else
#include <sys/select.h>
#endif

#include "daqc.h"
#include "daqc_private.h"
#include "daqc_response.h"
#include "daqc_internal.h"
#include "daqc_net.h"
#include "nds_auth.h"
#include "nds_logging.h"
#include "nds_os.h"
#include "nds2.h"
#include "daq_bsd_string.h"

#if _WIN32
#define NFDS( x ) 0
#else
#define NFDS( x ) ( x + 1 )
#endif

static int get_version_and_revision( daq_t* daq );

/** Static internal function to implement nds2_recv_channel_list() and
  * nds2_recv_channels_by_pattern().
  */
static int nds2_recv_channels_impl( daq_t*         daq,
                                    daq_channel_t* channel,
                                    int            num_channels,
                                    int*           num_channels_received,
                                    time_t         gps,
                                    enum chantype  type,
                                    const char*    pat,
                                    const char*    METHOD );

/** Static internal function to implement nds2_recv_channel_hash() and
  * nds2_recv_hash_by_pattern().
  */
static int nds2_recv_hash_impl( daq_t*        daq,
                                void*         hash,
                                int*          hash_len,
                                time_t        gps,
                                enum chantype type,
                                const char*   pat,
                                const char*   METHOD );

#define NDS2_VERSION_1_REVISION 0

/*
 *  Connect to the DAQD server on the host identified by `ip' address.
 *  Returns zero if OK or the error code if failed.
 */
int
nds2_connect( daq_t* daq, const char* host, int port )
{
    static const char* METHOD = "nds2_connect";
    int                resp = DAQD_OK;
    int                connect_rc = 0;

    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: ENTRY\n", METHOD );
    }
    /*  Get a socket.
     */
    if ( ( resp = daq_private_srvr_open( daq->conceal ) ) != DAQD_OK )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
        {
            nds_logging_printf( "INFO: %s: EXIT: %d\n", METHOD, resp );
        }
        return resp;
    }

    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }
    /*----------------------------------  Get the server address.     */
    resp = daq_set_server( daq, host, port );
    if ( resp != DAQD_OK )
    {
        nds_logging_print_errno( "Error in daq_set_server" );
        daq_private_srvr_close( daq->conceal );
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
        {
            nds_logging_printf( "INFO: %s: EXIT: %d\n", METHOD, resp );
        }
        return resp;
    }

    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }
    /*-----------------------------------  Connect to the server      */
    daq_private_srvr_nonblocking( daq->conceal, 1 );
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( "Connecting NDS2 .." );
        nds_logging_flush( );
    }
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }
    connect_rc = daq_private_srvr_connect( daq->conceal );
    if ( connect_rc == NDS_SOCKET_TRANSIENT_FAILURE )
    {
        struct timeval timeout = { 4, 0 };
        fd_set         writefds;
        int            ret;
        FD_ZERO( &writefds );
        FD_SET( daq->conceal->sockfd, &writefds );
        ret = select(
            NFDS( daq->conceal->sockfd ), NULL, &writefds, NULL, &timeout );
        if ( ret == 1 )
            connect_rc = NDS_SOCKET_OK;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }
    if ( connect_rc != NDS_SOCKET_OK )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            char pmsg[ 256 ];

            nds_logging_printf( " failed\n" );
            strerror_r( errno, pmsg, sizeof( pmsg ) );
            nds_logging_printf( "connect( ): errno: %d - %s\n", errno, pmsg );
        }
        daq_private_srvr_close( daq->conceal );
        daq->conceal->sockfd = -1;
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
        {
            nds_logging_printf( "INFO: %s: EXIT: %d\n", METHOD, DAQD_CONNECT );
        }
        return DAQD_CONNECT;
    }

    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( " authenticate" );
    }
    daq_private_srvr_nonblocking( daq->conceal, 0 );
    resp = daq_send( daq, "authorize" );
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }
    if ( resp == DAQD_SASL )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( " ..." );
        }
        resp = nds_authenticate( daq, host );
        if ( resp )
            resp = DAQD_SASL;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
    {
        nds_logging_printf(
            "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
    }

    if ( resp == DAQD_OK )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
        {
            nds_logging_printf(
                "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
        }
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( " done\n" );
        }
        daq->conceal->datafd = daq->conceal->sockfd;
        resp = get_version_and_revision( daq );
    }
    else
    {
        if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 20 ) )
        {
            nds_logging_printf(
                "INFO: %s: %6d - %s\n", METHOD, __LINE__, __FILE__ );
        }
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( " failed: %s\n", daq_strerror( resp ) );
        }
        daq_private_srvr_close( daq->conceal );
    }
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: EXIT: %d\n", METHOD, resp );
    }
    return resp;
}

/*
 *  Disconnect from the server and close the socket file descriptor
 */
int
nds2_disconnect( daq_t* daq )
{
#if _WIN32
    typedef int write_len_t;
#else /* _WIN32 */
    typedef size_t write_len_t;
#endif /* _WIN32 */

    /*  Send a quit command */
    static const char* METHOD = "nds2_disconnect";
    char*              command = "quit;\n";
    size_t             lcmd = strlen( command );

    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: ENTRY\n", METHOD );
    }
    if ( write( daq->conceal->sockfd, command, ( write_len_t )( lcmd ) ) !=
         lcmd )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "daq_disconnect: write errno=%d\n", METHOD, errno );
        }
        return DAQD_WRITE;
    }
    usleep( 100000 );
    nds_auth_disconnect( daq );
    if ( nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 10 ) )
    {
        nds_logging_printf( "INFO: %s: EXIT: %d\n", METHOD, 0 );
    }
    return 0;
}

/*
 *  Get the most recent error message recorded by the server.
 *  Returns zero if OK or the error code if failed.
 */
int
nds2_get_last_message( daq_t* daq, char* message, size_t maxlen, int* str_len )
{
    const char*     METHOD = "nds2_get_last_message";
    const char*     cmd = "get-last-message;";
    int             resp, tmp_len;
    nds_socket_type fd;

    if ( str_len )
        *str_len = 0;
    if ( maxlen == 0 )
        return DAQD_ERROR;

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: Requesting last message\n", METHOD );
    }

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: buf - %s\n", METHOD, cmd );
    }
    resp = daq_send( daq, cmd );
    if ( resp )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Error return from daq_send(): %s\n",
                                METHOD,
                                daq_strerror( resp ) );
        }
        return resp;
    }

    /*----------------------------------  Read the source string      */
    fd = daq->conceal->sockfd;
    tmp_len = _daq_read_string( fd, maxlen, message );
    if ( tmp_len < 0 )
        return DAQD_ERROR;
    *str_len = tmp_len;
    return resp;
}

#define BUF_LENGTH 512UL
#define MAX_TYPESTR_LENGTH 16

/*
 *  Receive channel hash using the NDS2 protocol
 */
int
nds2_recv_channel_hash(
    daq_t* daq, void* hash, int* length, time_t gps, enum chantype type )
{
    const char* METHOD = "nds2_recv_channel_hash";
    return nds2_recv_hash_impl( daq, hash, length, gps, type, 0, METHOD );
}
/*
 *  Receive channel hash using the NDS2 protocol
 */
int
nds2_recv_hash_by_pattern( daq_t*        daq,
                           void*         hash,
                           int*          length,
                           time_t        gps,
                           enum chantype type,
                           const char*   pat )
{
    const char* METHOD = "nds2_recv_hash_by_pattern";
    return nds2_recv_hash_impl( daq, hash, length, gps, type, pat, METHOD );
}
/*
 *  Receive channel hash using the NDS2 protocol
 */
static int
nds2_recv_hash_impl( daq_t*        daq,
                     void*         hash,
                     int*          length,
                     time_t        gps,
                     enum chantype type,
                     const char*   pat,
                     const char*   METHOD )
{
    const char* hash_cmd_template = "get-channel-crc XgpsgpsgpsX XunknownX {};";
    char*       buf;
    size_t      buf_len, pat_len;
    int         resp;
    nds_socket_type fd;

    if ( *length < sizeof( uint4_type ) )
        return DAQD_ERROR;

    /*----------------------------------  Get the pattern length */
    if ( !pat )
        pat_len = 0;
    else
        pat_len = strlen( pat );
    buf_len = strlen( hash_cmd_template ) + pat_len + 2;
    buf = malloc( buf_len );

#if HAVE_SPRINTF_S
    if ( !pat_len )
    {
        sprintf_s( buf,
                   buf_len,
                   "get-channel-crc %i %s;",
                   (int)gps,
                   cvt_chantype_str( type ) );
    }
    else
    {
        sprintf_s( buf,
                   buf_len,
                   "get-channel-crc %i %s {%s};",
                   (int)gps,
                   cvt_chantype_str( type ),
                   pat );
    }
#else /* HAVE_SPRINTF_S */
    if ( !pat_len )
    {
        sprintf(
            buf, "get-channel-crc %i %s;", (int)gps, cvt_chantype_str( type ) );
    }
    else
    {
        sprintf( buf,
                 "get-channel-crc %i %s {%s};",
                 (int)gps,
                 cvt_chantype_str( type ),
                 pat );
    }
#endif /* HAVE_SPRINTF_S */
    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: Requesting channel hash\n", METHOD );
    }

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: buf - %s\n", METHOD, buf );
    }
    resp = daq_send( daq, buf );
    free( buf );
    buf = NULL;
    if ( resp )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Error return from daq_send(): %s\n",
                                METHOD,
                                daq_strerror( resp ) );
        }
        return resp;
    }

    /*----------------------------------  Get the channel hash      */
    fd = daq->conceal->sockfd;
    if ( read_uint4( fd, (uint4_type*)hash ) )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( "Couldn't determine the channel hash\n" );
        }
        resp = DAQD_ERROR;
    }
    else
    {
        *length = sizeof( uint4_type );
    }
    return resp;
}

/*
 *  Receive channel data using the NDS2 protocol
 */
int
nds2_recv_channel_list( daq_t*         daq,
                        daq_channel_t* channel,
                        int            num_channels,
                        int*           num_channels_received,
                        time_t         gps,
                        enum chantype  type )
{
    const char* METHOD = "nds2_recv_channel_list";
    return nds2_recv_channels_impl( daq,
                                    channel,
                                    num_channels,
                                    num_channels_received,
                                    gps,
                                    type,
                                    "",
                                    METHOD );
}

/*
 *  Receive channel data using the NDS2 protocol
 */
int
nds2_recv_channels_by_pattern( daq_t*         daq,
                               daq_channel_t* channel,
                               int            num_channels,
                               int*           num_channels_received,
                               time_t         gps,
                               enum chantype  type,
                               const char*    pat )
{
    const char* METHOD = "nds2_recv_channels_by pattern";
    return nds2_recv_channels_impl( daq,
                                    channel,
                                    num_channels,
                                    num_channels_received,
                                    gps,
                                    type,
                                    pat,
                                    METHOD );
}

/*
 *  Receive channel data using the NDS2 protocol
 */
static int
nds2_recv_channels_impl( daq_t*         daq,
                         daq_channel_t* channel,
                         int            num_channels,
                         int*           num_channels_received,
                         time_t         gps,
                         enum chantype  type,
                         const char*    pat,
                         const char*    METHOD )
{
    const char* recv_cmd_template = "count-channels XgpsgpsgpsX XunknownX {};";

    char*           buf = NULL;
    size_t          buf_len, pat_len;
    char            typestr[ MAX_TYPESTR_LENGTH ];
    int             resp, i;
    int4_type       channels;
    nds_socket_type fd;

    /*----------------------------------  Get the pattern length */
    if ( !pat )
        pat_len = 0;
    else
        pat_len = strlen( pat );
    buf_len = strlen( recv_cmd_template ) + pat_len + 2;
    buf = malloc( buf_len );

    /*----------------------------------  Issue the request               */
    if ( num_channels != 0 )
    {
#if HAVE_SPRINTF_S
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 20 ) )
        {
            nds_logging_printf( "ERROR: %s: sizeof( buf ):%d %s - %d\n",
                                METHOD,
                                buf_len,
                                __FILE__,
                                __LINE__ );
        }
        if ( !pat_len )
        {
            sprintf_s( buf,
                       buf_len,
                       "get-channels %i %s;",
                       (int)gps,
                       cvt_chantype_str( type ) );
        }
        else
        {
            sprintf_s( buf,
                       buf_len,
                       "get-channels %i %s {%s};",
                       (int)gps,
                       cvt_chantype_str( type ),
                       pat );
        }
#else /* HAVE_SPRINTF_S */
        if ( !pat_len )
        {
            sprintf( buf,
                     "get-channels %i %s;",
                     (int)gps,
                     cvt_chantype_str( type ) );
        }
        else
        {
            sprintf( buf,
                     "get-channels %i %s {%s};",
                     (int)gps,
                     cvt_chantype_str( type ),
                     pat );
        }
#endif /* HAVE_SPRINTF_S */
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
        {
            nds_logging_printf( "INFO: Requesting channel list\n" );
        }
    }
    else
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
        {
            nds_logging_printf( "INFO: %s: sizeof( buf ):%d %s - %d\n",
                                METHOD,
                                buf_len,
                                __FILE__,
                                __LINE__ );
        }
#if HAVE_SPRINTF_S
        if ( !pat_len )
        {
            sprintf_s( buf,
                       buf_len,
                       "count-channels %i %s;",
                       (int)gps,
                       cvt_chantype_str( type ) );
        }
        else
        {
            sprintf_s( buf,
                       buf_len,
                       "count-channels %i %s {%s};",
                       (int)gps,
                       cvt_chantype_str( type ),
                       pat );
        }
#else /* HAVE_SPRINTF_S */
        if ( !pat_len )
        {
            sprintf( buf,
                     "count-channels %i %s;",
                     (int)gps,
                     cvt_chantype_str( type ) );
        }
        else
        {
            sprintf( buf,
                     "count-channels %i %s {%s};",
                     (int)gps,
                     cvt_chantype_str( type ),
                     pat );
        }
#endif /* HAVE_SPRINTF_S */
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
        {
            nds_logging_printf( "INFO: %s: Requesting channel count\n",
                                METHOD );
        }
    }
    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: buf - %s\n", METHOD, buf );
    }
    resp = daq_send( daq, buf );
    free( buf );
    buf = NULL;
    if ( resp )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Error return from daq_send(): %s\n",
                                METHOD,
                                daq_strerror( resp ) );
        }
        return resp;
    }

    /*----------------------------------  Get the number of channels      */
    fd = daq->conceal->sockfd;
    if ( read_uint4( fd, (uint4_type*)&channels ) || channels < 0 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "Couldn't determine the number of data channels\n" );
        }
        return DAQD_ERROR;
    }
    *num_channels_received = channels;
    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf(
            "INFO: %s: channels: %d num_channels_received: %d\n",
            METHOD,
            channels,
            *num_channels_received );
    }
    if ( num_channels == 0 )
        return DAQD_OK;

    /*----------------------------------  Loop over channels              */
    buf = malloc( BUF_LENGTH );
    for ( i = 0; i < channels; i++ )
    {
        enum chantype ctype;
        double        rate;
        int           len = _daq_read_string( fd, (size_t)BUF_LENGTH, buf );
        daq_data_t    dtype;

        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "INFO: %s: i: %d len: %d\n", METHOD, i, len );
        }
        if ( len <= 0 )
        {
            free( buf );
            return DAQD_ERROR;
        }
        if ( i < num_channels )
        {
            const char* p = buf;
            const char* end = buf + len;
            char        name[ MAX_CHANNEL_NAME_LENGTH ];
            _daq_get_string( &p, end, name, sizeof( name ) );
            _daq_get_string( &p, end, typestr, sizeof( typestr ) );
            ctype = cvt_str_chantype( typestr );
            rate = strtod( p, (char**)&p );
            _daq_get_string( &p, end, typestr, sizeof( typestr ) );
            dtype = data_type_code( typestr );
            daq_init_channel( channel + i, name, ctype, rate, dtype );
        }
    }
    free( buf );
    buf = NULL;
    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "DEBUG: Channel List worked OK\n" );
    }
    return DAQD_OK;
}

/*
 *  Receive epoch data using the NDS2 protocol
 */
int
nds2_recv_epoch_list( daq_t* daq, char* epochs, size_t max_len, int* str_len )
{
    const char*     METHOD = "nds2_recv_epoch_list";
    const char*     cmd = "list-epochs;";
    int             resp, tmp_len;
    nds_socket_type fd;

    if ( str_len )
        *str_len = 0;
    if ( max_len == 0 )
        return DAQD_ERROR;

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: Requesting epoch list\n", METHOD );
    }

    if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 10 ) )
    {
        nds_logging_printf( "INFO: %s: buf - %s\n", METHOD, cmd );
    }
    resp = daq_send( daq, cmd );
    if ( resp )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Error return from daq_send(): %s\n",
                                METHOD,
                                daq_strerror( resp ) );
        }
        return resp;
    }

    /*----------------------------------  Read the source string      */
    fd = daq->conceal->sockfd;
    tmp_len = _daq_read_string( fd, max_len, epochs );
    if ( tmp_len < 0 )
        return DAQD_ERROR;
    *str_len = tmp_len;
    return resp;
}

/*!
 * @brief Print the name string
 * @param daq The daq structure
 * @param dst Destination pointer
 * @param size The max size of dst, including the NULL terminator
 * @returns the number of bytes required to hold this information
 * @note if dst is NULL or size == 0 no output is done (ie it only
 * counts the number of bytes it would have taken to write the data).
 */
static size_t
_nds2_get_name_list( daq_t* daq, char* dst, size_t size )
{
    chan_req_t* req;

    size_t i = 0;
    size_t j = 0;
    size_t count = 0;
    size_t N = daq->num_chan_request;

    _daq_strlcat( dst, "{", size );
    ++i;
    req = daq->chan_req_list;
    for ( j = 0; j < N; j++ )
    {
        chantype_t  ctype;
        const char* ctype_str = NULL;

        _daq_slprintf( dst, size, "%s%s", ( j ? " " : "" ), req->name );
        i += snprintf( NULL, 0, "%s%s", ( j ? " " : "" ), req->name );

        ctype = req->type;
        if ( ctype != cUnknown )
        {
            ctype_str = cvt_chantype_str( ctype );
            _daq_slprintf( dst, size, ",%s", ctype_str );
            i += strlen( ctype_str ) + 1;
        }
        req++;
    }
    _daq_strlcat( dst, "}", size );
    ++i;
    return i;
}

/*
 *  Receive source list using the NDS2 protocol
 */
int
nds2_recv_source_data(
    daq_t* daq, char* list, size_t max_len, time_t gps, long* str_len )
{
    static const char* METHOD = "nds2_recv_source_data";
    int                rc = 0;
    size_t             txt_len;
    char *             cmd, *cmd_txt;

    *str_len = 0;

    /*----------------------------------  Build up a command
     */
    cmd = "get-source-data";

    /*----------------------------------  Get the text length, allocate a buffer
     */
    txt_len = strlen( cmd ) + strlen( "  {};" ) +
        _nds2_get_name_list( daq, NULL, 0 ) +
        +snprintf( NULL, 0, "%llu", (unsigned long long)gps ) +
        daq->num_chan_request;
    cmd_txt = (char*)malloc( txt_len );
    cmd_txt[ 0 ] = 0;

    /*----------------------------------  Build the command
     */
    _daq_slprintf( cmd_txt, txt_len, "%s %llu ", cmd, gps );
    _nds2_get_name_list( daq, cmd_txt, txt_len );
    if ( _daq_strlcat( cmd_txt, ";", txt_len ) >= txt_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: Internal error, command string truncated\n",
                METHOD );
        }
        rc = DAQD_ERROR;
        free( cmd_txt );
        return rc;
    }

    /*----------------------------------  Send the command free the command
     */
    rc = daq_send( daq, cmd_txt );
    free( cmd_txt );
    cmd_txt = 0;

    if ( rc )
    {
        nds_logging_printf( "Error in daq_send: %s\n", daq_strerror( rc ) );
        return rc;
    }

    /*----------------------------------  Read the source string      */
    *str_len = _daq_read_string( daq->conceal->sockfd, max_len, list );
    if ( *str_len < 0 )
        return DAQD_ERROR;
    return rc;
}

/*
 *  Receive source list using the NDS2 protocol
 */
int
nds2_recv_source_list(
    daq_t* daq, char* list, size_t max_len, time_t gps, long* str_len )
{
    static const char* METHOD = "nds2_recv_source_list";
    int                rc = 0;
    size_t             txt_len;
    char *             cmd, *cmd_txt;

    *str_len = 0;

    /*----------------------------------  Build up a command
     */
    cmd = "get-source-list";

    /*----------------------------------  Get the text length, allocate a buffer
     */
    txt_len = strlen( cmd ) + strlen( "  {};" ) +
        snprintf( NULL, 0, "%llu", (unsigned long long)gps ) +
        _nds2_get_name_list( daq, NULL, 0 ) + daq->num_chan_request;
    cmd_txt = (char*)malloc( txt_len );
    cmd_txt[ 0 ] = 0;

    /*----------------------------------  Build the command
     */

    _daq_slprintf( cmd_txt, txt_len, "%s %llu ", cmd, (unsigned long long)gps );
    _nds2_get_name_list( daq, cmd_txt, txt_len );
    if ( _daq_strlcat( cmd_txt, ";", txt_len ) >= txt_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: Internal error, command string truncated\n",
                METHOD );
        }
        rc = DAQD_ERROR;
        free( cmd_txt );
        return rc;
    }

    /*----------------------------------  Send the command free the command
     */
    rc = daq_send( daq, cmd_txt );
    free( cmd_txt );
    cmd_txt = 0;

    if ( rc )
    {
        nds_logging_printf( "Error in daq_send: %s\n", daq_strerror( rc ) );
        return rc;
    }

    /*----------------------------------  Read the source string      */
    *str_len = _daq_read_string( daq->conceal->sockfd, max_len, list );
    if ( *str_len < 0 )
        return DAQD_ERROR;
    return rc;
}

/*  nds2_request_data(daq_t*, time_t, time_t)
 *
 *  Get requested channel data for the specified interval.
 */
int
nds2_request_check( daq_t* daq, time_t start, time_t end )
{
    static const char* METHOD = "nds2_request_check";
    int                rc = DAQD_OK;
    size_t             N, txt_len;
    char *             cmd, *cmd_txt;

    /*  Get the request type and sum channel name length
     */
    N = daq->num_chan_request;
    if ( N == 0 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: No channels requested\n", METHOD );
        }
        return DAQD_ERROR;
    }

    /*----------------------------------  Build up a command
     */
    cmd = "check-data";

    /*----------------------------------  Get the text length, allocate a buffer
     */
    txt_len = strlen( cmd ) + strlen( "    {};" ) +
        snprintf( NULL,
                  0,
                  "%llu%llu",
                  (unsigned long long)start,
                  (unsigned long long)end ) +
        _nds2_get_name_list( daq, NULL, 0 ) + +N;
    cmd_txt = (char*)malloc( txt_len );
    cmd_txt[ 0 ] = 0;

    /*----------------------------------  Build the command
     */
    _daq_slprintf( cmd_txt,
                   txt_len,
                   "%s %llu %llu ",
                   cmd,
                   (unsigned long long)start,
                   (unsigned long long)end );
    _nds2_get_name_list( daq, cmd_txt, txt_len );
    if ( _daq_strlcat( cmd_txt, ";", txt_len ) >= txt_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: Internal error, command string truncated\n",
                METHOD );
        }
        rc = DAQD_ERROR;
        free( cmd_txt );
        goto cleanup;
    }

    /*----------------------------------  Send the command free the command
     */
    rc = daq_send( daq, cmd_txt );
cleanup:
    free( cmd_txt );
    cmd_txt = 0;
    return rc;
}

/*  nds2_request_data(daq_t*, time_t, time_t)
 *
 *  Get requested channel data for the specified interval.
 */
int
nds2_request_data( daq_t* daq, time_t start, time_t end, time_t dt )
{
    static const char* METHOD = "nds2_request_data";
    int                wid, bl, rc = DAQD_OK;
    size_t             N, i, txt_len;
    char *             cmd, *cmd_txt;
    chan_req_t*        req;

    /*  Check the dt number (j.i.c)
     */
    if ( dt > 2000000000 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: Data stride too long\n", METHOD );
        }
        return DAQD_ERROR;
    }

    /*  Get the request type and sum channel name length
     */
    N = daq->num_chan_request;
    if ( N == 0 )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf( "ERROR: %s: No channels requested\n", METHOD );
        }
        return DAQD_ERROR;
    }

    /*----------------------------------  Build up a command
     */
    cmd = "get-data";

    /*----------------------------------  Get the text length, allocate a buffer
     */
    txt_len = strlen( cmd ) + strlen( "     {};" ) +
        snprintf( NULL,
                  0,
                  "%llu%llu%llu",
                  (unsigned long long)start,
                  (unsigned long long)end,
                  (unsigned long long)dt ) +
        _nds2_get_name_list( daq, NULL, 0 ) + N;
    cmd_txt = (char*)malloc( txt_len );
    cmd_txt[ 0 ] = 0;

    /*----------------------------------  Build the command
     */
    _daq_slprintf( cmd_txt,
                   txt_len,
                   "%s %llu %llu %llu ",
                   cmd,
                   (unsigned long long)start,
                   (unsigned long long)end,
                   (unsigned long long)dt );
    _nds2_get_name_list( daq, cmd_txt, txt_len );
    if ( _daq_strlcat( cmd_txt, ";", txt_len ) >= txt_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 0 ) )
        {
            nds_logging_printf(
                "ERROR: %s: Internal error, command string truncated\n",
                METHOD );
        }
        rc = DAQD_ERROR;
        free( cmd_txt );
        return rc;
    }

    /*----------------------------------  Reset status words before request
     */
    req = daq->chan_req_list;
    for ( i = 0; i < N; ++i )
    {
        req[ i ].status = 0;
    }

    /*----------------------------------  Send the command free the command
     */
    rc = daq_send( daq, cmd_txt );
    free( cmd_txt );
    cmd_txt = 0;

    if ( rc )
    {
        nds_logging_printf( "Error in daq_send: %s\n", daq_strerror( rc ) );
        return rc;
    }

    /*------------------------------  Get the writer ID
     */
    wid = (int)( daq_recv_id( daq ) );
    if ( wid < 0 )
    {
        nds_logging_printf( "Error reading writerID in daq_recv_block\n" );
        return DAQD_ERROR;
    }

    /*------------------------------  Get the offline flag
     */
    bl = daq_recv_block_num( daq );
    if ( bl < 0 )
    {
        nds_logging_printf( "Error reading offline flag in daq_recv_block\n" );
        rc = DAQD_ERROR;
    }
    return rc;
}

struct nds2_chan_status
{
    int   status;
    int   offset;
    int   data_type;
    float rate;
    float signal_offset;
    float signal_slope;
};

/*  nds2_receive_reconfigure(daq_t* daq, long block_len)
 *
 *  Receive a reconfigure block. receive_reconfigure is received after the
 *  block header has been read in. The block length does not include the
 *  header length.
 *
 *  Valid return codes are:
 *  -1  Error Attempting to read a reconfigure block.
 *  -2  Successfully read reconfigure block.
 */
int
nds2_receive_reconfigure( daq_t* daq, size_t block_len )
{
    nds_socket_type fd;
    size_t          nchannels, i;
    size_t          status_len, byte_count;
    chan_req_t*     list;
    status_len = sizeof( struct nds2_chan_status );
    nchannels = daq->num_chan_request;
    if ( daq->nds1_rev <= 5 )
    {
        if ( nchannels * status_len != block_len )
        {
            if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
            {
                nds_logging_printf( "Channel reconfigure block length has "
                                    "bad length (%" PRISIZE_T ")\n",
                                    block_len );
            }
            return -1;
        }
    }

    fd = daq->conceal->datafd;
    list = daq->chan_req_list;
    byte_count = 0;
    for ( i = 0; i < nchannels && byte_count < block_len; i++ )
    {
        float      rate;
        uint4_type dtype;
        if ( read_uint4( fd, (uint4_type*)&list[ i ].status ) )
            return -1;
        if ( read_uint4( fd, &list[ i ].offset ) )
            return -1;
        if ( read_uint4( fd, &dtype ) )
            return -1;
        if ( list[ i ].type == cUnknown )
            list[ i ].type = dtype >> 16;
        list[ i ].data_type = ( dtype & 0xffff );
        if ( read_float( fd, &rate ) )
            return -1;
        list[ i ].rate = rate;
        if ( read_float( fd, &list[ i ].s.signal_offset ) )
            return -1;
        if ( read_float( fd, &list[ i ].s.signal_slope ) )
            return -1;
        byte_count += status_len;
        if ( daq->nds1_rev > 5 )
        {
            int str_len;
            str_len = _daq_read_string(
                fd, MAX_SIGNAL_UNIT_LENGTH, list[ i ].s.signal_units );
            if ( str_len < 0 )
                return -1;
            byte_count += sizeof( uint4_type ) + (unsigned)str_len;
        }
    }
    if ( byte_count != block_len )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf( "Channel reconfigure block length has "
                                "bad length (%" PRISIZE_T
                                ") expected (%" PRISIZE_T "\n",
                                byte_count,
                                block_len );
        }
        return -1;
    }
    return -2;
}

/*
 *  Set the default epoch for this session.
 */
int
nds2_set_epoch( daq_t* daq, const char* epoch )
{
    int   rc = 0;
    char* cmd = (char*)malloc( 12 + strlen( epoch ) );
    strcpy( cmd, "set-epoch " );
    strcat( cmd, epoch );
    strcat( cmd, ";" );
    rc = daq_send( daq, cmd );
    free( cmd );
    return rc;
}

/*
 *  User authentication
 */
int
nds2_startup( void )
{
    return nds_auth_startup( );
}

static int
get_version_and_revision( daq_t* daq )
{
    int        retval;
    char       cmd_buf[ 32 ];
    uint4_type v = UNSPECIFIED_NDS2_PROTOCOL_VERSION;
    uint4_type rv = UNSPECIFIED_NDS2_PROTOCOL_REVISION;
    uint4_type exp_rv = UNSPECIFIED_NDS2_PROTOCOL_REVISION;
    uint4_type max_rv = MAX_NDS2_PROTOCOL_REVISION;

    /*-------------------------------------------------------------------
     * Obtain the server protocol version
     *-------------------------------------------------------------------*/
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( " GET NDS version\n" );
    }
    switch ( ( retval = daq_send( daq, "server-protocol-version;" ) ) )
    {
    case DAQD_OK:
        /*---------------------------------------------------------------
         * Nothing special to do when the command succeeds.
         *---------------------------------------------------------------*/
        break;
    case DAQD_COMMAND_SYNTAX:
        /*---------------------------------------------------------------
         * This is the case when the server doesn't support versioning
         *   queries of the NDS 2 protocol
         *---------------------------------------------------------------*/
        v = UNSPECIFIED_NDS2_PROTOCOL_VERSION;
        retval = DAQD_OK;
        goto error_recovery;
        break;
    default:
        goto error_recovery;
        break;
    }

    /* Read server response */
    retval = read_uint4( daq->conceal->sockfd, &v );
    if ( retval != DAQD_OK )
    {
        goto error_recovery;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
    {
        nds_logging_printf( "NDS version of server is %d\n", v );
    }
    if ( ( v > MAX_NDS2_PROTOCOL_VERSION ) ||
         ( v < MIN_NDS2_PROTOCOL_VERSION ) )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "unsupported communication protocol version: " );
            nds_logging_printf( "received %d, only support %d to %d\n",
                                v,
                                MIN_NDS2_PROTOCOL_VERSION,
                                MAX_NDS2_PROTOCOL_VERSION );
            nds_logging_printf( " version %d not between %d to %d\n",
                                v,
                                MIN_NDS2_PROTOCOL_VERSION,
                                MAX_NDS2_PROTOCOL_VERSION );
        }
        retval = DAQD_VERSION_MISMATCH;
        goto error_recovery;
    }
    daq->nds1_ver = v; /* server protocol version */
    /*-------------------------------------------------------------------
     * Obtain the server protocol revision (supported in revision 1.6+)
     *-------------------------------------------------------------------*/
    sprintf(
        cmd_buf, "server-protocol-revision %d;", MAX_NDS2_PROTOCOL_REVISION );
    retval = daq_send( daq, cmd_buf );
    /*-------------------------------------------------------------------
     * Handle negotiation with an older server cleanly
     *-------------------------------------------------------------------*/
    if ( retval == DAQD_COMMAND_SYNTAX )
    {
        sprintf( cmd_buf, "server-protocol-revision;" );
        retval = daq_send( daq, cmd_buf );
    }
    switch ( retval )
    {
    case DAQD_OK:
        /*---------------------------------------------------------------
         * Nothing special to do when the command succeeds.
         *---------------------------------------------------------------*/
        break;
    case DAQD_COMMAND_SYNTAX:
        /*---------------------------------------------------------------
         * This is the case when the server doesn't support versioning
         *   queries of the NDS 2 protocol
         *---------------------------------------------------------------*/
        rv = UNSPECIFIED_NDS2_PROTOCOL_REVISION;
        retval = DAQD_OK;
        goto error_recovery;
        break;
    default:
        goto error_recovery;
        break;
    }

    /* Read server response */
    retval = read_uint4( daq->conceal->sockfd, &rv );
    if ( retval != DAQD_OK )
    {
        goto error_recovery;
    }
    /* Setup what to expect */
    if ( v == UNSPECIFIED_NDS2_PROTOCOL_VERSION )
    {
        exp_rv = UNSPECIFIED_NDS2_PROTOCOL_REVISION;
    }
    else if ( v <= 1 )
    {
        exp_rv = NDS2_VERSION_1_REVISION;
    }
    /* Determine if the expected revision was received */
    if ( rv < exp_rv || rv > max_rv )
    {
        if ( nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 0 ) )
        {
            nds_logging_printf(
                "Warning: communication protocol revision mismatch: " );
            nds_logging_printf(
                "expected %d.%d, received %d.%d\n", v, exp_rv, v, rv );
        }
    }
    daq->nds1_rev = rv;
    return retval;

error_recovery:
    /*
     * When recovering from an error condition,
     *   check to see if the severity warrents
     *   closing the connection to the server.
     */
    if ( retval == DAQD_OK )
    {
        daq->nds1_ver = v; /* server protocol version */
        daq->nds1_rev = rv; /* server protocol revision */
    }
    else
    {
        daq_private_srvr_close( daq->conceal );
    }
    return retval;
}
