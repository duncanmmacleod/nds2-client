/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#include "daq_config.h"

#if HAVE_ERRNO_H
#include <errno.h>
#endif /* HAVE_ERRNO_H */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !_WIN32
#include <unistd.h>
#endif /* ! _WIN32 */

#include "nds_logging.h"

#define TEST_PASS 0
#define TEST_FAIL 1

#if _WIN32
#define popen( a, b ) _popen( a, b )
#define pclose( a ) _pclose( a )
#define putenv( a ) _putenv( a )
#define unlink( a ) _unlink( a )
#endif /* _WIN32 */

static const char* envvariable = "NDS_LOGGING";

int
main( int ArgC, char** ArgV )
{
    int retval = TEST_FAIL;

    if ( strcmp( "--parse", ArgV[ 1 ] ) == 0 )
    {
        /*
         * Based on ticket #56
         */
        char buffer_values[ 2096 ] = "CONNECTION=30"
                                     ":VERBOSE_ERRORS=30"
                                     ":TRACE_ENTRY_EXIT=30"
                                     ":STATUS_UPDATE=30"
                                     ":USER=30";

        static char buffer[ 4096 ];

        sprintf( buffer, "%s=%s", envvariable, buffer_values );
        putenv( buffer );

        nds_logging_init( );

        if ( !( nds_logging_check( NDS_LOG_GROUP_CONNECTION, 30 ) &&
                nds_logging_check( NDS_LOG_GROUP_VERBOSE_ERRORS, 30 ) &&
                nds_logging_check( NDS_LOG_GROUP_TRACE_ENTRY_EXIT, 30 ) &&
                nds_logging_check( NDS_LOG_GROUP_STATUS_UPDATE, 30 ) &&
                nds_logging_check( NDS_LOG_GROUP_USER, 30 ) ) )
        {
            goto test_exit;
        }

        retval = TEST_PASS;
    }
    else if ( strcmp( "--quiet-init", ArgV[ 1 ] ) == 0 )
    {
        /*
         * Based on ticket #53
         */
        char   output[ 1024 ];
        char   cmd[ 2048 ];
        FILE*  fcmd = (FILE*)NULL;
        size_t output_len = 0;

        sprintf( cmd, "%s --quiet-init-child", ArgV[ 0 ] );
        fcmd = popen( cmd, "r" );
        while ( fgets( output, (int)sizeof( output ), fcmd ) )
        {
            output_len += strlen( output );
        }
        if ( pclose( fcmd ) != 0 )
        {
            goto test_exit;
        }
        if ( output_len > 0 )
        {
            goto test_exit;
        }

        retval = TEST_PASS;
    }
    else if ( strcmp( "--quiet-init-child", ArgV[ 1 ] ) == 0 )
    {
        static const char* filename = "quiet_init_child";
        FILE*              fstderr;
        size_t             bytes = 0;
        char               buffer[ 1024 ];

        unlink( filename );
        fstderr = freopen( filename, "w", stderr );
        nds_logging_init( );
        fclose( fstderr );

        fstderr = fopen( filename, "r" );
        bytes = fread( buffer, (size_t)1, sizeof( buffer ), fstderr );
        fclose( fstderr );

        if ( bytes )
        {
            fwrite( buffer, (size_t)1, bytes, stdout );
            fflush( stdout );
        }

        unlink( filename );

        retval = TEST_PASS;
    }

test_exit:
    return retval;
}
