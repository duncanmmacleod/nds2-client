/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#ifndef DAQC_NDS1_H
#define DAQC_NDS1_H

#ifndef DLL_EXPORT
#if WIN32 || WIN64
#define DLL_EXPORT __declspec( dllexport )
#else
#define DLL_EXPORT
#endif /* WIN32 || WIN64 */
#endif /* DLL_EXPORT */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Cycle the connection to the nds1 server.  This will reuse the
 * host and port information.  This only works after a successful
 * connection.  It is a work around for ticket 282 where the nds1
 * server sends DAQD_ERROR after some number of requests.
 *
 * This function is used to help transparently cycle the connection
 * and reset whatever is causing nds1 issues.
 */
DLL_EXPORT int nds1_reconnect( daq_t* daq );

/*
 *  Connect to the DAQD server on the host identified by `ip' address.
 *  Returns zero if OK or the error code if failed.
 */
int nds1_connect( daq_t* daq, const char* host, int port );

/*
 *  Disconnect from the DAQD server. Send a quit command and close the
 *  socket.
 *  Returns zero if OK or the error code if failed.
 */
int nds1_disconnect( daq_t* daq );

/*
 *  Get the most recent error message recorded by the server.
 *  Returns zero if OK or the error code if failed.
 */
int nds1_get_last_message( daq_t* daq, char* buf, size_t max_len, int* len );

/*
 *  Initialize the nds1 client
 *  Returns zero if OK or the error code if failed.
 */
int nds1_initalize( void );

/*
 *  Receive channel data using the NDS1 protocol
 */
int nds1_recv_channels( daq_t*         daq,
                        daq_channel_t* channel,
                        int            num_channels,
                        int*           num_channels_received );

/*
 *  Receive channel data using the NDS1 protocol for a single channel, by name.
 *
 *  FIXME: The NDS1 protocol's "status channels" command accepts any number of
 *  channel names as an optional argument, but the metadata is only valid for
 *  the zeroth channel. For the first, second, ..., Nth channels, the metadata
 *  (sample rate, type, units, etc.) are blank. If this bug in the NDS1 servers
 *  is ever fixed, then we should implement a multi-channel version of this
 *  query.
 */
int nds1_recv_channel( daq_t* daq, daq_channel_t* channel, const char* name );

/*  nds1_request_check(daq_t*, time_t, time_t)
 *
 *  Get requested channel data status for the specified interval.
 */
int nds1_request_check( daq_t* daq, time_t start, time_t end );

/*  nds1_request_data(daq_t*, time_t, time_t)
 *
 *  Get requested channel data for the specified interval.
 */
int nds1_request_data( daq_t* daq, time_t start, time_t dt );

/*  nds1_receive_reconfigure(daq_t* daq, long block_len)
 *
 *  Receive a reconfigure block. receive_reconfigure is received after the
 *  block header has been read in. The block length does not include the
 *  header length.
 */
int nds1_receive_reconfigure( daq_t* daq, size_t block_len );

/*
 *  Initialize the nds1 client
 *  Returns zero if OK or the error code if failed.
 */
int nds1_startup( void );

/*
 *  Supported NDS1 protocols
 */
#define MAX_NDS1_PROTOCOL_VERSION 12
#define MIN_NDS1_PROTOCOL_VERSION 11

#ifdef __cplusplus
}
#endif

#endif /* !defined(DAQC_NDS1_H) */
