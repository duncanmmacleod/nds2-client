/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
/* set vi: ts=8:softtabstop=4,shiftwidth=4,expandtab */

#ifndef NDS_AUTH_H
#define NDS_AUTH_H

#include "daqc.h"

/** @brief Authenticate client to server.
 *
 *  Perform authentication handshake with the server. The socket defined
 *  in the daq structure must be connected to the server.
 *
 *  @param daq NDS connection descriptor.
 *  @param server Server name string.
 *
 *  @return DAQD response code.
 */
int nds_authenticate( daq_t* daq, const char* server );

/** @brief  Clean up after authentication.
 *
 * Disconnect and delete authentication context.
 *
 *  @param daq NDS connection descriptor.
 *
 *  @return DAQD response code.
 */
int nds_auth_disconnect( daq_t* daq );

/** @brief Initialize authentication mechanism.
 *
 * Perform global authentication system initialization.
 *
 *  @return DAQD response code.
 */
int nds_auth_startup( void );

#endif /* !defined(NDS_AUTH_H) */
