#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "nds.hh"
#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    std::shared_ptr< NDS::connection > conn(
        std::make_shared< NDS::connection >( hostname, port ) );

    // The failure comes in with an unknown data type, which was triggered by
    // the addition of Virgo data
    // So just finishing is success
    NDS::channels_type chans = conn->find_channels(
        NDS::channel_predicate( "*", NDS::channel::CHANNEL_TYPE_RDS ) );
    std::cout << "chans.size == " << chans.size( ) << std::endl;
    NDS_ASSERT( chans.size( ) == 3 );
    NDS_ASSERT( chans[ 0 ].Name( ) == "X1:PEM-1" );
    NDS_ASSERT( chans[ 0 ].DataType( ) == NDS::channel::DATA_TYPE_UNKNOWN );
    NDS_ASSERT( chans[ 2 ].Name( ) == "X1:PEM-3" );
    NDS_ASSERT( chans[ 2 ].DataType( ) == NDS::channel::DATA_TYPE_FLOAT32 );
    conn->close( );
    return 0;
}
