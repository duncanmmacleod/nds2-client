#include "nds.hh"

#include <memory>
#include <iostream>
#include <cstdlib>
#include <sstream>

#include "test_macros.hh"
#include "nds_testing.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }
    connection::protocol_type proto = connection::PROTOCOL_TWO;

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    connection::channel_names_type ch;
    ch.push_back( "H1:DAQ-DC0_GPS" );

    conn->parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );
    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "TRUE" );

    bool error_with_iterate __attribute__( ( unused ) ) = false;
    try
    {
        auto stream = conn->iterate(
            NDS::request_period( 1169928640, 1169928640 + 2, 1 ), ch );
        for ( const auto& bufs : stream )
        {
            NDS_ASSERT( *( bufs->at( 0 ).cbegin< float >( ) ) == 0. );
        }
    }
    catch ( NDS::connection::daq_error& err )
    {
        std::cout << err.what( ) << std::endl;
        error_with_iterate = true;
    }
    NDS_ASSERT( !error_with_iterate );

    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "FALSE" );
    bool error_without_iterate __attribute__( ( unused ) ) = false;
    try
    {
        auto stream = conn->iterate(
            NDS::request_period( 1169928640, 1169928640 + 2, 1 ), ch );
        for ( const auto& bufs : stream )
        {
            NDS_ASSERT( false );
        }
    }
    catch ( NDS::connection::daq_error& /* err */ )
    {
        error_without_iterate = true;
    }
    NDS_ASSERT( !error_without_iterate );
    return 0;
}
