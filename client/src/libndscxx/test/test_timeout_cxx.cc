#include "nds.hh"
#include "nds_testing.hh"
#include "test_macros.hh"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <vector>

using NDS::channel;
using NDS::channels_type;
using NDS::buffers_type;

const std::string Header( "test_nds1_timeout_cxx" );

int
main( int argc, char** argv )
{
    std::vector< std::string > args = convert_args( argc, argv );

    using NDS::connection;

    std::string               hostname( "localhost" );
    connection::port_type     port = 31200;
    connection::protocol_type proto = connection::PROTOCOL_TRY;

    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }

    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if (::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps(::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    debug_message( Header ) << " about to connect to port: " << port
                            << " on host: " << hostname
                            << " using protocol: " << proto << std::endl;

    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    // This service this particular test connects to is setup to timeout on the
    // first command after the connection handshake.
    // So
    //  1. We expect to timeout which will show as a runtime_error.
    //  2. After a timeout we expect not to be able to do anything, this is why
    //     we are iterating, to prove we cannot do anything afterwards.
    for ( int iterations = 0; iterations < 2; ++iterations )
    {
        try
        {

            if ( proto == connection::PROTOCOL_TWO )
            {
                (void)( conn->count_channels( NDS::channel_predicate( "*" ) ) );
            }
            else
            {
                channels_type channels =
                    conn->find_channels( NDS::channel_predicate( ) );
            }

            debug_message( Header ) << " Received no exception" << std::endl;
            exit( 1 );
        }
        catch ( const std::runtime_error& Error )
        {
            debug_message( Header ) << " Received runtime_error: %s"
                                    << Error.what( ) << std::endl;
            if ( iterations > 0 )
            {
                exit( 0 );
            }
        }
        catch ( const std::exception& Error )
        {
            debug_message( Header ) << " Received wrong exception: %s"
                                    << Error.what( ) << std::endl;
            exit( 1 );
        }
        catch ( ... )
        {
            debug_message( Header ) << " Received unknown exception"
                                    << std::endl;
            exit( 1 );
        }
    }
    return 0;
}
