#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "nds.hh"
#include "test_macros.hh"
#include "nds_testing.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

// H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend
//[1167530460-1178294460)

int
main( int argc, char* argv[] )
{
    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    std::shared_ptr< NDS::connection > conn(
        std::make_shared< NDS::connection >( hostname, port ) );
    conn->parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );
    conn->parameters( ).set( "ALLOW_DATA_ON_TAPE", "true" );
    conn->parameters( ).set( "ITERATE_USE_GAP", "false" );

    NDS::buffer::gps_second_type        start = 1167530460;
    NDS::buffer::gps_second_type        stop = 1178294460;
    NDS::connection::channel_names_type names;
    names.push_back( "H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend" );

    std::vector< std::string > expected;
    expected.push_back( "66.7626" );
    expected.push_back( "0" );
    expected.push_back( "67.4257" );
    expected.push_back( "0" );
    expected.push_back( "0" );
    expected.push_back( "0" );

    std::vector< std::string > actual;

    int                          block_count = 0;
    NDS::buffer::gps_second_type last_gps __attribute__( ( unused ) ) = 0;

    auto stream = conn->iterate( NDS::request_period( start, stop ), names );
    for ( auto bufs : stream )
    {
        ++block_count;
        last_gps = bufs->at( 0 ).Stop( );
        const double* data = bufs->at( 0 ).cbegin< double >( );

        // the first sample is all zeros, so ask for something later
        {
            std::ostringstream ss;
            ss << data[ 120 ];
            actual.push_back( ss.str( ) );
        }
    }
    NDS_ASSERT( block_count == 6 );
    NDS_ASSERT( last_gps == 1178294460 );
    NDS_ASSERT( expected.size( ) == actual.size( ) );
    for ( int i = 0; i < expected.size( ); ++i )
    {
        NDS_ASSERT( expected[ i ] == actual[ i ] );
    }

    return 0;
}
