#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    bool                           path2 = find_opt( "-option-2", args );
    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = NDS::connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = NDS::connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = NDS::connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = NDS::connection::PROTOCOL_TWO;
    }

    pointer< NDS::connection > conn(
        make_unique_ptr< NDS::connection >( hostname, port, proto ) );

    NDS::connection::channel_names_type chans;
    int                                 expected_samples = 1;
    int                                 abort_at = 16;

    if ( proto == NDS::connection::PROTOCOL_ONE )
    {
        chans.push_back( "X1:FEC-117_CPU_METER" );
    }
    else
    {
        chans.push_back( "X1:FEC-117_CPU_METER,online" );
        expected_samples = 16;
        abort_at = 1;
    }
    std::vector< NDS::buffer::gps_nanosecond_type > nano_start_ref;
    std::vector< NDS::buffer::gps_nanosecond_type > nano_stop_ref;
    {
        auto                             samples_per_sec = abort_at;
        NDS::buffer::gps_nanosecond_type delta = 1000000000 / samples_per_sec;
        NDS::buffer::gps_nanosecond_type cur = 0;
        for ( auto i = 0; i < samples_per_sec; ++i )
        {
            nano_start_ref.push_back( cur );
            cur = ( cur + delta ) % 1000000000;
            nano_stop_ref.push_back( cur );
        }
    }

    if ( !path2 )
    {
        auto stream = conn->iterate(
            NDS::request_period( NDS::request_period::FAST_STRIDE ), chans );
        auto i = 0;
        for ( auto& bufs : stream )
        {
            auto& buf( bufs->at( 0 ) );
            NDS_ASSERT( buf.Samples( ) == expected_samples );
            std::cout << i << "] ";
            std::cout << buf.Start( ) << ":" << buf.StartNano( ) << "-"
                      << buf.Stop( ) << ":" << buf.StopNano( ) << ") "
                      << buf.at< int >( 0 ) << std::endl;
            std::cout << "expecting " << nano_start_ref[ i ] << "-"
                      << nano_stop_ref[ i ] << std::endl;
            NDS_ASSERT( buf.StartNano( ) == nano_start_ref[ i ] );
            NDS_ASSERT( buf.StopNano( ) == nano_stop_ref[ i ] );
            ++i;
            if ( i >= abort_at )
            {
                break;
            }
        }
    }
    else
    {
        auto stream = conn->iterate(
            NDS::request_period( 0, 1, NDS::request_period::FAST_STRIDE ),
            chans );
        auto i = 0;
        for ( auto& bufs : stream )
        {
            auto& buf( bufs->at( 0 ) );
            NDS_ASSERT( buf.Samples( ) == expected_samples );
            std::cout << i << "] ";
            std::cout << buf.Start( ) << ":" << buf.StartNano( ) << "-"
                      << buf.Stop( ) << ":" << buf.StopNano( ) << ") "
                      << buf.at< int >( 0 ) << std::endl;
            std::cout << "expecting " << nano_start_ref[ i ] << "-"
                      << nano_stop_ref[ i ] << std::endl;
            NDS_ASSERT( buf.StartNano( ) == nano_start_ref[ i ] );
            NDS_ASSERT( buf.StopNano( ) == nano_stop_ref[ i ] );
            ++i;
        }
    }
    return 0;
}
