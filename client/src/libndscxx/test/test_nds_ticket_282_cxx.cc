//
// Created by jonathan.hanks on 9/18/17.
//

#include <iostream>
#include <sstream>

#include <nds.hh>

#include "test_macros.hh"
#include "nds_testing.hh"

#include <stdlib.h>

using namespace NDS;

template < typename T >
T
rand_val( T max )
{
    return static_cast< T >( max * static_cast< float >( rand( ) ) /
                             static_cast< float >( RAND_MAX ) );
}

int
main( int argc, char* argv[] )
{

    std::vector< std::string > args = convert_args( argc, argv );
    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }
    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_ONE;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = NDS::connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = NDS::connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = NDS::connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = NDS::connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< NDS::connection > conn(
        make_unique_ptr< NDS::connection >( hostname, port, proto ) );

    NDS_ASSERT( conn->parameters( ).get( "CYCLE_NDS1_AFTER_N_COMMANDS" ) ==
                "1000" );

    srand( time( nullptr ) );
    buffer::gps_second_type dur_test = 100;

    connection::channel_names_type channels;
    channels.push_back( "H1:ASC-DHARD_P_IN1_DQ" );

    int d = 0;

    /* The full test needs to be able to cycle a connection.
    * The test framework is not setup to handle that yet.
    * So what we test is part of the 282 fix, the caching of channel
    * meta-data.  The test checks that we are sending the minimal number
    * of nds1 commands behind the scenes.
    */
    while ( d < 500 )
    {
        d = d + 1;
        buffer::gps_second_type start_time = 1189205111;

        buffers_type bufs =
            conn->fetch( start_time, start_time + dur_test, channels );
        std::cout << " Read test " << d << " Time " << start_time << std::endl;
    }
    return 0;
}
