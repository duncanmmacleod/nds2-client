#include "nds.hh"
#include "nds_helper.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "nds_testing.hh"

template < typename T >
std::string
to_string( const T& obj )
{
    std::ostringstream os;
    os << obj;
    return os.str( );
}

void
my_terminate_handler( )
{
    exit( 1 );
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::set_terminate( my_terminate_handler );
    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TWO;
    if (::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps(::getenv( "NDS_TEST_PROTO" ) );
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    {
        std::ostringstream os;
        os << "<" << hostname << ":" << port << " (protocol version ";
        os << conn->parameters( ).protocol( ) << ")>";
        std::cerr << os.str( ) << std::endl;
        std::cerr << *conn << std::endl;
        NDS_ASSERT( to_string( *conn ) == os.str( ) );
    }

    NDS::buffer::gps_second_type gps_start = 1116733655;
    NDS::buffer::gps_second_type gps_stop = 1116733675;

    connection::channel_names_type cn;

    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ" );
    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ" );
    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ" );
    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ" );
    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ" );
    cn.push_back( "H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ" );
    cn.push_back( "H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ" );
    cn.push_back( "H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ" );
    cn.push_back( "H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ" );
    cn.push_back( "H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ" );
    cn.push_back( "H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ" );
    cn.push_back( "H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ" );
    cn.push_back( "H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ" );
    cn.push_back( "H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ" );
    cn.push_back( "H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ" );
    cn.push_back( "H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ" );

    NDS::epoch cur_epoch( gps_start, gps_stop );

    NDS::availability_list_type avail = conn->get_availability( cur_epoch, cn );

    std::cerr << avail << std::endl;

    NDS_ASSERT( cn.size( ) == avail.size( ) );
    for ( int i = 0; i < cn.size( ); ++i )
    {
        NDS_ASSERT( cn[ i ] == avail[ i ].name );
        NDS_ASSERT( avail[ i ].data.size( ) == 2 );
        NDS_ASSERT( avail[ i ].data[ 0 ].frame_type == "H-H1_C" );
        NDS_ASSERT( avail[ i ].data[ 1 ].frame_type == "H-H1_R" );
        NDS_ASSERT( avail[ i ].data[ 0 ].gps_start ==
                    avail[ i ].data[ 1 ].gps_start );
        NDS_ASSERT( avail[ i ].data[ 0 ].gps_start == 1116733655 );
        NDS_ASSERT( avail[ i ].data[ 0 ].gps_stop ==
                    avail[ i ].data[ 1 ].gps_stop );
        NDS_ASSERT( avail[ i ].data[ 0 ].gps_stop == 1116733675 );
    }

    {
        std::string expected( "( <H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( "
                              "H-H1_C:1116733655-1116733675 "
                              "H-H1_R:1116733655-1116733675 ) >" );
        std::string actual( to_string( avail ).substr( 0, expected.size( ) ) );
        std::cerr << expected << std::endl << actual << std::endl;
        NDS_ASSERT( expected == actual );
    }

    {
        std::string expected( "<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( "
                              "H-H1_C:1116733655-1116733675 "
                              "H-H1_R:1116733655-1116733675 )" );
        std::string actual(
            to_string( avail[ 0 ] ).substr( 0, expected.size( ) ) );
        std::cerr << expected << std::endl << actual << std::endl;
        NDS_ASSERT( expected == actual );
    }

    {
        NDS::availability_list_type test_avail;
        NDS::availability           entry;

        entry.name = std::string( "X1:TEST_1" );
        entry.data.push_back( NDS::segment_list_type::value_type( NDS::segment(
            std::string( "X-X1_C" ), 1116733655, 1116733655 + 1024 ) ) );
        entry.data.push_back( NDS::segment_list_type::value_type( NDS::segment(
            std::string( "X-X1_R" ), 1116733655, 1116733655 + 100 ) ) );
        entry.data.push_back( NDS::segment_list_type::value_type(
            NDS::segment( std::string( "X-X1_R" ),
                          1116733655 + 1024,
                          1116733655 + 1024 + 100 ) ) );
        test_avail.push_back( entry );

        // std::cerr << test_avail << std::endl;

        NDS::simple_availability_list_type simple_test =
            test_avail.simple_list( );

        {
            std::string expected( "( ( <1116733655-1116734779> ) )" );
            std::string actual(
                to_string( simple_test ).substr( 0, expected.size( ) ) );
            std::cerr << expected << std::endl << actual << std::endl;
            NDS_ASSERT( expected == actual );
        }

        // std::cerr << simple_test << std::endl;

        NDS_ASSERT( simple_test.size( ) == 1 );
        NDS_ASSERT( simple_test[ 0 ].size( ) == 1 );
        NDS_ASSERT( simple_test[ 0 ][ 0 ].gps_start == 1116733655 );
        NDS_ASSERT( simple_test[ 0 ][ 0 ].gps_stop == 1116733655 + 1024 + 100 );
    }
    // test the implicit availability call in fetch
    NDS::buffers_type bufs = conn->fetch( gps_start, gps_stop, cn );
    std::cerr << bufs << std::endl;
    std::cerr << bufs[ 0 ] << std::endl;
    {
        std::string expected( "('<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (GPS time "
                              "1116733655, 10240 "
                              "samples)>','<H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ "
                              "(GPS time 1116733655, 10240 samples)>','" );
        std::string actual( to_string( bufs ).substr( 0, expected.size( ) ) );
        std::cerr << expected << std::endl << actual << std::endl;
        NDS_ASSERT( expected == actual );
    }
    {
        NDS::buffer&  buf = bufs[ 0 ];
        NDS::channel* ch = reinterpret_cast< NDS::channel* >( &buf );
        std::string   expected(
            "<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (512Hz, RAW, FLOAT32)>" );
        std::string actual( to_string( *ch ) );
        std::cerr << expected << std::endl << actual << std::endl;
        NDS_ASSERT( expected == actual );
    }
    // std::cerr << *dynamic_cast<NDS::channel*>(&bufs[0]) << std::endl;

    // Test larger fetches (this leads to > 128kb data returned from the server
    {
        NDS::epoch                          all_time( 0, 1999999999 );
        NDS::connection::channel_names_type cn_big;
        cn_big.push_back( "H1:GDS-CALIB_STRAIN,reduced" );
        NDS::availability_list_type avail_big =
            conn->get_availability( all_time, cn_big );
    }
    return 0;
}
