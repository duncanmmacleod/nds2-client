//
// Created by jonathan.hanks on 5/23/18.
//

#include "nds_channel.hh"
#include "nds_buffer.hh"

#include "nds_testing.hh"

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <iostream>
#include <type_traits>
#include <vector>
#include <complex>

const int SAMPLE_COUNT = 256;

template < typename T >
bool
check_data_point( T value, int ref )
{
    return value == static_cast< T >( ref );
}

template <>
bool
check_data_point< std::complex< float > >( std::complex< float > value,
                                           int                   ref )
{
    return value.real( ) == static_cast< float >( ref ) &&
        value.imag( ) == static_cast< float >( ref + 1 );
}

template < typename T >
std::vector< T >
generate_test_data( )
{
    std::vector< T > results;
    results.reserve( SAMPLE_COUNT );
    for ( auto i = 0; i < SAMPLE_COUNT; ++i )
    {
        results.push_back( static_cast< T >( i ) );
    }
    return results;
}

template <>
std::vector< std::complex< float > >
generate_test_data< std::complex< float > >( )
{
    static_assert( sizeof( std::complex< float > ) == sizeof( float ) * 2,
                   "std::complex<float> is != 2 floats!" );
    std::vector< std::complex< float > > results;
    results.reserve( SAMPLE_COUNT );
    for ( auto i = 0; i < SAMPLE_COUNT; ++i )
    {
        results.emplace_back( std::complex< float >(
            static_cast< float >( i ), static_cast< float >( i + 1 ) ) );
    }
    return results;
}

template < typename T, NDS::channel::data_type DT >
void
test_buffer_iterator( )
{
    static_assert( NDS::channel_data_type_conversion< T >::value == DT,
                   "Mismatch on test types" );
    std::vector< T > test_data = generate_test_data< T >( );

    NDS::channel chan( "test_chan",
                       NDS::channel::CHANNEL_TYPE_RAW,
                       DT,
                       SAMPLE_COUNT,
                       1.0,
                       1.0,
                       0.0,
                       "Unknown" );

    NDS::buffer buf( chan,
                     10000000,
                     0,
                     static_cast< const void* >( test_data.data( ) ),
                     test_data.size( ) * sizeof( T ) );

    auto begin = buf.cbegin< T >( );
    auto end = buf.cend< T >( );

    NDS_ASSERT( std::distance( buf.cbegin< T >( ), buf.cend< T >( ) ) ==
                std::distance( test_data.begin( ), test_data.end( ) ) );

    auto dist = std::distance( buf.cbegin< T >( ), buf.cend< T >( ) );

    NDS_ASSERT( std::distance( buf.cbegin< T >( ), buf.cend< T >( ) ) ==
                SAMPLE_COUNT );
    NDS_ASSERT( std::equal( begin, end, test_data.begin( ) ) );

    int i = 0;
    for ( auto cur = begin; cur < end; ++cur, ++i )
    {
        NDS_ASSERT( check_data_point( *cur, i ) );
    }
}

int
main( int argc, char* argv[] )
{
    test_buffer_iterator< std::int16_t, NDS::channel::DATA_TYPE_INT16 >( );
    test_buffer_iterator< std::int32_t, NDS::channel::DATA_TYPE_INT32 >( );
    test_buffer_iterator< std::int64_t, NDS::channel::DATA_TYPE_INT64 >( );
    test_buffer_iterator< float, NDS::channel::DATA_TYPE_FLOAT32 >( );
    test_buffer_iterator< double, NDS::channel::DATA_TYPE_FLOAT64 >( );
    test_buffer_iterator< std::complex< float >,
                          NDS::channel::DATA_TYPE_COMPLEX32 >( );
    test_buffer_iterator< std::uint32_t, NDS::channel::DATA_TYPE_UINT32 >( );

    return 0;
}
