#include "nds.hh"

#include "nds_testing.hh"

#include <cstdlib>

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <set>
#include <sstream>

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

using NDS::channel;
using NDS::channels_type;
using NDS::buffers_type;

const char* raw_bytepatterns[] = { "\xB0\x00",
                                   "\xDE\xAD\xBE\xEF",
                                   "\x00\x00\x00\x00\xFE\xED\xFA\xCE",
                                   "\xFE\xED\xFA\xCE" };

const char* min_bytepatterns[] = { "\x01\x01",
                                   "\x02\x02\x02\x02",
                                   "\x03\x03\x03\x03\x03\x03\x03\x03" };

const char* max_bytepatterns[] = { "\x11\x11",
                                   "\x12\x12\x12\x12",
                                   "\x13\x13\x13\x13\x13\x13\x13\x13" };

const char* mean_bytepatterns[] = { "\x1A\x1A\x1A\x1A\x1A\x1A\x1A\x1A",
                                    "\x2A\x2A\x2A\x2A\x2A\x2A\x2A\x2A",
                                    "\x3A\x3A\x3A\x3A\x3A\x3A\x3A\x3A" };

const char* rms_bytepatterns[] = { "\x1B\x1B\x1B\x1B\x1B\x1B\x1B\x1B",
                                   "\x2B\x2B\x2B\x2B\x2B\x2B\x2B\x2B",
                                   "\x3B\x3B\x3B\x3B\x3B\x3B\x3B\x3B" };

const char* n_bytepatterns[] = { "\x1C\x1C\x1C\x1C",
                                 "\x2C\x2C\x2C\x2C",
                                 "\x3C\x3C\x3C\x3C" };

bool
compare_bytes( const NDS::buffer& Source, const char* Pattern )
{
    bool        retval = true;
    const char* p( reinterpret_cast< const char* >( Pattern ) );

    const char* base =
        reinterpret_cast< const char* >( Source.cbegin< void >( ) );
    for ( NDS::buffer::size_type cur = 0; cur < Source.Samples( ); ++cur )
    {
        if ( !std::equal( &( base[ cur * Source.DataTypeSize( ) ] ),
                          &( base[ ( cur + 1 ) * Source.DataTypeSize( ) ] ),
                          p ) )
        {
            retval = false;
            break;
        }
    }

    return retval;
}

void
print_buffer_as_bytes( const NDS::buffer& Source )
{
    std::streamsize old_width = std::cerr.width( );
    std::cerr.width( 2 );

    NDS::buffer::size_type total = Source.Samples( ) * Source.DataTypeSize( );

    std::cerr << std::setfill( '0' );

    const char* base =
        reinterpret_cast< const char* >( Source.cbegin< void >( ) );
    for ( NDS::buffer::size_type i = 0; i < total; i++ )
    {
        std::cerr << std::setfill( '0' ) << std::setw( 2 ) << std::hex
                  << (int)base[ i ] << " ";
    }
    std::cerr << std::dec << std::endl;
    std::cerr.width( old_width );
    std::cerr << std::setfill( ' ' );
}

void
validate_trend_data( NDS::connection&             Connection,
                     const std::string&           Trend,
                     const std::string&           Stat,
                     NDS::buffer::gps_second_type Start,
                     NDS::buffer::gps_second_type End,
                     const char**                 BytePattern,
                     NDS::buffer::size_type       Samples,
                     NDS::buffer::size_type       Multiplier,
                     int DataMask = channel::DEFAULT_DATA_MASK )
{
    NDS::channels_type                  channel_info;
    buffers_type                        bufs;
    NDS::connection::channel_names_type cn;

    cn.push_back( std::string( "X1:A." ) + Stat + "," + Trend );
    cn.push_back( std::string( "X1:B." ) + Stat + "," + Trend );
    cn.push_back( std::string( "X1:C." ) + Stat + "," + Trend );

    bufs = Connection.fetch( Start, End, cn );
    NDS_ASSERT( bufs.size( ) == 3 );

    NDS::connection::channel_names_type::const_iterator cur_cn = cn.begin( );
    for ( buffers_type::const_iterator cur = bufs.begin( ), last = bufs.end( );
          cur != last;
          ++cur, ++cur_cn, ++BytePattern )
    {
        channel_info = Connection.find_channels(
            NDS::channel_predicate( *cur_cn,
                                    channel::DEFAULT_CHANNEL_MASK,
                                    ( channel::data_type )( DataMask ) ) );

        NDS_ASSERT( channel_info[ 0 ].Type( ) == cur->Type( ) );
        NDS_ASSERT( cur->Start( ) == Start );
        NDS_ASSERT( cur->StartNano( ) == 0 );
        NDS_ASSERT( cur->Samples( ) ==
                    ( Samples * cur->SampleRate( ) * Multiplier ) );
        NDS_ASSERT( compare_bytes( *cur, *BytePattern ) );
    }
}

inline static void
validate_meta_data( const NDS::channel&        Channel,
                    const char*                Name,
                    NDS::channel::channel_type Type,
                    NDS::channel::data_type    DataType,
                    int                        DataTypeSize,
                    double                     SampleRate,
                    float                      Gain,
                    float                      Slope,
                    float                      Offset,
                    const char*                Units )
{
    std::cerr << "Channel.SampleRate(): " << Channel.SampleRate( )
              << " Gain: " << Channel.Gain( ) << " Slope: " << Channel.Slope( )
              << " Offset: " << Channel.Offset( ) << " Units: '"
              << Channel.Units( ) << "'" << std::endl;
    NDS_ASSERT( Channel.NameLong( ).compare( Name ) == 0 );
    NDS_ASSERT( Channel.Type( ) == Type );
    NDS_ASSERT( Channel.DataType( ) == DataType );
    NDS_ASSERT( Channel.DataTypeSize( ) == DataTypeSize );
    NDS_ASSERT( Channel.SampleRate( ) == SampleRate );
    NDS_ASSERT( Channel.Gain( ) == Gain );
    NDS_ASSERT( Channel.Slope( ) == Slope );
    NDS_ASSERT( Channel.Offset( ) == Offset );
    NDS_ASSERT( Channel.Units( ).compare( Units ) == 0 );
}

int
main( int /* Argc */, char** /* ArgV */ ) try
{
    using NDS::connection;

    std::string        hostname( "localhost" );
    NDS::channels_type channels;

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 0;
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    //---------------------------------------------------------------------
    // Establish a connection
    //---------------------------------------------------------------------
    pointer< connection > conn( make_unique_ptr< connection >(
        hostname, port, connection::PROTOCOL_ONE ) );
    NDS_ASSERT( conn->parameters( ).host( ).compare( hostname ) == 0 );
    NDS_ASSERT( conn->parameters( ).port( ) == port );
    NDS_ASSERT( conn->parameters( ).protocol( ) == 1 );

    // exit( 0 );
    //---------------------------------------------------------------------
    // This test does a simple count of channels
    //
    // There is channels A - I (9)
    // Each channel has:
    //     Raw Data (+1)
    //     Second Trend Data (+5)
    //     Minute Trend Data (+5)
    //
    // NOTE:
    //     update this check when the channel count is
    //     changed on the mock server
    //---------------------------------------------------------------------
    channels = conn->find_channels( NDS::channel_predicate( "*" ) );

    static const int BASE_CHANNEL_COUNT __attribute__( ( unused ) ) = 9;
    std::cerr << "channels.size( ) = " << channels.size( ) << std::endl;
    NDS_ASSERT( channels.size( ) == ( BASE_CHANNEL_COUNT * ( 5 * 2 + 1 ) ) );

    // exit( 0 );
    // ------------------------------------------------------------------
    //  Make sure name, channel type and data type are correct
    // ------------------------------------------------------------------

    channels =
        conn->find_channels( NDS::channel_predicate( "X*:{A,B,C,G,H,I}" ) );
    NDS_ASSERT( channels.size( ) == 6 );

    validate_meta_data( channels[ 0 ],
                        "<X1:A (16Hz, ONLINE, INT16)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_INT16,
                        2,
                        16.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );
    validate_meta_data( channels[ 1 ],
                        "<X1:B (16Hz, ONLINE, INT32)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_INT32,
                        4,
                        16.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );
    validate_meta_data( channels[ 2 ],
                        "<X1:C (16Hz, ONLINE, INT64)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_INT64,
                        8,
                        16.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );
    validate_meta_data( channels[ 3 ],
                        "<X1:G (16Hz, ONLINE, UINT32)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_UINT32,
                        4,
                        16.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );
    validate_meta_data( channels[ 4 ],
                        "<X1:H (65536Hz, ONLINE, FLOAT32)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_FLOAT32,
                        4,
                        65536.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );
    validate_meta_data( channels[ 5 ],
                        "<X1:I (16384Hz, ONLINE, UINT32)>",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DATA_TYPE_UINT32,
                        4,
                        16384.0,
                        1.0,
                        1.0,
                        0.0,
                        "Undef" );

    // exit( 0 );
    // ------------------------------------------------------------------
    //  Make sure trend names are correct
    // ------------------------------------------------------------------

    channels = conn->find_channels( NDS::channel_predicate( "X1:G*" ) );
    {
        static const char* e[] = {
            "<X1:G (16Hz, ONLINE, UINT32)>",
            "<X1:G.max,m-trend (0.0166667Hz, MTREND, UINT32)>",
            "<X1:G.max,s-trend (1Hz, STREND, UINT32)>",
            "<X1:G.mean,m-trend (0.0166667Hz, MTREND, FLOAT64)>",
            "<X1:G.mean,s-trend (1Hz, STREND, FLOAT64)>",
            "<X1:G.min,m-trend (0.0166667Hz, MTREND, UINT32)>",
            "<X1:G.min,s-trend (1Hz, STREND, UINT32)>",
            "<X1:G.rms,m-trend (0.0166667Hz, MTREND, FLOAT64)>",
            "<X1:G.rms,s-trend (1Hz, STREND, FLOAT64)>",
            "<X1:G.n,m-trend (0.0166667Hz, MTREND, INT32)>",
            "<X1:G.n,s-trend (1Hz, STREND, INT32)>"
        };

        std::set< std::string > expected;

        for ( size_t x = 0; x < ( sizeof( e ) / sizeof( *e ) ); ++x )
        {
            expected.insert( e[ x ] );
        }

        std::set< std::string > actual;

        for ( channels_type::const_iterator cur = channels.begin( ),
                                            last = channels.end( );
              cur != last;
              ++cur )
        {
            actual.insert( cur->NameLong( ) );
        }
        NDS_ASSERT( expected.size( ) == actual.size( ) );
        for ( std::set< std::string >::const_iterator cur = expected.begin( ),
                                                      last = expected.end( );
              cur != last;
              ++cur )
        {
            NDS_ASSERT( actual.find( *cur ) != actual.end( ) );
        }
        // exit( 0 );
        // NDS_ASSERT( expected == actual );
    }

    // exit( 0 );
    // ------------------------------------------------------------------
    //  Validate channel contents - method 1
    // ------------------------------------------------------------------

    {
        buffers_type                        bufs;
        NDS::connection::channel_names_type cn;

        cn.push_back( "X1:A" );
        cn.push_back( "X1:B" );
        cn.push_back( "X1:C" );
        cn.push_back( "X1:G" );

        std::cerr << "Beginning 4 channel fetch method 1" << std::endl;

        bufs = conn->fetch( 1000000000, 1000000004, cn );

        std::cerr << "4 channel fetch is complete" << std::endl;
        NDS_ASSERT( bufs.size( ) == 4 );
        const char** cur_rbp = raw_bytepatterns;
        for ( buffers_type::const_iterator cur = bufs.begin( ),
                                           last = bufs.end( );
              cur != last;
              ++cur, ++cur_rbp )
        {
            std::cerr << cur->NameLong( ) << " start=" << cur->Start( )
                      << " samples=" << cur->Samples( )
                      << " sample rate=" << cur->SampleRate( ) << std::endl;
            print_buffer_as_bytes( *cur );
            NDS_ASSERT( cur->Start( ) == 1000000000 );
            NDS_ASSERT( cur->StartNano( ) == 0 );
            NDS_ASSERT( ( cur->Samples( ) / cur->SampleRate( ) ) == 4 );
            NDS_ASSERT( compare_bytes( *cur, *cur_rbp ) );
        }
    }

    // ------------------------------------------------------------------
    //  Validate channel contents - method 2
    // ------------------------------------------------------------------

    {
        buffers_type                        bufs;
        NDS::connection::channel_names_type cn;

        cn.push_back( "X1:A" );
        cn.push_back( "X1:B" );
        cn.push_back( "X1:C" );
        cn.push_back( "X1:G" );

        std::cerr << "Beginning 4 channel fetch method 2" << std::endl;

        bufs = conn->fetch( 1000000000, 1000000004, cn );

        std::cerr << "4 channel fetch is complete" << std::endl;
        NDS_ASSERT( bufs.size( ) == 4 );
        const char** cur_rbp = raw_bytepatterns;
        for ( buffers_type::const_iterator cur = bufs.begin( ),
                                           last = bufs.end( );
              cur != last;
              ++cur, ++cur_rbp )
        {
            std::cerr << cur->NameLong( ) << " start=" << cur->Start( )
                      << " samples=" << cur->Samples( )
                      << " sample rate=" << cur->SampleRate( ) << std::endl;
            print_buffer_as_bytes( *cur );
            NDS_ASSERT( cur->Start( ) == 1000000000 );
            NDS_ASSERT( cur->StartNano( ) == 0 );
            NDS_ASSERT( ( cur->Samples( ) / cur->SampleRate( ) ) == 4 );
            NDS_ASSERT( compare_bytes( *cur, *cur_rbp ) );
        }
    }

    // ------------------------------------------------------------------
    //  Trend channels - minute and second
    // ------------------------------------------------------------------

    {
        NDS::buffer::gps_second_type start = 0, end = 0;
        int                          mult;
        int                          samples;

        NDS::connection::channel_names_type trend_types;

        trend_types.push_back( "s-trend" );
        trend_types.push_back( "m-trend" );

        for ( NDS::connection::channel_names_type::const_iterator
                  cur_trend = trend_types.begin( ),
                  last_trend = trend_types.end( );
              cur_trend != last_trend;
              ++cur_trend )
        {
            if ( cur_trend->compare( "s-trend" ) == 0 )
            {
                start = 1000000000;
                end = 1000000020;
                mult = 1;
                samples = 20;
            }
            else
            {
                start = 1000000020;
                end = 1000000140;
                mult = 60;
                samples = 2;
            }

            validate_trend_data( *conn,
                                 *cur_trend,
                                 "min",
                                 start,
                                 end,
                                 min_bytepatterns,
                                 samples,
                                 mult );
            validate_trend_data( *conn,
                                 *cur_trend,
                                 "max",
                                 start,
                                 end,
                                 max_bytepatterns,
                                 samples,
                                 mult );
            validate_trend_data( *conn,
                                 *cur_trend,
                                 "rms",
                                 start,
                                 end,
                                 rms_bytepatterns,
                                 samples,
                                 mult );
            validate_trend_data( *conn,
                                 *cur_trend,
                                 "mean",
                                 start,
                                 end,
                                 mean_bytepatterns,
                                 samples,
                                 mult );
            validate_trend_data( *conn,
                                 *cur_trend,
                                 "n",
                                 start,
                                 end,
                                 n_bytepatterns,
                                 samples,
                                 mult );
            std::cout << "Offline iterate" << std::endl;
            {
                buffers_type                        bufs;
                NDS::connection::channel_names_type cn;

                cn.push_back( std::string( "X1:A.mean," ) + *cur_trend );
                cn.push_back( std::string( "X1:B.mean," ) + *cur_trend );
                cn.push_back( std::string( "X1:C.mean," ) + *cur_trend );

                bufs =
                    **( conn->iterate( NDS::request_period( start, end ), cn )
                            .begin( ) );
                NDS_ASSERT( bufs.size( ) == 3 );

                const char** bp = mean_bytepatterns;
                NDS::connection::channel_names_type::const_iterator cur_cn =
                    cn.begin( );

                for ( buffers_type::const_iterator cur = bufs.begin( ),
                                                   last = bufs.end( );
                      cur != last;
                      ++cur, ++cur_cn, ++bp )
                {
                    NDS_ASSERT( cur->Start( ) == start );
                    NDS_ASSERT( cur->StartNano( ) == 0 );
                    // a limit of the current mock nds server, trends only send
                    // one sample
                    NDS_ASSERT( cur->Samples( ) ==
                                ( samples * cur->SampleRate( ) * mult ) );
                    NDS_ASSERT( compare_bytes( *cur, *bp ) );
                }
            }

            {
                buffers_type                        bufs;
                NDS::connection::channel_names_type cn;

                cn.push_back( std::string( "X1:A.mean," ) + *cur_trend );
                cn.push_back( std::string( "X1:B.mean," ) + *cur_trend );
                cn.push_back( std::string( "X1:C.mean," ) + *cur_trend );

                bufs =
                    **( conn->iterate( NDS::request_period( start, end ), cn )
                            .begin( ) );
                NDS_ASSERT( bufs.size( ) == 3 );
                const char** bp = mean_bytepatterns;
                NDS::connection::channel_names_type::const_iterator cur_cn =
                    cn.begin( );

                for ( buffers_type::const_iterator cur = bufs.begin( ),
                                                   last = bufs.end( );
                      cur != last;
                      ++cur, ++cur_cn, ++bp )
                {
                    NDS_ASSERT( cur->Start( ) == start );
                    NDS_ASSERT( cur->StartNano( ) == 0 );
                    // a limit of the current mock nds server, trends only send
                    // one sample
                    NDS_ASSERT( cur->Samples( ) ==
                                ( samples * cur->SampleRate( ) * mult ) );
                    NDS_ASSERT( compare_bytes( *cur, *bp ) );
                }
            }
        } // for trend types
    } // Trend testing block

    // ------------------------------------------------------------------
    //  Iterator example
    // ------------------------------------------------------------------

    {
        //-------------------------------------------------------------------
        // This is an example of how to use the C++ implementation of
        // the python type of iterator
        //-------------------------------------------------------------------
        NDS::connection::channel_names_type cn;

        cn.push_back( "X1:A" );
        cn.push_back( "X1:B" );
        cn.push_back( "X1:C" );

        buffers_type bufs =
            **( conn->iterate( NDS::request_period( ), cn ).begin( ) );

        NDS_ASSERT( bufs.size( ) == 3 );
        const char** bp = raw_bytepatterns;
        NDS::connection::channel_names_type::const_iterator cur_cn =
            cn.begin( );

        for ( buffers_type::const_iterator cur = bufs.begin( ),
                                           last = bufs.end( );
              cur != last;
              ++cur, ++cur_cn, ++bp )
        {
            NDS_ASSERT( cur->Start( ) == 1000000000 );
            NDS_ASSERT( cur->StartNano( ) == 0 );
            NDS_ASSERT( cur->Samples( ) == cur->SampleRate( ) );
            NDS_ASSERT( compare_bytes( *cur, *bp ) );
        }
    }

    //---------------------------------------------------------------------
    // connection automatically closed when garbage collected,
    // but test explicit close anyway
    //---------------------------------------------------------------------
    conn->close( );

    //---------------------------------------------------------------------
    // Return the results
    //---------------------------------------------------------------------
    return ( 0 );
}
catch ( const std::exception& Exception )
{
    std::cerr << "FATAL: exception: " << Exception.what( ) << std::endl;
    NDS_ASSERT( 0 );
    return ( 1 );
}
catch ( ... )
{
    std::cerr << "FATAL: unknown exception: " << std::endl;
    return ( 2 );
}
