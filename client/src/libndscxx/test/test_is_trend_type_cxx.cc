#include "nds.hh"

#include <vector>
#include <string>

#include "nds_testing.hh"

using NDS::channel;

typedef std::vector< std::string > test_vector;

test_vector
create_mTrends( )
{
    test_vector retval;
    retval.push_back( "A,m-trend" );
    retval.push_back( "X1:ABC,m-trend" );
    retval.push_back( "X1:ABC.mean,m-trend" );
    retval.push_back( "A:,m-trend" );
    return retval;
}

test_vector
create_sTrends( )
{
    test_vector retval;
    retval.push_back( "A,s-trend" );
    retval.push_back( "X1:ABC,s-trend" );
    retval.push_back( "X1:ABC.mean,s-trend" );
    retval.push_back( "A:,s-trend" );
    return retval;
}

test_vector
create_junk( )
{
    test_vector retval;
    retval.push_back( "" );
    retval.push_back( "s-trend" );
    retval.push_back( ",s-trend" );
    retval.push_back( "m-trend" );
    retval.push_back( ",m-trend" );
    retval.push_back( "S-TREND" );
    retval.push_back( ",S-TREND" );
    retval.push_back( "M-TREND" );
    retval.push_back( ",M-TREND" );
    retval.push_back( ",mtrend" );
    retval.push_back( "STREND" );
    retval.push_back( ",strend" );
    retval.push_back( "MTREND" );
    retval.push_back( "X1:ABCCDEFGs-trend" );
    retval.push_back( "X1:ABCDEFGS-TREND" );
    retval.push_back( "X1:ABCCDEFGm-trend" );
    retval.push_back( "X1:ABCDEFGM-TREND" );
    retval.push_back( "X1:ABCDEFGM" );
    return retval;
}

int
main( int, char** )
{
    test_vector mTrends( create_mTrends( ) );
    test_vector sTrends( create_sTrends( ) );
    test_vector junk( create_junk( ) );

    for ( test_vector::iterator cur = mTrends.begin( ); cur != mTrends.end( );
          ++cur )
    {
        NDS_ASSERT( channel::IsMinuteTrend( *cur ) == true );
        NDS_ASSERT( channel::IsSecondTrend( *cur ) == false );
    }
    for ( test_vector::iterator cur = sTrends.begin( ); cur != sTrends.end( );
          ++cur )
    {
        NDS_ASSERT( channel::IsMinuteTrend( *cur ) == false );
        NDS_ASSERT( channel::IsSecondTrend( *cur ) == true );
    }
    for ( test_vector::iterator cur = junk.begin( ); cur != junk.end( ); ++cur )
    {
        NDS_ASSERT( channel::IsMinuteTrend( *cur ) == false );
        NDS_ASSERT( channel::IsSecondTrend( *cur ) == false );
    }
    return 0;
}
