#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TWO;

    NDS::buffer::gps_second_type start = 1107632127;
    NDS::buffer::gps_second_type end = 1107633000;

    NDS::connection::channel_names_type channels{ "X1:CHAN-1" };

    auto params = NDS::parameters( hostname, port, proto );
    NDS_ASSERT( params.set( "ALLOW_DATA_ON_TAPE", "1" ) );
    NDS_ASSERT( params.set( "GAP_HANDLER", "STATIC_HANDLER_NAN" ) );
    NDS_ASSERT( params.set( "ITERATE_USE_GAP_HANDLERS", "1" ) );
    try
    {
        auto stream =
            NDS::iterate( params, NDS::request_period( start, end ), channels );
        NDS_ASSERT( false );
    }
    catch ( NDS::connection::daq_error& err )
    {
        std::string err_msg = err.what( );

        std::cout << "err_msg = " << err_msg << std::endl;

        NDS_ASSERT( err_msg.find( "Unable to map channel name 'X1:CHAN-1'" ) !=
                    std::string::npos );
        NDS_ASSERT( err_msg.find( "Possible values include:" ) !=
                    std::string::npos );
        NDS_ASSERT( err_msg.find( "Possible values include:" ) !=
                    std::string::npos );
        NDS_ASSERT( err_msg.find( "<X1:CHAN-1 (256Hz, RAW, FLOAT32)>" ) !=
                    std::string::npos );
        NDS_ASSERT( err_msg.find( "<X1:CHAN-1 (512Hz, RAW, FLOAT32)>" ) !=
                    std::string::npos );
    }
    return 0;
}
