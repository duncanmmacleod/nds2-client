#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }
    connection::protocol_type proto = connection::PROTOCOL_TWO;
    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    NDS::epoch         epoch1( 1000000000, 1000000002 );
    NDS::channels_type channels_list_two_sec =
        conn->find_channels( NDS::channel_predicate( epoch1, "*" ) );

    NDS::channels_type channels_list_two_sec_a = conn->find_channels(
        NDS::channel_predicate( "*",
                                epoch1,
                                ( NDS::channel::channel_type )(
                                    NDS::channel::CHANNEL_TYPE_STREND |
                                    NDS::channel::CHANNEL_TYPE_MTREND ) ) );

    NDS::epoch         epoch2( 1000000000, 1000000001 );
    NDS::channels_type channels_list_one_sec =
        conn->find_channels( NDS::channel_predicate( "*", epoch2 ) );

    NDS::channels_type channels_list_one_sec_a = conn->find_channels(
        NDS::channel_predicate( epoch2,
                                "*",
                                ( NDS::channel::channel_type )(
                                    NDS::channel::CHANNEL_TYPE_STREND |
                                    NDS::channel::CHANNEL_TYPE_MTREND ) ) );

    NDS_ASSERT( channels_list_two_sec.size( ) > channels_list_one_sec.size( ) );
    NDS_ASSERT( channels_list_two_sec_a.size( ) >
                channels_list_one_sec_a.size( ) );
}
