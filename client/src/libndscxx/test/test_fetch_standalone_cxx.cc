#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_ONE;

    bool explicit_args = find_opt( "-explicit-args", args );
    if ( !explicit_args )
    {
        std::ostringstream os;
        os << hostname << ":" << port;
        set_env_val( "NDSSERVER", os.str( ) );
        set_env_val( "NDS2_CLIENT_PROTOCOL_VERSION", "1" );
    }

    NDS::buffer::gps_second_type start = 1108835634;
    NDS::buffer::gps_second_type end = start + 4;

    NDS::connection::channel_names_type channels{
        "H1:SUS-BS_BIO_ENCODE_DIO_0_OUT",
        "H1:SUS-BS_BIO_M1_MSDELAYON",
        "H1:SUS-BS_COMMISH_STATUS",
        "H1:SUS-BS_DACKILL_BYPASS_TIMEMON",
        "H1:SUS-BS_DCU_ID",
        "H1:SUS-BS_DITHERP2EUL_1_1",
        "H1:SUS-BS_DITHERP2EUL_2_1",
        "H1:SUS-BS_DITHERY2EUL_1_1",
        "H1:SUS-BS_DITHERY2EUL_2_1",
        "H1:SUS-BS_DITHER_P_IPCERR"
    };

    if ( explicit_args )
    {
        auto buffers = NDS::fetch(
            NDS::parameters( hostname, port, proto ), start, end, channels );
    }
    else
    {
        auto buffers = NDS::fetch( start, end, channels );
    }
    return 0;
}
