#ifndef NDS_FOREACH_HH
#define NDS_FOREACH_HH

namespace NDS
{

    namespace detail
    {

        /**
         * \brief Apply a given function to all elements of two iteratables.
         * \param[in] first1 Start of the first range
         * \param[in] last1 End of the first range
         * \param[in] first2 Start of the second range
         * \param[in] fn Function to apply
         * \returns fn
         *
         * \remarks This iterates on [first1, last1).  First2 must have
         * at least enough entries to as are contained in [first1, last1).
         * fn is called as fn(cur_list1, cur_list2)
         */
        template < class InputIterator1, class InputIterator2, class Function >
        Function
        for_each2( InputIterator1 first1,
                   InputIterator1 last1,
                   InputIterator2 first2,
                   Function       fn )
        {
            for ( ; first1 != last1; ++first1, ++first2 )
            {
                fn( *first1, *first2 );
            }
            return fn;
        }

        /**
         * \brief Apply a given function to all elements of three iteratables.
         * \param[in] first1 Start of the first range
         * \param[in] last1 End of the first range
         * \param[in] first2 Start of the second range
         * \param[in] first3 Start of the third range
         * \param[in] fn Function to apply
         * \returns fn
         *
         * \remarks This iterates on [first1, last1).  Each sequence first2,
         * first3 must have
         * at least enough entries to as are contained in [first1, last1).
         * fn is called as fn(cur_list1, cur_list2, cur_list3)
         */
        template < class InputIterator1,
                   class InputIterator2,
                   class InputIterator3,
                   class Function >
        Function
        for_each3( InputIterator1 first1,
                   InputIterator1 last1,
                   InputIterator2 first2,
                   InputIterator3 first3,
                   Function       fn )
        {
            for ( ; first1 != last1; ++first1, ++first2, ++first3 )
            {
                fn( *first1, *first2, *first3 );
            }
            return fn;
        }
    }
}

#endif // NDS_FOREACH_HH
