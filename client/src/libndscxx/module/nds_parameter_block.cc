#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <sstream>

#include "nds_memory.hh"
#include "nds_parameter_block.hh"
#include "nds_param_str.h"
#include "debug_stream.hh"

namespace NDS
{
    namespace detail
    {
        namespace
        {
            std::string
            str_lower( const std::string& input )
            {
                std::string dest( input );

                std::transform( input.begin( ),
                                input.end( ),
                                dest.begin( ),
                                []( char cur ) -> char {
                                    return static_cast< char >(
                                        std::tolower( cur ) );
                                } );
                return dest;
            }

            NDS::connection::protocol_type
            str_to_proto( const std::string&             input,
                          NDS::connection::protocol_type default_val )
            {
                NDS::detail::dout() << "str_to_proto(" << input << ")" << std::endl;
                std::string lower_str( str_lower( input ) );
                if ( lower_str == "try" || lower_str == "protocol_try")
                {
                    return NDS::connection::PROTOCOL_TRY;
                }
                else if ( lower_str == "1" || lower_str == "one" || lower_str == "protocol_one" )
                {
                    return NDS::connection::PROTOCOL_ONE;
                }
                else if ( lower_str == "2" || lower_str == "two" || lower_str == "protocol_two" )
                {
                    return NDS::connection::PROTOCOL_TWO;
                }
                return default_val;
            }

            int
            env_or_int( const std::string& key, int default_val )
            {
                char* tmp = std::getenv( key.c_str( ) );
                if ( !tmp )
                {
                    return default_val;
                }
                std::string str_val( tmp );
                try
                {
                    return std::stoi( tmp );
                }
                catch ( ... )
                {
                }
                return default_val;
            }

            bool
            env_or_bool( const std::string& key, bool default_val )
            {
                char* tmp = std::getenv( key.c_str( ) );
                if ( !tmp )
                {
                    return default_val;
                }
                bool result = true;
                if ( str_to_bool( std::string( tmp ), result ) )
                {
                    return result;
                }
                return default_val;
            }

            bool
            str_to_gap_handler( std::string        handler_name,
                                param_gap_handler& dest )
            {
                if ( handler_name == "ABORT_HANDLER" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< abort_gap_handler >( ) ) );
                    return true;
                }
                else if ( handler_name == "STATIC_HANDLER_ZERO" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< fixed_point_gap_handler >(
                            fixed_point_gap_handler::static_val::ZERO_VAL ) ) );
                    return true;
                }
                else if ( handler_name == "STATIC_HANDLER_ONE" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< fixed_point_gap_handler >(
                            fixed_point_gap_handler::static_val::ONE_VAL ) ) );
                    return true;
                }
                else if ( handler_name == "STATIC_HANDLER_NAN" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< fixed_point_gap_handler >(
                            fixed_point_gap_handler::static_val::NAN_VAL ) ) );
                    return true;
                }
                else if ( handler_name == "STATIC_HANDLER_POS_INF" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< fixed_point_gap_handler >(
                            fixed_point_gap_handler::static_val::
                                POS_INF_VAL ) ) );
                    return true;
                }
                else if ( handler_name == "STATIC_HANDLER_NEG_INF" )
                {
                    dest = std::move( param_gap_handler(
                        std::move( handler_name ),
                        NDS::detail::make_unique< fixed_point_gap_handler >(
                            fixed_point_gap_handler::static_val::
                                NEG_INF_VAL ) ) );
                    return true;
                }
                return false;
            }

            param_gap_handler
            env_or_gap_handler( const std::string& key,
                                std::string        default_val )
            {
                char*       tmp = std::getenv( key.c_str( ) );
                std::string handler_name(
                    ( tmp ? std::string( tmp ) : std::move( default_val ) ) );

                param_gap_handler results(
                    "ABORT_HANDLER",
                    NDS::detail::make_unique< abort_gap_handler >( ) );
                str_to_gap_handler( handler_name, results );
                return results;
            }

            /// \brief prepend NDS_PARAM_ENV_PREFIX to str and return the result
            /// as a std::string
            /// \note This is safe to call if str is nullptr.
            std::string
            env_key( const char* str )
            {
                str = ( str ? str : "" );

                return std::string( NDS_PARAM_ENV_PREFIX ) + str;
            }

            param_net_conn_info
            env_extract_conn_info_or( param_net_conn_info default_val )
            {
                NDS::connection::port_type default_port =
                    NDS::connection::DEFAULT_PORT;
                {
                    std::string key = env_key( NDS_PARAM_PORT );
                    char*       tmp = getenv( key.c_str( ) );
                    if ( tmp )
                    {
                        try
                        {
                            default_port =
                                static_cast< decltype( default_port ) >(
                                    std::stoi( tmp ) );
                        }
                        catch ( ... )
                        {
                        }
                    }
                }
                NDS::connection::protocol_type default_proto =
                    NDS::connection::PROTOCOL_TRY;
                {
                    std::string key = env_key( NDS_PARAM_PROTOCOL_VERSION );
                    char*       tmp = getenv( key.c_str( ) );
                    if ( tmp )
                    {
                        default_proto = str_to_proto(
                            std::string( tmp ), NDS::connection::PROTOCOL_TRY );
                    }
                }
                try
                {
                    // first read from NDS_PARAM_HOSTNAME/PORT/PROTOCOL_VERSION
                    {
                        std::string key( env_key( NDS_PARAM_HOSTNAME ) );
                        char*       tmp_hostname = getenv( key.c_str( ) );
                        if ( tmp_hostname )
                        {
                            return param_net_conn_info(
                                tmp_hostname, default_port, default_proto );
                        }
                    }
                    // now read a string from NDSSERVER
                    {
                        char* tmp = getenv( "NDSSERVER" );
                        if ( tmp )
                        {
                            std::string data( tmp );
                            data = data.substr( 0, data.find( ',' ) );
                            auto                pos = data.find( ':' );
                            param_net_conn_info results( data.substr( 0, pos ),
                                                         default_port,
                                                         default_proto );
                            if ( pos != std::string::npos )
                            {
                                results.port = stoi(
                                    data.substr( pos + 1, std::string::npos ) );
                            }
                            return results;
                        }
                    }
                }
                catch ( ... )
                {
                }
                return default_val;
            }
        }

        bool
        str_to_bool( const std::string& input, bool& dest )
        {
            std::string lower_str = str_lower( input );
            if ( lower_str == "true" || lower_str == "yes" || lower_str == "1" )
            {
                dest = true;
                return true;
            }
            else if ( lower_str == "false" || lower_str == "no" ||
                      lower_str == "0" )
            {
                dest = false;
                return true;
            }
            return false;
        }

        parameter_block::parameter_block( )
            : _max_nds1_command_count( env_or_int(
                  env_key( NDS_CYCLE_NDS1_AFTER_N_COMMANDS ), 1000 ) ),
              _gap_handler( env_or_gap_handler(
                  env_key( NDS_PARAM_GAP_HANDLER ), "ABORT_HANDLER" ) ),
              _allow_data_on_tape( env_or_bool(
                  env_key( NDS_PARAM_ALLOW_DATA_ON_TAPE ), false ) ),
              _iterate_uses_gap_handler(
                  env_or_bool( env_key( NDS_PARAM_ITERATE_GAPS ), false ) ),
              _net_conn_info(
                  env_extract_conn_info_or( param_net_conn_info( ) ) )
        {
        }

        parameter_block::parameter_block( const param_net_conn_info& conn_info )
            : _max_nds1_command_count( env_or_int(
                  env_key( NDS_CYCLE_NDS1_AFTER_N_COMMANDS ), 1000 ) ),
              _gap_handler( env_or_gap_handler(
                  env_key( NDS_PARAM_GAP_HANDLER ), "ABORT_HANDLER" ) ),
              _allow_data_on_tape( env_or_bool(
                  env_key( NDS_PARAM_ALLOW_DATA_ON_TAPE ), false ) ),
              _iterate_uses_gap_handler(
                  env_or_bool( env_key( NDS_PARAM_ITERATE_GAPS ), false ) ),
              _net_conn_info( conn_info )
        {
        }

        bool
        parameter_block::set_gap_handler( const std::string& handler_str )
        {
            return str_to_gap_handler( handler_str, _gap_handler );
        }

        param_net_conn_info&
        parameter_block::conn_info( )
        {
            return _net_conn_info;
        }

        const param_net_conn_info&
        parameter_block::conn_info( ) const
        {
            return _net_conn_info;
        }

        std::string
        parameter_block::get( const std::string& parameter ) const
        {
            std::string result;
            if ( parameter == NDS_PARAM_ALLOW_DATA_ON_TAPE )
            {
                result = ( allow_data_on_tape( ) ? "true" : "false" );
            }
            else if ( parameter == NDS_PARAM_GAP_HANDLER )
            {
                result = _gap_handler.name;
            }
            else if ( parameter == NDS_PARAM_ITERATE_GAPS )
            {
                result = ( iterate_uses_gap_handler( ) ? "true" : "false" );
            }
            else if ( parameter == NDS_CYCLE_NDS1_AFTER_N_COMMANDS )
            {
                std::ostringstream os;
                os << _max_nds1_command_count;
                result = os.str( );
            }
            else if ( parameter == NDS_PARAM_HOSTNAME )
            {
                return result = _net_conn_info.hostname;
            }
            else if ( parameter == NDS_PARAM_PORT )
            {
                std::ostringstream os;
                os << _net_conn_info.port;
                result = os.str( );
            }
            else if ( parameter == NDS_PARAM_PROTOCOL_VERSION )
            {
                switch ( _net_conn_info.protocol )
                {
                case NDS::connection::PROTOCOL_ONE:
                    result = "1";
                    break;
                case NDS::connection::PROTOCOL_TWO:
                    result = "2";
                    break;
                case NDS::connection::PROTOCOL_TRY:
                    result = "try";
                    break;
                default:
                    result = "invalid";
                    break;
                }
            }
            return result;
        }

        bool
        parameter_block::set( const std::string& parameter,
                              const std::string& value )
        {
            if ( parameter == NDS_PARAM_ALLOW_DATA_ON_TAPE )
            {
                bool bool_val = true;
                if ( str_to_bool( value, bool_val ) )
                {
                    _allow_data_on_tape = bool_val;
                    return true;
                }
            }
            else if ( parameter == NDS_PARAM_GAP_HANDLER )
            {
                return set_gap_handler( value );
            }
            else if ( parameter == NDS_PARAM_ITERATE_GAPS )
            {
                bool bool_val = true;
                if ( str_to_bool( value, bool_val ) )
                {
                    _iterate_uses_gap_handler = bool_val;
                    return true;
                }
            }
            else if ( parameter == NDS_CYCLE_NDS1_AFTER_N_COMMANDS )
            {
                try
                {
                    _max_nds1_command_count = std::stoi( value );
                }
                catch ( ... )
                {
                    return false;
                }
                return true;
            }
            else if ( parameter == NDS_PARAM_HOSTNAME )
            {
                _net_conn_info.hostname = value;
                return true;
            }
            else if ( parameter == NDS_PARAM_PORT )
            {
                try
                {
                    _net_conn_info.port =
                        static_cast< decltype( _net_conn_info.port ) >(
                            std::stoi( value ) );
                }
                catch ( ... )
                {
                    return false;
                }
                return true;
            }
            else if ( parameter == NDS_PARAM_PROTOCOL_VERSION )
            {
                _net_conn_info.protocol =
                    str_to_proto( value, NDS::connection::PROTOCOL_TRY );
                return true;
            }

            return false;
        }

        std::vector< std::string >
        parameter_block::parameters( ) const
        {
            std::vector< std::string > results{
                NDS_PARAM_ALLOW_DATA_ON_TAPE, NDS_PARAM_GAP_HANDLER,
                NDS_PARAM_ITERATE_GAPS,       NDS_CYCLE_NDS1_AFTER_N_COMMANDS,
                NDS_PARAM_HOSTNAME,           NDS_PARAM_PORT,
                NDS_PARAM_PROTOCOL_VERSION
            };
            return results;
        }

        std::string
        parameter_block::env_prefix( ) const
        {
            return std::string( NDS_PARAM_ENV_PREFIX );
        }
    }
}
