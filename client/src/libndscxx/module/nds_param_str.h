#ifndef NDS_PARAMETER_STRING_H
#define NDS_PARAMETER_STRING_H

/* Generic prefix to use when reading a variable from the environment */
#define NDS_PARAM_ENV_PREFIX "NDS2_CLIENT_"

/* Preference name relating to handling data on tape */
#define NDS_PARAM_ALLOW_DATA_ON_TAPE "ALLOW_DATA_ON_TAPE"

/*
 * For organizational purposes put items that are only used in the C++ bindings
 * below here.
 */

/* Preference name relating to the gap handler to use */
#define NDS_PARAM_GAP_HANDLER "GAP_HANDLER"

/* Iterate does not 'need' to work on full data set, set to false to return
 * availble
 * data and skip gaps.  Set to true to use the configured gap handler. */
#define NDS_PARAM_ITERATE_GAPS "ITERATE_USE_GAP_HANDLERS"

/*
 * Should the nds1 connection be cycled after a number of commands.
 */
#define NDS_CYCLE_NDS1_AFTER_N_COMMANDS "CYCLE_NDS1_AFTER_N_COMMANDS"

/*
 * Specifies a hostname to connect to
 */
#define NDS_PARAM_HOSTNAME "HOSTNAME"

/*
 * Specifies a port to connect to
 */
#define NDS_PARAM_PORT "PORT"

/*
 * Specifies a protocol version to connect with
 */
#define NDS_PARAM_PROTOCOL_VERSION "PROTOCOL_VERSION"

#endif /* NDS_PARAMETER_STRING_H */