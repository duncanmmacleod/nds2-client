#include "nds.hh"
#include "nds_channel.hh"
#include "nds_buffer.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_composer.hh"
#include "nds_helper.hh"
#include "nds_foreach.hh"
#include "nds_memory.hh"

#include "nds_errno.hh"

#include "debug_stream.hh"

#include "nds_os.h"

#ifdef HAVE_IO_H
#include <io.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstring>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <utility>

namespace
{
    template < typename T, typename U >
    void
    destructive_translate_to_shared_ptr( T& input, U& output )
    {
        U intermediate; // use this to maintain our invariants on output, don't
        intermediate.reserve( input.size( ) );
        for ( typename T::iterator cur = input.begin( ); cur != input.end( );
              ++cur )
        {
            auto tmp = std::make_shared< typename T::value_type >( );
            tmp->swap( *cur );
            intermediate.push_back( tmp );
        }
        output.swap( intermediate );
    };

    template < typename T, typename U >
    void
    destructive_translate_to_shared_ptr_swp( T& input, U& output )
    {
        U intermediate; // use this to maintain our invariants on output, don't
        intermediate.reserve( input.size( ) );
        for ( typename T::iterator cur = input.begin( ); cur != input.end( );
              ++cur )
        {
            auto tmp = std::make_shared< typename T::value_type >( );
            std::swap( *tmp, *cur );
            intermediate.push_back( tmp );
        }
        output.swap( intermediate );
    };

    inline void
    time_range_validator( NDS::buffer::gps_second_type gps_start,
                          NDS::buffer::gps_second_type gps_stop )
    {
        if ( gps_stop < gps_start )
        {
            throw std::invalid_argument(
                "GPS start time is greater than GPS stop time" );
        }
    }
}

namespace NDS
{
    inline namespace abi_0
    {
        parameters::parameters( )
            : p_( detail::make_unique< detail::parameter_block >( ) )
        {
        }

        parameters::parameters( const NDS::connection::host_type& host,
                                NDS::connection::port_type        port,
                                NDS::connection::protocol_type    protocol )
            : p_( detail::make_unique< detail::parameter_block >(
                  detail::param_net_conn_info( host, port, protocol ) ) )
        {
        }

        parameters::parameters( const parameters& other )
            : p_( detail::make_unique< detail::parameter_block >( *other.p_ ) )
        {
        }

        parameters::parameters( parameters&& other ) noexcept = default;

        parameters::~parameters( ) = default;

        parameters&
        parameters::operator=( const parameters& other )
        {
            if ( this != &other )
            {
                p_ =
                    detail::make_unique< detail::parameter_block >( *other.p_ );
            }
            return *this;
        }

        parameters& parameters::
        operator=( parameters&& other ) noexcept = default;

        connection::host_type
        parameters::host( ) const
        {
            return p_->conn_info( ).hostname;
        }

        connection::port_type
        parameters::port( ) const
        {
            return p_->conn_info( ).port;
        }

        connection::protocol_type
        parameters::protocol( ) const
        {
            return p_->conn_info( ).protocol;
        }

        std::string
        parameters::get( const std::string& key ) const
        {
            return p_->get( key );
        }

        bool
        parameters::set( const std::string& key, const std::string& value )
        {
            return p_->set( key, value );
        }

        NDS::connection::parameters_type
        parameters::parameter_list( ) const
        {
            return p_->parameters( );
        }

        const channel_predicate_object::data_type_list
            channel_predicate_object::all_data_types{
                NDS::channel::data_type::DATA_TYPE_INT16,
                NDS::channel::data_type::DATA_TYPE_INT32,
                NDS::channel::data_type::DATA_TYPE_INT64,
                NDS::channel::data_type::DATA_TYPE_FLOAT32,
                NDS::channel::data_type::DATA_TYPE_FLOAT64,
                NDS::channel::data_type::DATA_TYPE_COMPLEX32,
                NDS::channel::data_type::DATA_TYPE_UINT32
            };
        const channel_predicate_object::channel_type_list
            channel_predicate_object::all_channel_types{
                NDS::channel::channel_type::CHANNEL_TYPE_ONLINE,
                NDS::channel::channel_type::CHANNEL_TYPE_RAW,
                NDS::channel::channel_type::CHANNEL_TYPE_RDS,
                NDS::channel::channel_type::CHANNEL_TYPE_STREND,
                NDS::channel::channel_type::CHANNEL_TYPE_MTREND,
                NDS::channel::channel_type::CHANNEL_TYPE_TEST_POINT,
                NDS::channel::channel_type::CHANNEL_TYPE_STATIC
            };

        //---------------------------------------------------------------------
        // Connection
        //---------------------------------------------------------------------
        connection::connection( const host_type& host,
                                port_type        port,
                                protocol_type    protocol )
        {
            //-------------------------------------------------------------------
            // Parameter validation
            //-------------------------------------------------------------------
            if ( ( protocol <= PROTOCOL_INVALID ) ||
                 ( protocol > PROTOCOL_TRY ) )
            {
                std::ostringstream msg;
                msg << "The value " << protocol
                    << " is an invalid protocol request";
                throw connection::error( msg.str( ) );
            }

            NDS::parameters params( host, port, protocol );
            //-------------------------------------------------------------------
            // Go on with establishing the connection
            // If there is any issue with allocating the internal private data
            //   structure, let the exception pass up to the caller for
            //   them to handle as this class has no meaning without the
            //   private data structure.
            //-------------------------------------------------------------------
            p_ = std::make_shared< detail::conn_p_type >( params );

            // connect = nds2_open(host, port, protocol);
        }

        connection::connection( const NDS::parameters& params )
            : p_( std::make_shared< detail::conn_p_type >( params ) )
        {
        }

        connection::~connection( ) = default;

        void
        connection::close( )
        {
            p_->shutdown( );
        }

        availability_list_type
        connection::get_availability( const epoch&              time_span,
                                      const channel_names_type& channel_names )
        {
            p_->sync_parameters( );
            return p_->get_availability( time_span, channel_names );
        }

        bool
        connection::check( buffer::gps_second_type   gps_start,
                           buffer::gps_second_type   gps_stop,
                           const channel_names_type& channel_names )
        {
            time_range_validator( gps_start, gps_stop );
            p_->sync_parameters( );
            return p_->check( gps_start, gps_stop, channel_names );
        }

        buffers_type
        connection::fetch( buffer::gps_second_type   gps_start,
                           buffer::gps_second_type   gps_stop,
                           const channel_names_type& channel_names )
        {
            time_range_validator( gps_start, gps_stop );
            p_->sync_parameters( );
            return p_->fetch( gps_start, gps_stop, channel_names );
        }

        connection::count_type
        connection::count_channels( const channel_predicate_object& pred )
        {
            p_->sync_parameters( );

            int channel_mask = 0;
            for ( auto val : pred.channel_types( ) )
            {
                channel_mask |= (int)val;
            }
            int data_mask = 0;
            for ( auto val : pred.data_types( ) )
            {
                data_mask |= (int)val;
            }
            return p_->count_channels( pred.glob( ),
                                       (NDS::channel::channel_type)channel_mask,
                                       (NDS::channel::data_type)data_mask,
                                       pred.sample_rates( ).minimum,
                                       pred.sample_rates( ).maximum );
        }

        channels_type
        connection::find_channels( const channel_predicate_object& pred )
        {
            time_range_validator( pred.time_span( ).gps_start,
                                  pred.time_span( ).gps_stop );
            p_->sync_parameters( );

            channels_type results;
            p_->find_channels( results, pred );
            return results;
        }

        parameters&
        connection::parameters( ) const
        {
            return p_->parameters_;
        }

        const channel::hash_type&
        connection::hash( ) const
        {
            p_->sync_parameters( );

            return p_->hash( );
        }

        epochs_type
        connection::get_epochs( )
        {
            p_->sync_parameters( );
            return p_->get_epochs( );
        }

        NDS::data_iterable
        NDS::connection::iterate( NDS::request_period       period,
                                  const channel_names_type& channel_names )
        {
            time_range_validator( period.start, period.stop );
            p_->sync_parameters( );

            std::shared_ptr< detail::iterate_handler > handler(
                p_->dispatch_iterate(
                    period.start, period.stop, period.stride, channel_names ) );
            return NDS::data_iterable( std::move( handler ) );
        }

        bool
        connection::request_in_progress( ) const
        {
            return p_->request_in_progress( );
        }

        //---------------------------------------------------------------------
        // Connection::error
        //---------------------------------------------------------------------
        connection::error::error( const std::string& What, int ErrNo )
            : std::runtime_error( What )
        {
            if ( ErrNo != 0 )
            {
                errno = ErrNo;
            }
        }

        //---------------------------------------------------------------------
        // Connection::already_closed_error
        //---------------------------------------------------------------------
        connection::already_closed_error::already_closed_error( )
            : error( "Connection has already been closed", NDS::ALREADY_CLOSED )
        {
        }

        //---------------------------------------------------------------------
        // Connection::minute_trend_error
        //---------------------------------------------------------------------
        connection::minute_trend_error::minute_trend_error( )
            : error(
                  "minute trend ranges need to be aligned on minute boundries",
                  NDS::MINUTE_TRENDS )
        {
        }

        //---------------------------------------------------------------------
        // Connection::transfer_busy_error
        //---------------------------------------------------------------------
        connection::transfer_busy_error::transfer_busy_error( )
            : error( "Connection already transfering data", NDS::TRANSFER_BUSY )
        {
        }

        //---------------------------------------------------------------------
        // Connection::unexpected_channels_received_error
        //---------------------------------------------------------------------
        connection::unexpected_channels_received_error::
            unexpected_channels_received_error( )
            : error( "An unexpected number of channels were received",
                     NDS::UNEXPECTED_CHANNELS_RECEIVED )
        {
        }

        //---------------------------------------------------------------------
        // Connection::daq_error
        //---------------------------------------------------------------------
        connection::daq_error::daq_error( int daq_code )
            : error( format( daq_code ), NDS::ERROR_LAST + daq_code ),
              error_code( daq_code )
        {
        }

        connection::daq_error::daq_error(
            int daq_code, const std::string& additional_information )
            : error( format( daq_code, additional_information.c_str( ) ),
                     NDS::ERROR_LAST + daq_code ),
              error_code( daq_code )
        {
        }

        connection::daq_error::daq_error( int         daq_code,
                                          const char* additional_information )
            : error( format( daq_code, additional_information ),
                     NDS::ERROR_LAST + daq_code ),
              error_code( daq_code )
        {
        }

        std::string
        connection::daq_error::format( int DAQCode, const char* extra )
        {
            std::ostringstream msg;

            msg << "Low level daq error occured"
                << " [" << DAQCode << "]"
                << ": " << daq_strerror( DAQCode );
            if ( extra )
            {
                msg << "\n" << extra;
            }
            if ( DAQCode == DAQD_ONTAPE )
            {
                msg << "\nThis is not a fatal error, it is a notification that "
                       "your data is not\n";
                msg << "immediately available.  If you set DATA_ON_TAPE to 1 "
                       "you "
                       "may reissue your request.\n";
                msg << "Please note that retrieving data from tape is slow, "
                       "and "
                       "may take several minutes or longer.\n";
                msg << "\nYou may request data on tape by doing either of\n";
                msg << "1. setting the 'NDS2_CLIENT_ALLOW_DATA_ON_TAPE' "
                       "environment variable to '1'\n";
                msg << "or 2. calling the "
                       "parameters().set(\"ALLOW_DATA_ON_TAPE\", "
                       "\"1\") on your connection object.";
            }
            return msg.str( );
        }

        std::ostream&
        operator<<( std::ostream& os, const connection& conn )
        {
            os << "<" << conn.parameters( ).host( ) << ":"
               << conn.parameters( ).port( );
            os << " (protocol version ";
            os << ( conn.parameters( ).protocol( ) ==
                            NDS::connection::PROTOCOL_TWO
                        ? "2)>"
                        : "1)>" );
            return os;
        }
    }
}
