#ifndef __NDS_GAP_HANDLER_HH__
#define __NDS_GAP_HANDLER_HH__

#include <stdint.h>

#include "nds_buffer.hh"

#include "nds_export.hh"

#include "nds_memory.hh"

namespace NDS
{
    namespace detail
    {
        class delayed_gap_handler
        {
        public:
            delayed_gap_handler( ) = default;
            ;

            virtual ~delayed_gap_handler( ) = default;
            ;

            virtual void operator( )( ) = 0;
        };

        class gap_handler
        {
        public:
            DLL_EXPORT gap_handler( ) = default;

            DLL_EXPORT virtual ~gap_handler( ) = default;

            DLL_EXPORT virtual std::unique_ptr< delayed_gap_handler >
            fill_gap( buffer&           cur_buffer,
                      buffer::size_type start_sample_offset,
                      buffer::size_type end_sample_offset ) const = 0;

            DLL_EXPORT virtual std::unique_ptr< gap_handler >
            clone( ) const = 0;
        };

        class fixed_point_gap_handler : public gap_handler
        {
        public:
            struct static_val
            {
                ::int16_t  int16val;
                ::int32_t  int32val;
                ::int64_t  int64val;
                float      float32val;
                double     float64val;
                float      complexrval;
                float      complexival;
                ::uint32_t uint32val;

                typedef enum {
                    ZERO_VAL = 0,
                    ONE_VAL,
                    NAN_VAL,
                    POS_INF_VAL,
                    NEG_INF_VAL,
                } fixed_values;
                DLL_EXPORT static_val(::int16_t  int16val,
                                      ::int32_t  int32val,
                                      ::int64_t  int64val,
                                      float      float32val,
                                      double     float64val,
                                      float      complexrval,
                                      float      complexival,
                                      ::uint32_t uint32val );

                DLL_EXPORT explicit static_val( fixed_values spec );

                DLL_EXPORT explicit static_val( double value = 0.0 );
            };

        public:
            DLL_EXPORT fixed_point_gap_handler( static_val::fixed_values spec )
                : val( spec ){};
            DLL_EXPORT fixed_point_gap_handler( const static_val& val )
                : val( val ){};
            DLL_EXPORT ~fixed_point_gap_handler( ) override = default;

            DLL_EXPORT std::unique_ptr< delayed_gap_handler >
            fill_gap( buffer&           cur_buffer,
                      buffer::size_type start_sample_offset,
                      buffer::size_type end_sample_offset ) const override;

            DLL_EXPORT std::unique_ptr< gap_handler >
                       clone( ) const override
            {
                return NDS::detail::make_unique< fixed_point_gap_handler >(
                    val );
            }

        protected:
            static_val val;
        };

        class continuation_gap_handler : public fixed_point_gap_handler
        {
        public:
            DLL_EXPORT continuation_gap_handler( const static_val default_val )
                : fixed_point_gap_handler( default_val ){};
            DLL_EXPORT ~continuation_gap_handler( ) override = default;

            DLL_EXPORT std::unique_ptr< delayed_gap_handler >
            fill_gap( buffer&           cur_buffer,
                      buffer::size_type start_sample_offset,
                      buffer::size_type end_sample_offset ) const override;

            DLL_EXPORT std::unique_ptr< gap_handler >
                       clone( ) const override
            {
                return NDS::detail::make_unique< continuation_gap_handler >(
                    val );
            }
        };

        class abort_gap_handler : public gap_handler
        {
        public:
            DLL_EXPORT abort_gap_handler( ){};
            DLL_EXPORT ~abort_gap_handler( ) override = default;

            DLL_EXPORT std::unique_ptr< delayed_gap_handler >
            fill_gap( buffer&           cur_buffer,
                      buffer::size_type start_sample_offset,
                      buffer::size_type end_sample_offset ) const override;
            DLL_EXPORT std::unique_ptr< gap_handler >
                       clone( ) const override
            {
                return std::unique_ptr< gap_handler >(
                    NDS::detail::make_unique< abort_gap_handler >( ) );
            }
        };
    }
}

#endif // __NDS_GAP_HANDLER_HH__
