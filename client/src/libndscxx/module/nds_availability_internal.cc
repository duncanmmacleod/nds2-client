//
// Created by jonathan.hanks on 4/18/18.
//

#include "nds_availability.hh"
#include "nds_helper.hh"

namespace NDS
{
    NDS::simple_segment_list_type
    availability::simple_list( ) const
    {
        simple_segment_list_type tmp;

        detail::simplify_availability_intl( *this, tmp );
        return tmp;
    }

    NDS::simple_availability_list_type
    availability_list_type::simple_list( ) const
    {
        simple_availability_list_type tmp;
        detail::simplify_availability_list_intl( *this, tmp );
        return tmp;
    }
}