//
// Created by jonathan.hanks on 9/6/18.
//
#ifndef NDS2_CLIENT_NDS_CHANNEL_SELECTION_HH
#define NDS2_CLIENT_NDS_CHANNEL_SELECTION_HH

#include <memory>

#include "nds_channel.hh"
#include "nds_connection_ptype.hh"

namespace NDS
{
    namespace detail
    {
        // Given a channel name return the channel name with any [ms]-trend
        // component stripped out
        DLL_EXPORT
        std::string filter_trend_from_string( const std::string& input );

        // Given a channel return the channel with the name stripped of
        // [ms]-trend.
        //
        // On NDS1 it can be necessary to strip the 's-trend', 'm-trend'
        // text from a channel name to make the names match between
        // channel listing and fetch/iterate
        DLL_EXPORT
        channel strip_trend_from_name( const channel& chan );

        // Given a channel name, try to determine which NDS::channel
        // it corresponds to.
        class channel_selector
        {
        public:
            enum class selection_method
            {
                UNIQUE_CHANNEL, /// require a unique channel, no expansion in
                /// epoch allowed
                FIRST_CHANNEL, /// take the first channel found, no expansion in
                /// epoch allowed
                UNIQUE_THEN_FIRST /// If there are no channels, allow a global
                /// epoch and take the first
            };

            // Initialize the channel_selector with a connection object
            DLL_EXPORT
            explicit channel_selector( std::shared_ptr< conn_p_type > conn )
                : conn_( std::move( conn ) )
            {
            }
            DLL_EXPORT
            channel_selector( const channel_selector& other ) = default;
            DLL_EXPORT
            channel_selector( channel_selector&& other ) = default;

            channel_selector&
            operator=( const channel_selector& other ) = default;
            channel_selector& operator=( channel_selector&& other ) = default;

            // Given a channel name return a channel object that it maps to, or
            // raise a daq_error
            // if no good match could be found.
            // Do not use for online channels
            DLL_EXPORT
            channel operator( )(
                const std::string& name,
                selection_method   policy = selection_method::UNIQUE_CHANNEL );

        private:
            // \brief Given a list of channels return a vector of pointers
            // into the input list that are filtered to be compatible with
            // the given channel_type_mask.  Then sort the list.
            // \note You must ensure that the input is around as long as the
            // output array is needed, as non-owning pointers are returned
            std::vector< const NDS::channel* > filter_and_sort_channel_list(
                NDS::channels_type&        input,
                NDS::channel::channel_type channel_type_mask );

            // \brief Given a list of possible channels, downselect to one
            // @param input List of channels
            // @param name The channel name
            // @param policy Is any match good, or does it need to be unambigous
            // @notes may throw if the policy can not be satisfied
            channel downselect( NDS::channels_type&        input,
                                const std::string&         name,
                                NDS::channel::channel_type channel_type_mask,
                                selection_method           policy );

            std::shared_ptr< conn_p_type > conn_;
        };
    }
}

#endif // NDS2_CLIENT_NDS_CHANNEL_SELECTION_HH
