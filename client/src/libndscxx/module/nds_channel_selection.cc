#define NOMINMAX

#include <algorithm>
#include <cstring>
#include <cctype>
#include <type_traits>
#include <iterator>

#include "nds_channel_selection.hh"
#include "nds_str_utility.hh"
#include "common/utils.hh"

namespace NDS
{
    namespace detail
    {
        std::string
        filter_trend_from_string( const std::string& input )
        {
            const static std::string mtrend( "m-trend" );
            const static std::string strend( "s-trend" );
            const static std::string sep( ",%" );
            std::string              results;
            results.reserve( input.size( ) );

            std::string            last_sep = "";
            std::string::size_type cur = 0, pos = 0;
            pos = input.find_first_of( sep, cur );
            while ( pos != std::string::npos )
            {
                std::string::size_type seg_size = pos - cur;
                const char*            start = input.data( ) + cur;
                if ( std::strncmp( start,
                                   mtrend.c_str( ),
                                   std::min( mtrend.size( ), seg_size ) ) !=
                         0 &&
                     std::strncmp( start,
                                   strend.c_str( ),
                                   std::min( strend.size( ), seg_size ) ) != 0 )
                {
                    results.append( last_sep );
                    results.append( input.c_str( ) + cur, seg_size );
                }
                last_sep = input[ pos ];
                cur = pos + 1;
                pos = input.find_first_of( sep, cur );
            }
            if ( std::strcmp( input.c_str( ) + cur, mtrend.c_str( ) ) != 0 &&
                 std::strcmp( input.c_str( ) + cur, strend.c_str( ) ) != 0 )
            {
                results.append( last_sep );
                results.append( input.c_str( ) + cur );
            }
            return results;
        }

        channel
        strip_trend_from_name( const channel& chan )
        {
            channel result( filter_trend_from_string( chan.Name( ) ),
                            chan.Type( ),
                            chan.DataType( ),
                            chan.SampleRate( ),
                            chan.Gain( ),
                            chan.Slope( ),
                            chan.Offset( ),
                            chan.Units( ) );
            return result;
        }

        bool
        channel_type_less_than( const channel* c1, const channel* c2 )
        {
            return c1->Type( ) < c2->Type( );
        }

        bool
        channel_type_equal( const channel* c1, const channel* c2 )
        {
            return c1->Type( ) == c2->Type( );
        }

        std::string
        err_msg_malformed_channel( const std::string& name )
        {
            if ( name.empty( ) )
            {
                return "A channel with no name was requested, channels must "
                       "have names.";
            }
            std::ostringstream err_msg;
            err_msg << "The requested channel '" << name
                    << "' is an invalid channel name.";
            return err_msg.str( );
        }

        std::string
        err_msg_lookup_failure( const std::string&             name,
                                NDS::connection::protocol_type protocol )
        {
            std::ostringstream err_msg;
            err_msg << "Unable to map channel name '" << name
                    << "' to an existing";
            if ( protocol == connection::protocol_type::PROTOCOL_ONE ||
                 protocol == connection::protocol_type::PROTOCOL_TWO )
            {
                err_msg << " NDS "
                        << ( protocol == connection::protocol_type::PROTOCOL_ONE
                                 ? 1
                                 : 2 );
            }
            err_msg << " channel.";
            return err_msg.str( );
        }

        template < typename It >
        std::string
        err_msg_lookup_ambigous( const std::string& name,
                                 It                 dup_begin,
                                 It                 dup_end )
        {
            typedef
                typename std::remove_pointer< typename It::value_type >::type
                               T_base_val;
            std::ostringstream err_msg;
            err_msg << "Unable to map channel name '" << name
                    << "' to an single NDS 2 channel.\n";
            err_msg << "  Possible values include:\n";
            std::ostream_iterator< T_base_val > out_it( err_msg, "\n" );
            std::transform(
                dup_begin,
                dup_end,
                out_it,
                []( const typename It::value_type it_val ) -> T_base_val& {
                    return *it_val;
                } );
            return err_msg.str( );
        }

        std::vector< const NDS::channel* >
        channel_selector::filter_and_sort_channel_list(
            NDS::channels_type&        input,
            NDS::channel::channel_type channel_type_mask )
        {
            std::vector< const NDS::channel* > filtered_set;
            auto filter_pred = [=]( const channel& cur ) -> bool {
                return ( cur.Type( ) != channel::CHANNEL_TYPE_UNKNOWN &&
                         ( cur.Type( ) & channel_type_mask ) != 0 );
            };
            std::for_each( input.begin( ),
                           input.end( ),
                           [&filter_pred, &filtered_set]( const channel& cur ) {
                               if ( filter_pred( cur ) )
                               {
                                   filtered_set.push_back( &cur );
                               }
                           } );
            std::sort( filtered_set.begin( ),
                       filtered_set.end( ),
                       detail::channel_type_less_than );
            return filtered_set;
        }

        channel
        channel_selector::downselect(
            NDS::channels_type&        input,
            const std::string&         name,
            NDS::channel::channel_type channel_type_mask,
            selection_method           policy )
        {
            std::vector< const channel* > filtered_set{
                filter_and_sort_channel_list( input, channel_type_mask )
            };

            if ( filtered_set.empty( ) )
            {
                throw connection::daq_error(
                    DAQD_ERROR,
                    detail::err_msg_lookup_failure( name, conn_->protocol ) );
            }
            if ( policy == selection_method::FIRST_CHANNEL )
            {
                return *( filtered_set[ 0 ] );
            }
            if ( conn_->protocol == NDS::connection::PROTOCOL_ONE )
            {
                if ( input.size( ) == 1 )
                {
                    return input[ 0 ];
                }
                throw connection::daq_error(
                    DAQD_ERROR,
                    detail::err_msg_lookup_failure( name, conn_->protocol ) );
            }

            auto dup =
                nds_impl::common::adjacent_find( filtered_set.begin( ),
                                                 filtered_set.end( ),
                                                 detail::channel_type_equal );
            if ( dup != filtered_set.end( ) )
            {
                auto dup_end = std::find_if_not(
                    dup, filtered_set.end( ), [dup]( const channel* cur ) {
                        return cur->Type( ) == ( *dup )->Type( );
                    } );
                throw connection::daq_error(
                    DAQD_ERROR,
                    detail::err_msg_lookup_ambigous( name, dup, dup_end ) );
            }
            const channel* selection = filtered_set.front( );
            NDS::detail::dout( )
                << "Selected channel " << selection->Name( ) << ","
                << channel::ToString( selection->Type( ) ) << ","
                << selection->SampleRate( );
            NDS::detail::dout( )
                << " (" << channel::ToString( selection->DataType( ) ) << ")"
                << std::endl;
            return *selection;
        }

        channel
        channel_selector::operator( )( const std::string& name,
                                       selection_method   policy )
        {
            NDS::detail::dout( ) << "Searching for channel matching '" << name
                                 << "'" << std::endl;

            if ( conn_->protocol == connection::protocol_type::PROTOCOL_ONE )
            {
                channels_type lst;
                conn_->find_channels_nds1( lst,
                                           name,
                                           channel::DEFAULT_CHANNEL_MASK,
                                           channel::DEFAULT_DATA_MASK,
                                           channel::MIN_SAMPLE_RATE,
                                           channel::MAX_SAMPLE_RATE );
                return strip_trend_from_name( downselect(
                    lst, name, channel::DEFAULT_CHANNEL_MASK, policy ) );
            }

            std::string tmpName( name );

            // channel names may be specified with ',' or '%' as separators,
            // only deal with one of them
            std::transform(
                tmpName.begin( ),
                tmpName.end( ),
                tmpName.begin( ),
                []( char ch ) -> char { return ( ch == '%' ? ',' : ch ); } );
            std::string channel_name_str;
            std::string channel_rate_str;
            std::string channel_type_str;

            auto channel_type_mask = ( channel::channel_type )(
                channel::CHANNEL_TYPE_RAW | channel::CHANNEL_TYPE_RDS |
                channel::CHANNEL_TYPE_TEST_POINT |
                channel::CHANNEL_TYPE_STATIC );
            channel::sample_rate_type min_sample_rate =
                channel::MIN_SAMPLE_RATE;
            channel::sample_rate_type max_sample_rate =
                channel::MAX_SAMPLE_RATE;

            std::vector< std::string > parts;
            split( tmpName, ',', parts );
            if ( parts.empty( ) || parts.size( ) > 3 )
            {
                throw connection::daq_error(
                    DAQD_ERROR, detail::err_msg_malformed_channel( name ) );
            }

            {
                std::vector< std::string >::iterator cur = parts.begin( );
                // exract the name
                channel_name_str = *cur;
                ++cur;
                // extract and classify the other optional portions of the name
                // type and rate [order is undefined]
                for ( ; cur != parts.end( ); ++cur )
                {
                    if ( cur->empty( ) )
                    {
                        continue;
                    }
                    if ( std::isdigit( ( *cur )[ 0 ] ) )
                    {
                        if ( !channel_rate_str.empty( ) )
                        {
                            throw connection::daq_error(
                                DAQD_ERROR,
                                detail::err_msg_malformed_channel( name ) );
                        }
                        std::istringstream rate_parser( *cur );
                        rate_parser >> min_sample_rate;
                        max_sample_rate = min_sample_rate;
                    }
                    else
                    {
                        if ( !channel_type_str.empty( ) )
                        {
                            throw connection::daq_error(
                                DAQD_ERROR,
                                detail::err_msg_malformed_channel( name ) );
                        }
                        channel_type_str = *cur;
                        // validate the type string
                        channel_type_mask = channel::convert_daq_chantype(
                            cvt_str_chantype( channel_type_str.c_str( ) ) );
                        if ( channel_type_mask ==
                             channel::CHANNEL_TYPE_UNKNOWN )
                        {
                            throw connection::daq_error(
                                DAQD_ERROR,
                                detail::err_msg_lookup_failure(
                                    name, conn_->protocol ) );
                        }
                    }
                }
            }

            NDS::detail::dout( ) << "Name broken into parts "
                                 << channel_name_str << "," << channel_type_str
                                 << "," << channel_rate_str << std::endl;
            // unfortunately we don't always get good results when specifying a
            // mask
            // on the channel type
            channels_type cList;
            conn_->find_channels_nds2( cList,
                                       channel_name_str,
                                       channel::DEFAULT_CHANNEL_MASK,
                                       channel::DEFAULT_DATA_MASK,
                                       min_sample_rate,
                                       max_sample_rate );

            if ( cList.empty( ) &&
                 policy == selection_method::UNIQUE_THEN_FIRST )
            {
                epoch e = conn_->current_epoch( );
                conn_->set_epoch( 0, buffer::GPS_INF );
                conn_->find_channels_nds2( cList,
                                           channel_name_str,
                                           channel::DEFAULT_CHANNEL_MASK,
                                           channel::DEFAULT_DATA_MASK,
                                           min_sample_rate,
                                           max_sample_rate );
                conn_->set_epoch( e.gps_start, e.gps_stop );
                policy = selection_method::FIRST_CHANNEL;
            }

            return downselect( cList, name, channel_type_mask, policy );
        }
    }
}