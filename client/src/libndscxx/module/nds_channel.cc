#include <cstring>

#include <iostream>
#include <sstream>
#include <stdexcept>

#define NDS_CHANNEL_IMPLEMENTATION 1

#include "nds_channel.hh"

#include "daqc.h"

namespace NDS
{

    inline namespace abi_0
    {
        //---------------------------------------------------------------------

        const unsigned char channel::hash_type::zero_hash[] = { '\0' };
        const size_t        channel::hash_type::MAX_SIZE = 256;
        const channel::sample_rate_type channel::MIN_SAMPLE_RATE = 0.0f;
        const channel::sample_rate_type channel::MAX_SAMPLE_RATE = 1e12f;

        //---------------------------------------------------------------------

        channel::hash_type::hash_type( )
        {
            resize( MAX_SIZE );
        }

        bool
        channel::hash_type::compare( const void* Buffer, size_t Length ) const
        {
            size_t len =
                ( ( Length == (size_t)-1 ) ? ( size( ) ) : ( Length ) );

            return ( !::memcmp( Buffer, &( this->operator[]( 0 ) ), len ) );
        }

        void
        channel::hash_type::reset( )
        {
            resize( MAX_SIZE );
        }

        //=====================================================================
        //=====================================================================
        channel::channel( )
        {
        }

        channel::channel( const channel& Source ) = default;

        channel::channel( const std::string& Name,
                          channel_type       Type,
                          data_type          DataType,
                          sample_rate_type   SampleRate,
                          signal_gain_type   Gain,
                          signal_slope_type  Slope,
                          signal_offset_type Offset,
                          std::string        Units )
            : name( Name ), type( Type ), data( DataType ),
              sample_rate( SampleRate ), gain( Gain ), slope( Slope ),
              offset( Offset ), units( Units )
        {
        }

        std::string
        channel::NameLong( ) const
        {
            std::ostringstream retval;

            retval << "<" << Name( ) << " (" << SampleRate( ) << "Hz,"
                   << " " << ToString( Type( ) ) << ","
                   << " " << ToString( DataType( ) ) << ")>";

            return retval.str( );
        }

        const std::string&
        channel::ToString( channel_type ChannelType )
        {
            switch ( ChannelType )
            {
            case CHANNEL_TYPE_ONLINE:
            {
                static std::string v( "ONLINE" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_RAW:
            {
                static std::string v( "RAW" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_RDS:
            {
                static std::string v( "RDS" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_STREND:
            {
                static std::string v( "STREND" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_MTREND:
            {
                static std::string v( "MTREND" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_TEST_POINT:
            {
                static std::string v( "TEST_POINT" );
                return ( v );
            }
            break;
            case CHANNEL_TYPE_STATIC:
            {
                static std::string v( "STATIC" );
                return ( v );
            }
            break;
            default:
            {
                static std::string v( "UNKNOWN" );
                return ( v );
            }
            break;
            }
        }

        const std::string&
        channel::ToString( data_type DataType )
        {
            switch ( DataType )
            {
            case DATA_TYPE_INT16:
            {
                static std::string v( "INT16" );
                return ( v );
            }
            break;
            case DATA_TYPE_INT32:
            {
                static std::string v( "INT32" );
                return ( v );
            }
            break;
            case DATA_TYPE_INT64:
            {
                static std::string v( "INT64" );
                return ( v );
            }
            break;
            case DATA_TYPE_FLOAT32:
            {
                static std::string v( "FLOAT32" );
                return ( v );
            }
            break;
            case DATA_TYPE_FLOAT64:
            {
                static std::string v( "FLOAT64" );
                return ( v );
            }
            break;
            case DATA_TYPE_COMPLEX32:
            {
                static std::string v( "COMPLEX32" );
                return ( v );
            }
            break;
            case DATA_TYPE_UINT32:
            {
                static std::string v( "UINT32" );
                return ( v );
            }
            break;
            default:
            {
                static std::string v( "UNKNOWN" );
                return ( v );
            }
            break;
            }
        }

        channel::channel_type
        channel::convert_daq_chantype( int ChanType )
        {
            switch ( ChanType )
            {
            case cOnline:
                return CHANNEL_TYPE_ONLINE;
                break;
            case cRaw:
                return CHANNEL_TYPE_RAW;
                break;
            case cRDS:
                return CHANNEL_TYPE_RDS;
                break;
            case cSTrend:
                return CHANNEL_TYPE_STREND;
                break;
            case cMTrend:
                return CHANNEL_TYPE_MTREND;
                break;
            case cTestPoint:
                return CHANNEL_TYPE_TEST_POINT;
                break;
            case cStatic:
                return CHANNEL_TYPE_STATIC;
                break;
            case cUnknown:
            default:
                return CHANNEL_TYPE_UNKNOWN;
                break;
            }
        }

        channel::data_type
        channel::convert_daq_datatype( int DataType )
        {
            switch ( DataType )
            {
            //-------------------------------------------------------------------
            // Integers - Signed
            //-------------------------------------------------------------------
            case _16bit_integer:
                return DATA_TYPE_INT16;
                break;
            case _32bit_integer:
                return DATA_TYPE_INT32;
                break;
            case _64bit_integer:
                return DATA_TYPE_INT64;
                break;
            //-------------------------------------------------------------------
            // Integers - Unsigned
            //-------------------------------------------------------------------
            case _32bit_uint:
                return DATA_TYPE_UINT32;
                break;
            //-------------------------------------------------------------------
            // Reals
            //-------------------------------------------------------------------
            case _32bit_float:
                return DATA_TYPE_FLOAT32;
                break;
            case _64bit_double:
                return DATA_TYPE_FLOAT64;
                break;
            //-------------------------------------------------------------------
            // Complex
            //-------------------------------------------------------------------
            case _32bit_complex:
                return DATA_TYPE_COMPLEX32;
                break;
            case _undefined:
            default:
                return DATA_TYPE_UNKNOWN;
                break;
            }
        }

        void
        channel::swap( channel& Source )
        {
            std::string        tname( Source.name );
            channel_type       ttype( Source.type );
            data_type          tdata( Source.data );
            sample_rate_type   tsample_rate( Source.sample_rate );
            signal_gain_type   tgain( Source.gain );
            signal_slope_type  tslope( Source.slope );
            signal_offset_type toffset( Source.offset );
            std::string        tunits( Source.units );

            Source.name = name;
            Source.type = type;
            Source.data = data;
            Source.sample_rate = sample_rate;
            Source.gain = gain;
            Source.slope = slope;
            Source.offset = offset;
            Source.units = units;

            name = tname;
            type = ttype;
            data = tdata;
            sample_rate = tsample_rate;
            gain = tgain;
            slope = tslope;
            offset = toffset;
            units = tunits;
        }

        std::ostream&
        operator<<( std::ostream& os, const channel& ch )
        {
            os << ch.NameLong( );
            return os;
        }

        const std::string&
        channel_type_to_string( channel::channel_type ctype )
        {
            switch ( ctype )
            {
            case channel::CHANNEL_TYPE_ONLINE:
            {
                static std::string v( cvt_chantype_str( cOnline ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_RAW:
            {
                static std::string v( cvt_chantype_str( cRaw ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_RDS:
            {
                static std::string v( cvt_chantype_str( cRDS ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_STREND:
            {
                static std::string v( cvt_chantype_str( cSTrend ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_MTREND:
            {
                static std::string v( cvt_chantype_str( cMTrend ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_TEST_POINT:
            {
                static std::string v( cvt_chantype_str( cTestPoint ) );
                return ( v );
            }
            break;
            case channel::CHANNEL_TYPE_STATIC:
            {
                static std::string v( cvt_chantype_str( cStatic ) );
                return ( v );
            }
            break;
            default:
            {
                static std::string v( "UNKNOWN" );
                return ( v );
            }
            break;
            }
        }

        const std::string&
        data_type_to_string( channel::data_type dtype )
        {
            switch ( dtype )
            {
            case channel::DATA_TYPE_INT16:
            {
                static std::string v( data_type_name( _16bit_integer ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_INT32:
            {
                static std::string v( data_type_name( _32bit_integer ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_INT64:
            {
                static std::string v( data_type_name( _64bit_integer ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_FLOAT32:
            {
                static std::string v( data_type_name( _32bit_float ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_FLOAT64:
            {
                static std::string v( data_type_name( _64bit_double ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_COMPLEX32:
            {
                static std::string v( data_type_name( _32bit_complex ) );
                return v;
            }
            break;
            case channel::DATA_TYPE_UINT32:
            {
                static std::string v( data_type_name( _32bit_uint ) );
                return v;
            }
            break;
            default:
            {
                static std::string v( "undefined" );
                return v;
            }
            break;
            }
        }
    }
}
