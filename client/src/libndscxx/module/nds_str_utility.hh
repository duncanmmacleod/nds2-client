#ifndef NDS_DETAIL_STR_UTIL_HH
#define NDS_DETAIL_STR_UTIL_HH

#include <algorithm>
#include <string>
#include <vector>

namespace NDS
{
    namespace detail
    {
        template < typename iter >
        void
        split( iter                        first,
               iter                        last,
               char                        delim,
               std::vector< std::string >& dest )
        {
            std::vector< std::string > results;
            if ( first == last )
            {
                return;
            }
            iter cur = first;
            iter prev = first;
            cur = std::find( prev, last, delim );
            while ( cur != last )
            {
                // prev -> cur = a segment
                results.push_back( std::string( prev, cur ) );

                prev = cur;
                ++prev;
                cur = std::find( prev, last, delim );
            }
            // prev -> last = last segment
            results.push_back( std::string( prev, cur ) );

            dest.swap( results );
        }

        inline void
        split( const std::string&          inp,
               char                        delim,
               std::vector< std::string >& dest )
        {
            split( inp.begin( ), inp.end( ), delim, dest );
        }

        inline std::vector< std::string >
        split( const std::string& inp, char delim )
        {
            std::vector< std::string > retval;
            split( inp, delim, retval );
            return retval;
        }
    }
}

#endif // NDS_DETAIL_STR_UTIL_HH