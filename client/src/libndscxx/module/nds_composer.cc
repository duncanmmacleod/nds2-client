#include <algorithm>
#include <algorithm>

#include "nds.hh"
#include "nds_request_fragment.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_gap_handler.hh"
#include "nds_composer.hh"

#include "debug_stream.hh"

namespace NDS
{
    namespace detail
    {

        fixed_point_gap_handler composer::zero_gap_handler(
            fixed_point_gap_handler::static_val::ZERO_VAL );

        composer::composer(
            request_fragment::working_buffers&         buffers,
            const NDS::connection::channel_names_type& channel_names,
            const NDS::detail::buffer_initializer&     initializer,
            const gap_handler&                         _gap_handler,
            bool buffers_have_been_initialized )
            : buffers( buffers ), _channel_names( channel_names ),
              initializer( initializer ), _gap_handler( _gap_handler ),
              cur_progress( buffers.size( ), 0 ), delayed_handlers( ),
              empty_is_safe( buffers_have_been_initialized ),
              initialized( false ), finished( false )
        {
            if ( _channel_names.size( ) != buffers.size( ) )
            {
                throw std::invalid_argument( "Internal error, buffer count "
                                             "does not match channel name "
                                             "count" );
            }
        }

        void
        composer::add_segment( const buffers_type& cur_bufs )
        {
            if ( finished )
            {
                return;
            }
            if ( cur_bufs.size( ) != buffers.size( ) )
            {
                return;
            }
            // first time through we need to size everything right
            // we put it off until here so we know the sample rates and such
            if ( !initialized )
            {
                initialize_buffers( cur_bufs );
            }

            size_t i = 0;
            for ( buffers_type::const_iterator cur = cur_bufs.begin( );
                  cur != cur_bufs.end( );
                  ++cur, ++i )
            {
                const buffer&           cur_buffer = *cur;
                buffer::gps_second_type offset_secs =
                    cur_buffer.Start( ) - buffers[ i ]->Start( );
                buffer::size_type offset_samples =
                    cur_buffer.seconds_to_samples( offset_secs );

                bounds_check( *( buffers[ i ] ),
                              cur_progress[ i ],
                              offset_samples,
                              offset_samples + cur_buffer.Samples( ) );

                unsigned char *start = nullptr, *end = nullptr;
                if ( cur_progress[ i ] != offset_samples )
                {
                    // only print the debug output once
                    if ( i == 0 )
                    {
                        NDS::detail::dout( )
                            << "   GAP: "
                            << cur_buffer.samples_to_seconds(
                                   cur_progress[ i ] ) +
                                buffers[ i ]->Start( )
                            << " - " << ( size_t )( offset_secs +
                                                    buffers[ i ]->Start( ) )
                            << std::endl;
                    }
                    fill_gap(
                        *( buffers[ i ] ), cur_progress[ i ], offset_samples );
                }
                if ( i == 0 )
                {
                    NDS::detail::dout( ) << std::endl
                                         << "  " << cur_bufs.size( )
                                         << " channels" << std::endl;
                    NDS::detail::dout( ) << "  " << cur_buffer.Start( ) << ":"
                                         << cur_buffer.StartNano( ) << " - "
                                         << cur_buffer.Stop( ) << std::endl
                                         << std::endl;
                }
                unsigned char* dest =
                    const_cast< unsigned char* >(
                        reinterpret_cast< const unsigned char* >(
                            buffers[ i ]->cbegin< void >( ) ) ) +
                    cur_buffer.samples_to_bytes( offset_samples );
                start = const_cast< unsigned char* >(
                    reinterpret_cast< const unsigned char* >(
                        cur_buffer.cbegin< void >( ) ) );
                end = start +
                    ( cur_buffer.samples_to_bytes( cur_buffer.Samples( ) ) );
                std::copy( start, end, dest );
                NDS::detail::dout( )
                    << "i: " << i << " start = " << (void*)start
                    << " stop = " << (void*)end << " dest = " << (void*)dest;
                NDS::detail::dout( )
                    << " Samples: " << cur_buffer.Samples( )
                    << " sample size = " << cur_buffer.DataTypeSize( )
                    << std::endl;
                cur_progress[ i ] = offset_samples + cur_buffer.Samples( );
            }
        }

        void
        composer::do_finish_gap_handling( )
        {
            /* handle trailing gaps */
            size_t i = 0;
            for ( request_fragment::working_buffers::iterator
                      cur = buffers.begin( );
                  cur != buffers.end( );
                  ++cur, ++i )
            {
                bool          gap_printed = false;
                const buffer& cur_buffer = *( *cur );
                if ( cur_progress[ i ] < cur_buffer.Samples( ) )
                {
                    if ( !gap_printed )
                    {
                        gap_printed = true;
                        NDS::detail::dout( ) << "   Trailing GAP: "
                                             << cur_buffer.samples_to_seconds(
                                                    cur_progress[ i ] ) +
                                cur_buffer.Start( )
                                             << " - " << cur_buffer.Stop( )
                                             << std::endl;
                    }
                    // trailing gap
                    fill_gap( *( buffers[ i ] ),
                              cur_progress[ i ],
                              cur_buffer.Samples( ) );
                    cur_progress[ i ] = cur_buffer.Samples( );
                }
            }
            for ( const auto& cur : delayed_handlers )
            {
                if ( cur )
                {
                    ( *cur )( );
                }
            }
            delayed_handlers.clear( );
        }

        void
        composer::bounds_check( const buffer&     cur_buffer,
                                buffer::size_type cur_fill,
                                buffer::size_type offset_start,
                                buffer::size_type offset_end )
        {
            buffer::size_type dest_samples = cur_buffer.Samples( );

            if ( offset_start >= dest_samples || offset_start > offset_end ||
                 offset_end > dest_samples || offset_start < cur_fill )
            {
                throw connection::unexpected_channels_received_error( );
            }
        }
    }
}
