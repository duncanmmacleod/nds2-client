#include <iostream>

#include "nds_buffer.hh"

namespace NDS
{
    inline namespace abi_0
    {
        //---------------------------------------------------------------------
        // buffer
        //---------------------------------------------------------------------
        buffer::buffer( )
        {
        }

        buffer::buffer( const channel&      ChannelInfo,
                        gps_second_type     Second,
                        gps_nanosecond_type NanoSecond,
                        const void*         Buffer,
                        size_type           BufferSize )
            : channel( ChannelInfo ), gps_second( Second ),
              gps_nanosecond( NanoSecond )
        {
            const data_type::value_type* start(
                reinterpret_cast< const data_type::value_type* >( Buffer ) );
            const data_type::value_type* end(
                reinterpret_cast< const data_type::value_type* >( Buffer ) +
                BufferSize );
            data.resize( BufferSize );
            if ( data.size( ) > 0 )
            {
                std::copy( start, end, &( data[ 0 ] ) );
            }
        }

        buffer::buffer( const channel&      ChannelInfo,
                        gps_second_type     Second,
                        gps_nanosecond_type NanoSecond,
                        data_type&&         Buffer )
            : channel( ChannelInfo ), gps_second( Second ),
              gps_nanosecond( NanoSecond ), data( std::move( Buffer ) )
        {
        }

        void
        buffer::swap( buffer& Source )
        {
            gps_second_type     gpss( Source.gps_second );
            gps_nanosecond_type gpsns( Source.gps_nanosecond );

            Source.gps_second = gps_second;
            Source.gps_nanosecond = gps_nanosecond;

            gps_second = gpss;
            gps_nanosecond = gpsns;

            data.swap( Source.data );
            channel::swap( Source );
        }

        //=====================================================================
        //=====================================================================
        void
        buffer::reset_channel_info( const channel&      ChannelInfo,
                                    gps_second_type     Second,
                                    gps_nanosecond_type NanoSecond )
        {
            channel tmp( ChannelInfo );
            ( (channel*)this )->swap( tmp );
            gps_second = Second;
            gps_nanosecond = NanoSecond;
            data.resize( 0 );
        }

        std::ostream&
        operator<<( std::ostream& os, const buffer& obj )
        {
            os << "<" << obj.Name( ) << " (GPS time " << obj.Start( ) << ", "
               << obj.Samples( ) << " samples)>";
            return os;
        }

        extern std::ostream&
        operator<<( std::ostream& os, const buffers_type& obj )
        {
            os << "(";
            buffers_type::const_iterator cur = obj.begin( );
            for ( ; cur != obj.end( ); ++cur )
            {
                os << "'" << *cur << "',";
            }
            os << ")";
            return os;
        }
    }
}
