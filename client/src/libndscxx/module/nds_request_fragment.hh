#ifndef NDS_REQUEST_FRAGMENT_HH
#define NDS_REQUEST_FRAGMENT_HH

#include <memory>

#include "nds_availability.hh"

namespace NDS
{
    namespace detail
    {
        struct request_fragment
        {
            // typedef std::pair<buffer::gps_second_type,
            // buffer::gps_second_type>
            // time_span_type;
            typedef std::vector< buffer* >   working_buffers;
            typedef simple_segment_list_type time_span_type;

            channel::channel_names_type names;
            working_buffers             buffers;
            time_span_type              time_spans;

            bool is_compatible( const time_span_type& avail ) const;

            bool push_back_if( const std::string& name,
                               time_span_type     avail,
                               buffer*            dest_buffer );

            void bulk_set( const buffer::channel_names_type& names,
                           const working_buffers&            dest_buffers,
                           buffer::gps_second_type           gps_start,
                           buffer::gps_second_type           gps_stop );
        };

        typedef std::vector< request_fragment > request_fragments_type;
    }
}

#endif // NDS_CONNECTION_REQUEST_HH
