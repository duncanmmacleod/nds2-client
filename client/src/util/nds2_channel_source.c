/* -*- mode: c; c-basic-offset: 4; -*- */
#if HAVE_CONFIG_H
#include "daq_config.h"
#endif /* HAVE_CONFIG_H */

#include "daqc.h"
#include "trench.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SOURCE_LIST 262144

/************************************************************************
 *                                                                      *
 *       Main Routine                                                   *
 *                                                                      *
 ************************************************************************/
int
main( int argc, const char* argv[] )
{
    enum nds_version vrsn = nds_v2;

    short       port_id = DAQD_PORT;
    const char* node_id = "localhost";
    const char* epoch_str = 0;
    time_t      gps = 0;
    int         verbose = 0;
    int         avail = 0;

    int   iarg;
    int   syntax = 0;
    daq_t daq;
    int   rc;

    for ( iarg = 1; iarg < argc && *( argv[ iarg ] ) == '-'; ++iarg )
    {
        if ( !strcmp( argv[ iarg ], "--help" ) )
        {
            syntax = 1;
            break;
        }
        else if ( !strcmp( argv[ iarg ], "-a" ) ||
                  !strcmp( argv[ iarg ], "--available" ) )
        {
            avail++;
        }
        else if ( !strcmp( argv[ iarg ], "-k" ) ||
                  !strcmp( argv[ iarg ], "--epoch" ) )
        {
            epoch_str = argv[ ++iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "-n" ) )
        {
            node_id = argv[ ++iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "-p" ) )
        {
            port_id = (short)strtol( argv[ ++iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "-s" ) )
        {
            gps = (time_t)strtol( argv[ ++iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "-v" ) )
        {
            verbose++;
        }
        else if ( !strcmp( argv[ iarg ], "-1" ) )
        {
            vrsn = nds_v1;
        }
        else if ( !strcmp( argv[ iarg ], "-2" ) )
        {
            vrsn = nds_v2;
        }
        else
        {
            fprintf( stderr, "Unrecognized command: %s\n", argv[ iarg ] );
            syntax = 1;
            break;
        }
    }
    if ( iarg == argc )
    {
        fprintf( stderr, "No channels requested!\n" );
        syntax = 1;
    }

    if ( syntax )
    {
        fprintf( stderr, "Command syntax: \n" );
        fprintf(
            stderr,
            "nds_channel_source [-n <server>] [-p <port>] [-s <start-gps>]\n" );
        fprintf(
            stderr,
            "                   [-a | --available] [-k | --epoch <epoch>]\n" );
        fprintf( stderr, "                   <channel-list>\n" );
        fprintf( stderr, "where: \n" );
        fprintf( stderr,
                 "    -a      List time spans for which data are available\n" );
        fprintf( stderr, "    -k      Select in data availability\n" );
        fprintf( stderr, "    -n      Specify server IP address\n" );
        fprintf( stderr, "    -p      Specify port number [%i]\n", DAQD_PORT );
        fprintf( stderr, "    -s      Specify start time (as GPS)\n" );
        fprintf( stderr, "    -v      Verbose printout (dump channel data)\n" );
        fprintf( stderr, "    --help  This message\n" );
        fprintf( stderr, "    <channel-list> Space delimited channel list\n" );
        return 1;
    }
    daq_startup( );

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Connect to the NDS server                                     *
     *                                                                    *
     *--------------------------------------------------------------------*/
    if ( !port_id )
        port_id = DAQD_PORT;
    rc = daq_connect( &daq, node_id, port_id, vrsn );
    if ( rc )
    {
        printf( "Error in daq_connect: %s\n", daq_strerror( rc ) );
        return 1;
    }

    /*-------------------------------------------------------------------*
     *                                                                   *
     *     Get and dump the channel data for the specified channels      *
     *                                                                   *
     *-------------------------------------------------------------------*/
    {
        int i;

        for ( i = iarg; i < argc; ++i )
        {
            daq_channel_t chan;
            daq_init_channel( &chan, argv[ i ], cUnknown, 0.0, _undefined );
            daq_request_channel_from_chanlist( &daq, &chan );
        }
    }

    /*-------------------------------------------------------------------*
     *                                                                   *
     *     Get and dump the source types for the specified channels      *
     *                                                                   *
     *-------------------------------------------------------------------*/
    {
        char* list = malloc( (size_t)MAX_SOURCE_LIST );
        long  src_len;

        if ( epoch_str )
        {
            rc = daq_set_epoch( &daq, epoch_str );
            if ( rc )
                printf( "Warning: Unable to set epoch '%s': %s\n",
                        epoch_str,
                        daq_strerror( rc ) );
        }
        if ( avail )
        {
            rc = daq_recv_source_data(
                &daq, list, (size_t)MAX_SOURCE_LIST, gps, &src_len );
        }
        else
        {
            rc = daq_recv_source_list(
                &daq, list, (size_t)MAX_SOURCE_LIST, gps, &src_len );
        }
        if ( rc )
        {
            printf( "Error in daq_recv_source_list: %s\n", daq_strerror( rc ) );
        }
        else
        {
            char* cur = list;
            char* last = &( list[ src_len ] );
            char* cbrace;

            printf( "Requested source list: \n" );
            for ( cur = list, last = &( list[ src_len ] ); cur < last; cur++ )
            {

                if ( *cur == ' ' )
                    continue;
                cbrace = strchr( cur, '}' );
                if ( cbrace )
                {
                    cbrace += 1;
                    *cbrace = 0;
                    printf( "%s\n", cur );
                    cur = cbrace;
                }
                else
                {
                    printf( "Source list is too large, please limit the "
                            "request with an epock '-k'" );
                    break;
                }
            }
        }
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *     All done, go away...                                           *
     *                                                                    *
     *--------------------------------------------------------------------*/
    daq_disconnect( &daq );
    daq_recv_shutdown( &daq );
    return 0;
}
