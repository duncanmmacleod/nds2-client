/* -*- mode: c; c-basic-offset: 4; -*- */
#include "daqc.h"
#include "daqc_private.h"
#include "daqc_internal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define NEW_VECT( type, dim ) ( (type*)malloc( dim * sizeof( type ) ) )

/************************************************************************
 *                                                                      *
 *       Main Routine                                                   *
 *                                                                      *
 ************************************************************************/
int
main( int argc, const char* argv[] )
{
    enum nds_version vrsn = nds_v2;

    short       port_id = DAQD_PORT;
    const char* node_id = "localhost";
    int         verbose = 0;

    int iarg;
    int syntax = 0;
    for ( iarg = 1; iarg < argc && *( argv[ iarg ] ) == '-'; ++iarg )
    {
        if ( !strcmp( argv[ iarg ], "--help" ) )
        {
            syntax = 1;
            break;
        }
        else if ( !strcmp( argv[ iarg ], "-n" ) )
        {
            node_id = argv[ ++iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "-p" ) )
        {
            port_id = (short)strtol( argv[ ++iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "-v" ) )
        {
            verbose++;
        }
        else
        {
            fprintf( stderr, "Unrecognized argument: %s\n", argv[ iarg ] );
            syntax = 1;
            break;
        }
    }

    if ( syntax )
    {
        fprintf( stderr, "Command syntax: \n" );
        fprintf( stderr, "nds2_auth [-v] [-n <server>] [-p <port>]\n" );
        fprintf( stderr, "where: \n" );
        fprintf( stderr, "    -n      Specify server IP address\n" );
        fprintf( stderr, "    -p      Specify port number [%i]\n", DAQD_PORT );
        fprintf( stderr, "    -v      Verbose printout\n" );
        return 1;
    }
    daq_startup( );

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Connect to the NDS server                                     *
     *                                                                    *
     *--------------------------------------------------------------------*/
    if ( !port_id )
        port_id = DAQD_PORT;
    daq_t daq;
    int   rc = daq_connect( &daq, node_id, port_id, vrsn );
    if ( rc )
    {
        printf( "Error in daq_connect: %s\n", daq_strerror( rc ) );
        return 1;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *     All done, go away...                                           *
     *                                                                    *
     *--------------------------------------------------------------------*/
    daq_disconnect( &daq );
    daq_recv_shutdown( &daq );
    return 0;
}
